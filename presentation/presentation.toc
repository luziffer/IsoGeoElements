\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{Spline Construction}{6}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{1}{B(asic)-Splines}{7}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{2}{Non-uniform Rational B-Splines}{27}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{3}{Physical Maps}{29}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Quasi-Interpolation}{31}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{1}{Least-Squares Projector}{32}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{2}{$L^2$-Projector}{34}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Variational Methods}{38}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{1}{Variational Crimes}{41}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{2}{Example PDEs}{46}{0}{3}
