deps = [

  #dependencies
  "LinearAlgebra",
  "FastGaussQuadrature",
  "PyPlot",

  #dependencies of examples
  "CSV",
  "DataFrames"
  
  #only needed for test
  "Calculus",  
  "ProgressMeter",
  
  #only neededed for comparison
  "JuAFEM"
])

using Pkg
uuid(name) = Pkg.METADATA_compatible_uuid(name)
for dep in deps 
	println(dep, " = \"", uuid(dep),"\"")
end


