#include("draw1D.jl")
import PyPlot: contour, contourf, plot_surface, quiver



function plot(spline::Union{BSpline2D,NURBS2D}; order=0,
        style=(order==0 ? :contourf : :quiver),
        t = subdivisions(spline, n=(order==0 ? 15 : 5) ),
        coef=nothing, i=nothing, kwargs... )

    order = (order == 1 ? :∇ : order)

    #setup 2D grid
    x = repeat([t[1]], length(t[2]))
    y = repeat([t[2]], length(t[1]))
    x = Matrix( hcat(x...)); y = Matrix( hcat(y...)' )

    #plotting argument
    if order==0
        z = nothing
    elseif order==:∇
        uv = nothing
    else
        error("Derivative of order $(order) not supported.")
    end

    #heights from coefficients
    if coef != nothing
        if order == 0
            B = evaluate(spline, t)
            B = reshape(B, size(B,1)*size(B,2), size(B,3), size(B,4))
            z = sum( coef .* B, dims=1 )[1,:,:]
        elseif order == :∇
            ∇B = evaluate(spline, t)
            ∇B = reshape(B, 2, size(∇B,2)*size(∇B,3), size(∇B,4), size(∇B,5))
            uv = reshape(coef, 1, size(coef)...) .* ∇B
            uv = sum(uv, dims=2)
            uv = uv[:, 1, repeat([:], length(size(uv))-2)... ]
        end

    #heights from single index
    elseif i != nothing
        if i isa Integer #linear index
            s = size(spline)
            indices = [ (a,b) for a=1:s[1], b=1:s[2] ]
            i = indices[i] #to multi-index
        end

        if order == 0
            z = evaluate(spline, t, i)
        elseif order == :∇
            uv = ∇evaluate(spline, t, i)
        end
    else error("Either coefficient or index must be set.") end

    if order == 0
        if style == :plot_surface
            plot_surface(x,y,z; kwargs...)
        elseif style == :contour
            contour(x,y,z; kwargs...)
        elseif style == :contourf
            contourf(x,y,z; kwargs...)
        else
            error("Style $(style) not supported.")
        end

    elseif order == :∇
        u,v = uv[1,:,:], uv[2,:,:]
        if style == :quiver
            quiver(x,y,u,v; kwargs...)
        else
            error("Style $(style) not supported for gradient.")
        end
    end
end



function plot(spline::Union{BSpline2D,NURBS2D}, phys::PhysicalMap{2}; order=0,
        style=(order==0 ? :contourf : :quiver),
        t = subdivisions(spline, n=(order==0 ? 15 : 5) ),
        coef=nothing, i=nothing, kwargs... )

    order = (order == 1 ? :∇ : order)

    #setup 2D grid
    xy = evaluate(phys,t)
    x = xy[1,:,:]; y = xy[2,:,:]

    #plotting argument
    if order==0
        z = nothing
    elseif order==:∇
        uv = nothing
    else
        error("Derivative of order $(order) not supported.")
    end

    #heights from coefficients
    if coef != nothing
        if order == 0
            B = evaluate(spline, t)
            B = reshape(B, size(B,1)*size(B,2), size(B,3), size(B,4))
            z = sum( coef .* B, dims=1 )[1,:,:]
        elseif order == :∇
            ∇B = evaluate(spline, phys, t)
            ∇B = reshape(B, 2, size(∇B,2)*size(∇B,3), size(∇B,4), size(∇B,5))
            uv = reshape(coef, 1, size(coef)...) .* ∇B
            uv = sum(uv, dims=2)
            uv = uv[:, 1, repeat([:], length(size(uv))-2)... ]
        end

    #heights from single index
    elseif i != nothing
        if i isa Integer #linear index
            s = size(spline)
            indices = [ (a,b) for a=1:s[1], b=1:s[2] ]
            i = indices[i] #to multi-index
        end

        if order == 0
            z = evaluate(spline, t, i)
        elseif order == :∇
            uv = ∇evaluate(spline, phys, t, i)
        end
    else error("Either coefficient or index must be set.") end

    if order == 0
        if style == :plot_surface
            plot_surface(x,y,z; kwargs...)
        elseif style == :contour
            contour(x,y,z; kwargs...)
        elseif style == :contourf
            contourf(x,y,z; kwargs...)
        else
            error("Style $(style) not supported.")
        end

    elseif order == :∇
        u,v = uv[1,:,:], uv[2,:,:]
        if style == :quiver
            quiver(x,y,u,v; kwargs...)
        else
            error("Style $(style) not supported for gradient.")
        end
    end
end
