BezierSpline(p::Integer) = BSpline1D([ zeros(p+1)..., ones(p+1)... ], p)

RegularSpline(ζ::Vector{T} where T <: Real, p::Integer) =
    BSpline1D( [ repeat([ζ[1]],p)..., ζ..., repeat([ζ[end]],p) ... ], p)

PiecewisePoly(ζ::Vector{T} where T <: Real, p::Integer) =
    BSpline1D( [ζ[1], repeat(ζ', p)..., ζ[end]] , p)
