using Calculus: gradient


"""piece of annulus, isomorphic to unit box"""
struct Annulus
    r_min::Real
    r_max::Real
    ϕ_min::Real
    ϕ_max::Real
end
QuaterAnnulus() = Annulus(1.0, 2.0, 0., π/2)

function PhysicalMap(a::Annulus)
    #control points
    function x(r,ϕ)
        y = [cos(ϕ) sin(ϕ)]
        y = r .*  y ./ maximum( abs.(y) )
        return y
    end
    ϕ_mid = (a.ϕ_max + a.ϕ_min)/2
    controls = [
        x(a.r_min, a.ϕ_min); x(a.r_min, ϕ_mid); x(a.r_min, a.ϕ_max)
        x(a.r_max, a.ϕ_min); x(a.r_max, ϕ_mid); x(a.r_max, a.ϕ_max)
    ]
    controls = round.(controls', digits=12)

    #nurbs basis
    radial= BSpline1D([0.,0,0,1,1,1],2)
    angular = BSpline1D([0.,0,1,1],1)
    spline = BSpline((radial, angular))
    weights = [ x(1.,a.ϕ_min); x(1.,ϕ_mid); x(1.,a.ϕ_max) ]
    weights = round.(weights', digits=12)
    weights = 1 ./ sqrt.( sum( weights.^2, dims=1)[1,:] )
    weights = [ weights; weights ]
    spline = NURBS2D(spline, weights)


    phys = PhysicalMap(spline, controls)
    return phys
end

#calculated albegraically
function inverse_quater_annulus(x,y)
    r = sqrt.( x.^2 + y.^2 )
    y = y./r
    β = -(2-√2) .* y .- (√2)
    α =  (2-√2) .* y .+ (√2-1)
    t2 = (-β .- sqrt.(β.^2 .- 4 .* α .* y )) ./ (2 .* α)
    t1 = r[:,1] .- 1; t2 = t2[1,:]
    return t1,t2
end

#integrate using radial coordinates
function mass_matrix_quater_annulus(spline::Union{NURBS2D,BSpline2D}; n_quadrature=25)
    phys = PhysicalMap(QuaterAnnulus())

    #basis properties
    shape = size(spline)
    indices = LinearIndices(shape)
    p = ( spline isa NURBS ? spline.bspline.p : spline.p )
    Ξ = ( spline isa NURBS ? spline.bspline.Ξ : spline.Ξ )

    #uniform grid quadrature
    x = 2. .* [ 0. : 1/n_quadrature : 1. ... ]
    y = 2. .* [ 0. : 1/n_quadrature : 1. ... ]
    x = hcat( repeat([x], n_quadrature+1)... )
    y = hcat( repeat([y], n_quadrature+1)... )
    y = Matrix(y')
    r² = x .^2 .+ y .^2
    mask = 1 .< r² .< 4.
    x = x[mask]; y = y[mask]
    N = sum(mask)

    #loop over quadrature points
    M = zeros(length(spline), length(spline))
    for (x_n,y_n) ∈ zip(x,y)
        t_n = inverse_quater_annulus([x_n],[y_n])

        #loop over indices and evaluate
        for i1=1:shape[1], i2=1:shape[2]
            if !( Ξ[1][i1] ≤ t_n[1][1] ≤ Ξ[1][i1+p[1]+1] ) continue end
            if !( Ξ[2][i2] ≤ t_n[2][1] ≤ Ξ[2][i2+p[2]+1] ) continue end
            i = indices[i1,i2]
            B_i = evaluate(spline, t_n, i)[1,1]
            for j1=i1:min(shape[1],i1+p[1]), j2=i2:min(shape[2],i2+p[2])
                j = indices[j1,j2]
                B_j = evaluate(spline, t_n, j)[1,1]

                M[i,j] += B_i * B_j
            end
        end
    end
    M = M .* (3π/4) ./ N

    #symmetrize
    for i=1:length(spline)
        for j=1:i-1
            M[i,j]=M[j,i]
        end
    end

    return M
end



#integrate using radial coordinates
function stiffness_matrix_quater_annulus(spline::Union{NURBS2D,BSpline2D}; n_quadrature=25)
    phys = PhysicalMap(QuaterAnnulus())

    #basis properties
    shape = size(spline)
    indices = LinearIndices(shape)
    p = ( spline isa NURBS ? spline.bspline.p : spline.p )
    Ξ = ( spline isa NURBS ? spline.bspline.Ξ : spline.Ξ )

    #uniform grid quadrature
    x = 2. .* [ 0. : 1/n_quadrature : 1. ... ]
    y = 2. .* [ 0. : 1/n_quadrature : 1. ... ]
    x = hcat( repeat([x], n_quadrature+1)... )
    y = hcat( repeat([y], n_quadrature+1)... )
    y = Matrix(y')
    r² = x .^2 .+ y .^2
    mask = 1 .< r² .< 4.
    x = x[mask]; y = y[mask]
    N = sum(mask)

    #loop over quadrature points
    S = zeros(length(spline), length(spline))
    for (x_n,y_n) ∈ zip(x,y)
        t_n = inverse_quater_annulus([x_n],[y_n])

        #loop over indices and evaluate
        for i1=1:shape[1], i2=1:shape[2]
            if !( Ξ[1][i1] ≤ t_n[1][1] ≤ Ξ[1][i1+p[1]+1] ) continue end
            if !( Ξ[2][i2] ≤ t_n[2][1] ≤ Ξ[2][i2+p[2]+1] ) continue end
            i = indices[i1,i2]

            B_i = x -> evaluate(spline, inverse_quater_annulus([x[1]],[x[2]]), (i1,i2))[1,1]
            ∇B_i = gradient(B_i, [x_n,y_n])
            for j1=i1:min(shape[1],i1+p[1]), j2=i2:min(shape[2],i2+p[2])
                j = indices[j1,j2]
                B_j = x -> evaluate(spline, inverse_quater_annulus([x[1]],[x[2]]), (j1,j2))[1,1]
                ∇B_j = gradient(B_j, [x_n,y_n])
                
                S[i,j] += sum(∇B_i .* ∇B_j)
            end
        end
    end
    S = S .* (3π/4) ./ N

    #symmetrize
    for i=1:length(spline)
        for j=1:i-1
            S[i,j]=S[j,i]
        end
    end

    return S
end
