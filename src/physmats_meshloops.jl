
function stiffness_matrix(spline::Union{BSpline{d},NURBS{d}}, phys::PhysicalMap{d}) where d
    Ns = size(spline)
    M = zeros( Ns..., Ns... )

    #breakpoints#breakpoints
    if spline isa NURBS
        Ξ = spline.bspline.Ξ
        p = spline.bspline.p
    else
        Ξ = spline.Ξ
        p = spline.p
    end
    ζ = Tuple( unique(Ξ[k]) for k=1:d )
    L = Tuple( length(ζ[k])-1 for k=1:d )

    #conversion from breakpoint indices 'j' to knot indices 'i'
    ii = CartesianIndices(size(spline))
    jj = CartesianIndices(L)
    I = Tuple( convert(Vector{Integer}, indexin(ζ[k], Ξ[k])) for k=1:d )

    #quadrature
    Q_ref = Tuple( gauss(2p[k]+4) for k=1:d )
    Q = Tuple( quadrature(ζ[k], Q_ref[k]) for k=1:d )
    x = Tuple( Q[k].x for k=1:d )
    w = Tuple( Q[k].w for k=1:d )
    m = Tuple( length(Q_ref[k].x) for k=1:d )

    #loop over mesh segments (j₁ j₂ ... ) : (j₁+1 j₂+1 ...)
    for j=1:prod(L)
        j = jj[j]; j = Tuple(j[k] for k=1:d)
        #j_min = j; j_max = Tuple( j .+ ones(Integer,d) )

        #extract segment quadrature
        a_j = [ max(1, m[k]*(j[k]-1)+1) for k=1:d ]
        b_j = [ min(length(x[k]), m[k]*j[k]) for k=1:d ]
        x_j = Tuple( x[k][ a_j[k]:b_j[k] ] for k=1:d )
        γ = Tuple( w[k][ a_j[k]:b_j[k] ] for k=1:d )
        w_j = ( b_j[k]-a_j[k]+1 for k=1:d )
        w_j = ones(w_j...)
        for k=1:d
            shape = ones(Integer, d)
            shape[k] = length(γ[k])
            w_j = w_j .* reshape( γ[k], shape... )
        end

        #convert breakpoint indices to knot indices
        i_min = Tuple( I[k][j[k]] for k=1:d)
        i_max = Tuple( I[k][j[k]+1] for k=1:d)

        #extend to overlapping support
        i_min = Tuple( max(1, i_min[k]-p[k]) for k=1:d)
        i_max = Tuple( min(Ns[k], i_max[k]) for k=1:d)

        #evaluate
        B = evaluate(spline, x_j, i_min, i_max)
        shape = size(B)
        B1 = reshape( B, ones(Integer, d)..., shape[1:d]..., shape[d+1:2d]...)
        B2 = reshape( B, shape[1:d]..., ones(Integer, d)..., shape[d+1:2d]...)

        #jacobi determinant
        detDF = jacobi(phys, x_j)
        detDF = reshape(detDF, ones(Integer, 2d)..., size(detDF)...)

        #product of basis functions
		B = B1 .* B2		
		
        #weighted sum
        w_j = reshape(w_j, ones(Integer, 2d)..., size(w_j)...)
        B = w_j .* B .* detDF #integrant
        B = sum(B, dims=[ 2d+1:3d...])
        B = B[ repeat([:], 2d)..., ones(Integer, d)... ]

        #insert into global massmatrix
        indices = Tuple(i_min[k]:i_max[k] for k=1:d)
        M[ indices..., indices... ] += B
    end

    N = prod(Ns)
    M = reshape(M, N, N)
    return M
end


function stiffness_matrix(spline::Union{BSpline{d},NURBS{d}}, phys::PhysicalMap{d}) where d
    Ns = size(spline)
    S = zeros( Ns..., Ns... )

    #breakpoints#breakpoints
    if spline isa NURBS
        Ξ = spline.bspline.Ξ
        p = spline.bspline.p
    else
        Ξ = spline.Ξ
        p = spline.p
    end
    ζ = Tuple( unique(Ξ[k]) for k=1:d )
    L = Tuple( length(ζ[k])-1 for k=1:d )

    #conversion from breakpoint indices 'j' to knot indices 'i'
    ii = CartesianIndices(size(spline))
    jj = CartesianIndices(L)
    I = Tuple( convert(Vector{Integer}, indexin(ζ[k], Ξ[k])) for k=1:d )

    #quadrature
    Q_ref = Tuple( gauss(2p[k]+4) for k=1:d )
    Q = Tuple( quadrature(ζ[k], Q_ref[k]) for k=1:d )
    x = Tuple( Q[k].x for k=1:d )
    w = Tuple( Q[k].w for k=1:d )
    m = Tuple( length(Q_ref[k].x) for k=1:d )

    #loop over mesh segments (j₁ j₂ ... ) : (j₁+1 j₂+1 ...)
    for j=1:prod(L)
        j = jj[j]; j = Tuple(j[k] for k=1:d)
        #j_min = j; j_max = Tuple( j .+ ones(Integer,d) )

        #extract segment quadrature
        a_j = [ max(1, m[k]*(j[k]-1)+1) for k=1:d ]
        b_j = [ min(length(x[k]), m[k]*j[k]) for k=1:d ]
        x_j = Tuple( x[k][ a_j[k]:b_j[k] ] for k=1:d )
        γ = Tuple( w[k][ a_j[k]:b_j[k] ] for k=1:d )
        w_j = ( b_j[k]-a_j[k]+1 for k=1:d )
        w_j = ones(w_j...)
        for k=1:d
            shape = ones(Integer, d)
            shape[k] = length(γ[k])
            w_j = w_j .* reshape( γ[k], shape... )
        end

        #convert breakpoint indices to knot indices
        i_min = Tuple( I[k][j[k]] for k=1:d)
        i_max = Tuple( I[k][j[k]+1] for k=1:d)

        #extend to overlapping support
        i_min = Tuple( max(1, i_min[k]-p[k]) for k=1:d)
        i_max = Tuple( min(Ns[k], i_max[k]) for k=1:d)

        #evaluate
        ∇B = ∇evaluate(spline, phys, x_j, i_min, i_max)
        n = size(∇B,1); shape = size(∇B)[2:end]
        ∇B1 = reshape( ∇B, n, ones(Integer, d)..., shape[1:d]..., shape[d+1:2d]...)
        ∇B2 = reshape( ∇B, n, shape[1:d]..., ones(Integer, d)..., shape[d+1:2d]...)

        #jacobi determinant
        detDF = jacobi(phys, x_j)
        detDF = reshape(detDF, ones(Integer, 2d)..., size(detDF)...)

        #dot product
        B = ∇B1 .* ∇B2
        B = sum( B, dims=1 )
        B = B[1, repeat([:], 3d)... ]

        #weighted sum
        w_j = reshape(w_j, ones(Integer, 2d)..., size(w_j)...)
        B = w_j .* B .* detDF #integrant
        B = sum(B, dims=[ 2d+1:3d...])
        B = B[ repeat([:], 2d)..., ones(Integer, d)... ]

        #insert into global massmatrix
        indices = Tuple(i_min[k]:i_max[k] for k=1:d)
        S[ indices..., indices... ] += B
    end

    N = prod(Ns)
    S = reshape(S, N, N)
    return S
end
