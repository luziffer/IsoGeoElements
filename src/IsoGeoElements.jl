
module IsoGeoElements

export

#bases
    BSpline,
    BSpline1D,
    BSpline2D,
    NURBS,
    NURBS1D,
    NURBS2D,
    meshsize,

#evaluation and derivative
    evaluate,
    ∂evaluate,
    ∇evaluate,

#geometry
    PhysicalMap,
    jacobi,
    boundary,

#integration
    subdivisions,
    Quadrature,
    Quadrature1D,
    gauss,
    trapz,
    quadrature,

#variational
    mass_matrix,
    stiffness_matrix,

#quasi-interpolation
    LstSqProjector,

#commonly used
    BezierSpline,
    RegularSpline,
    PiecewisePoly,
    Annulus,
    QuaterAnnulus


include("nurbs.jl")
include("evaluate.jl")
include("derivatives.jl")
include("grids.jl")
include("massmats.jl")
include("stiffmats.jl")
include("physmats.jl")
include("projectors.jl")
include("boundaries.jl")
include("draw1D.jl")
include("draw2D.jl")
include("common.jl")


end
