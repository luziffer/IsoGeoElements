#include("physmats_indexloops.jl")
#include("physmats_meshloops.jl")
using FastGaussQuadrature: gausslegendre

function mass_matrix(spline::Union{NURBS{d},BSpline{d}}, phys::PhysicalMap{d}) where d
    Ξ,p = (spline isa NURBS ? (spline.bspline.Ξ, spline.bspline.p) : (spline.Ξ, spline.p) )
    multiindices = CartesianIndices( size(spline) )

    M = zeros( length(spline), length(spline) )
    for i=1:length(spline)
        ii = Tuple(multiindices[i])

        #quadrature rule
        t, γ = [], []
        for k=1:d
            x_k,w_k = gausslegendre(2*p[k])

            #weights
            append!(γ, [repeat(w_k, p[k]+1)])

            #abscisses
            x_k = .5 .* (x_k .+ 1)
            t_k = []
            for q=0:p[k]
                l = Ξ[k][ii[k]+q]
                r = Ξ[k][ii[k]+q+1]
                append!(t_k, x_k .* (r-l) .+ l)
            end
            append!(t, [t_k])
        end
        t, γ = Tuple(t), Tuple(γ)

        #evaluate shape functions
        Bi = evaluate(spline, t, ii)
        for j=i:length(spline)
            jj = Tuple(multiindices[j])
            if any( (jj .- ii) .> p .+ 1 ) continue end
            Bj = evaluate(spline, t, jj)

            #integrate

            M_ij = Bi .* Bj
            M_ij = detJ(phys, t) .* M_ij
            for k=1:d
                γ_k = γ[k]
                shape = ones(Int, d)
                shape[k] = length(γ_k)
                γ_k = reshape(γ_k, shape...)
                M_ij = γ_k .* M_ij
                M_ij = sum(M_ij, dims=k)
            end
            M_ij = M_ij[ ones(Int,d)... ]

            #update matrix
            M[i,j] += M_ij
        end
    end

    #enforce symmetry
    for i=1:length(spline)
        for j=1:i-1
            M[i,j] = M[j,i]
        end
    end
    return M
end
