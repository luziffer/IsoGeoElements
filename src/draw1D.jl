#include("grids.jl")
import PyPlot: plot

function plot(spline::Union{BSpline1D, NURBS1D}; t = subdivisions(spline,n=30),
    coef=nothing, i=nothing, order=0, kwargs... )

    if (coef != nothing) && (i != nothing)
        error("Either display single index or coeffients, not both.")
    end
    if order ≥ 2 || order < 0
        error("derivative ∂^$order invalid")
    end

    y = nothing
    if i isa Integer
        if order == 0
            y = evaluate(spline, t, i)
        elseif order == 1
            y = ∂evaluate(spline, t, i, i)[1,:]
        end
    elseif coef isa Vector
        @assert length(coef) == length(spline) "coef $(length(coef)) must match spline $(length(sline))"
        if order==0
            y = evaluate(spline, t)' * coef
        elseif order==1
            y = ∂evaluate(spline, t)' * coef
        end
    else
        if order == 0
            y = evaluate(spline, t)'
        elseif order==1
            y = ∂evaluate(spline, t)'
        end
    end
    #println("t, y ", size(t), size(y))
    plot(t, y; kwargs...)
end


function plot(spline::Union{BSpline1D, NURBS1D}, phys::PhysicalMap{1}; t = subdivisions(spline,n=30),
    coef=nothing, i=nothing, order=0, kwargs... )

    if (coef != nothing) && (i != nothing)
        error("Either display single index or coeffients, not both.")
    end
    if order ≥ 2 || order < 0
        error("derivative ∂^$order invalid")
    end

    z = nothing
    if i isa Integer
        if order == 0
            z = evaluate(spline, t, i)
        elseif order == 1
            uv = ∂evaluate(spline, t, i, i)[1,:]
        end
    elseif coef isa Vector
        @assert length(coef) == length(spline) "coef $(length(coef)) must match spline $(length(sline))"
        if order==0
            z = evaluate(spline,t)' * coef
        elseif order==1
            uv = ∂evaluate(spline, phys, t)' * coef
        end
    else
        if order == 0
            z = evaluate(spline, t)'
        elseif order==1
            uv = ∇evaluate(spline, phys, t, order=order)'
        end
    end


    xy = evaluate(phys, t)
    x = xy[1,:]; y = xy[2, :]
    if order == 0
        plot3D(x,y,z; kwargs...)
    elseif order == 1
        u = uv[1,:]
        v = uv[2,:]
        quiver(x,y,u,v)
    end

end
