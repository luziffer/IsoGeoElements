
#=
function boundary(spline::BSpline{d}, dim::Integer) where d
    shape = size(spline)
    mask = zeros(Bool, shape)
    bnd₋ = [ repeat([:],dim-1)..., 1, repeat([:],d-dim)... ]
    bnd₊ = [ repeat([:],dim-1)..., shape[dim], repeat([:],d-dim)... ]
    mask[ bnd₋ ... ] .= true
    mask[ bnd₊ ... ] .= true
    return mask
end
=#

function boundary(spline::Union{BSpline{d}, NURBS{d}}) where d
    shape = size(spline)
    indices = CartesianIndices(shape)
    mask = zeros(Bool, shape)
    for k=1:d
        mask_k = [true, zeros(Bool, shape[k]-2)..., true]
        new_shape = ones(Int, d)
        new_shape[k] = length(mask_k)
        mask_k = reshape(mask_k, new_shape...)
        mask = mask .| mask_k
    end
    return mask
end

#=
boundary(nurbs::NURBS{d}, dim::Integer) where d = boundary(nurbs.bspline, dim)
boundary(nurbs::NURBS{d}) where d = boundary(nurbs.bspline)
=#
