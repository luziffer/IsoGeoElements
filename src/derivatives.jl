#= Univariate Derivative =#

#fully vectorized
function ∂evaluate(spline::BSpline1D, t)
    p,Ξ = spline.p[1], spline.Ξ[1]

    h = Ξ[p+1:end] .- Ξ[1:end-p]
    nz = h .> 0; h[nz] = 1 ./ h[nz]

    B = evaluate( BSpline1D(Ξ, p-1), t )
    ∂B = p .* h .* B
    ∂B = ∂B[1:end-1, :] .- ∂B[2:end, :]

    return ∂B
end


#sparsely vectorized
function ∂evaluate(spline::BSpline1D, t, i_min::Integer, i_max::Integer)
@assert ensure_index_valid(spline, (i_min,), (i_max,))

    p,Ξ = spline.p[1], spline.Ξ[1]
    Ξ = Ξ[ i_min : i_max+p+1 ]
    s = BSpline1D(Ξ,p)
    ∂B = ∂evaluate(s, t)

    return ∂B
end

∂evaluate(spline::BSpline1D, t, i::Integer) = ∂evaluate(spline, t, i, i)[1,:]



#= Multivariate Derivative =#

#fully vectorized
function ∇evaluate(spline::BSpline{d}, t::NTuple{d}) where d
    N,T = [N for N in size(spline)], [ length(t[k]) for k=1:d ]
    indices = repeat([:], 2d)
    ∇B = ones( Tuple([d; N...; T... ]) )
    stencil = ones(Integer, length(size(∇B))-1 )
    for k=1:d
        spline_k = BSpline1D(spline.Ξ[k], spline.p[k])
        B_k = evaluate( spline_k, t[k] )
        ∂B_k = ∂evaluate( spline_k, t[k] )

        #manual broadcast
        shape = copy(stencil)
        shape[k] = size(B_k,1)
        shape[k+d] = size(B_k,2)
        B_k = reshape(B_k, shape...)
        ∂B_k = reshape(∂B_k, shape...)

        for l=1:d
            ∇B[l, indices... ] = ∇B[l, indices...] .* ( l==k ? ∂B_k : B_k)
        end
    end

    shape = (d, N..., T...)
    ∇B = reshape(∇B, shape )
    return ∇B
end

function ∇evaluate(spline::BSpline{d}, t::NTuple{d}, i_min::MultiIndex{d}, i_max::MultiIndex{d}) where d
@assert ensure_index_valid(spline, i_min, i_max)

    N = collect( i_max[k]-i_min[k]+1 for k=1:d )
    T = collect( length(t[k]) for k=1:d )
    indices = [ repeat([:], 2d)... ]

    ∇B = ones( d, N..., T... )
    stencil = ones(Integer, length(size(∇B))-1 )
    for k=1:d

        spline_k = BSpline1D(spline.Ξ[k], spline.p[k])
        B_k = evaluate( spline_k, t[k], i_min[k], i_max[k] )
        ∂B_k = ∂evaluate( spline_k, t[k], i_min[k], i_max[k])

        #manual broadcast
        shape = copy(stencil)
        shape[k] = size(B_k,1)
        shape[k+d] = size(B_k,2)
        B_k = reshape(B_k, shape...)
        ∂B_k = reshape(∂B_k, shape...)

        for l=1:d
            ∇B[l, indices... ] = ∇B[l, indices...] .* ( l==k ? ∂B_k : B_k )
        end
    end

    shape = (d, N..., T...)
    ∇B = reshape(∇B, shape )
    return ∇B

end

#Integer evaluations
function ∇evaluate(spline::BSpline{d}, t::NTuple{d},  i_min::Integer, i_max::Integer) where d
    cartesian = CartesianIndices(size(spline))
    i_min = cartesian[i_min]
    i_max = cartesian[i_min]
    i_min = Tuple( i_min[k] for k=1:d )
    i_max = Tuple( i_min[k] for k=1:d )
    return ∇evaluate(spline, t, i_min, i_max)
end

#single evaluation
∇evaluate(spline::BSpline{d}, t::NTuple{d}, i::Union{Integer, MultiIndex{d}}) where d =
    ∇evaluate(spline,t,i,i)[ 1:d, ones(Integer,d)... , [1:length(t[k]) for k=1:d]... ]



#= NURBS =#

#fully vectorized
function ∇evaluate(nurbs::NURBS{d}, t::NTuple{d}) where d
    B = evaluate(nurbs.bspline, (d==1 ? t[1] : t))
    ∇B = ∇evaluate(nurbs.bspline,t)
    shape = size(B)

    #flatten indices
    N = length(nurbs)
    B = reshape(B, (1, N, shape[d+1:end]...) )
    ∇B = reshape(∇B, (d, N, shape[d+1:end]...) )
    w = reshape(nurbs.weights, 1, N )


    #projective transformation
    B = w .* B
    W = sum(B, dims=2)
    nz = W .!= 0
    W[nz] = 1 ./ W[nz]
    ∇B = w .* ∇B
    ∇W = sum(∇B, dims=2)
    ∇B = (∇B  .- B .* ∇W .* W) .* W
    ∇B = reshape(∇B, (d,shape...) )

    return ∇B
end

function ∇evaluate(nurbs::NURBS{d}, t::NTuple{d},
    i_min::MultiIndex{d}, i_max::MultiIndex{d}) where d

    #support ranges
    N,b = size(nurbs), nurbs.bspline
    j_min = Tuple(max(1,   i_min[k]-b.p[k]-1)  for k=1:d)
    j_max = Tuple(min(N[k],i_max[k]+b.p[k]+1)  for k=1:d)

    #crop support range
    w = reshape(nurbs.weights, N)
    indices = [ j_min[k]:j_max[k] for k=1:d ]
    w = w[ indices... ]
    i = (d==1 ? j_min[1] : j_min)
    j = (d==1 ? j_max[1] : j_max)
    B = evaluate(b,(d==1 ? t[1] : t),i,j)
    ∇B = ∇evaluate(b,t,j_min,j_max)
    shape = size(B)
    N = prod(shape[1:d])
    B = reshape(B, (1, N, shape[d+1:end]...) )
    ∇B = reshape(∇B, (d, N, shape[d+1:end]...) )
    w = reshape(w, 1, N )

    #projective transformation
    B = w .* B
    W = sum(B, dims=2)
    nz = W .!= 0
    W[nz] = 1 ./ W[nz]
    ∇B = w .* ∇B
    ∇W = sum(∇B, dims=2)
    ∇B = (∇B  .- B .* ∇W .* W) .* W

    #crop desired range
    ∇B = reshape(∇B, d, shape...)
    rel_min = [ i_min[k]-j_min[k]+1 for k=1:d ]
    rel_max = [ i_max[k]-j_min[k]+1 for k=1:d ]
    indices = [ rel_min[k]:rel_max[k] for k=1:d ]
    indices = [ indices... , [1:shape[d+k] for k=1:d]... ]
    ∇B = ∇B[ 1:d, indices... ]
    return ∇B
end

#Integer evaluations
function ∇evaluate(spline::NURBS{d}, t::NTuple{d},  i_min::Integer, i_max::Integer) where d
    cartesian = CartesianIndices(size(spline))
    i_min = cartesian[i_min]
    i_max = cartesian[i_min]
    i_min = Tuple( i_min[k] for k=1:d )
    i_max = Tuple( i_min[k] for k=1:d )
    return ∇evaluate(spline, t, i_min, i_max)
end

#single evaluation
∇evaluate(spline::NURBS{d}, t::NTuple{d}, i::Union{Integer, MultiIndex{d}}) where d =
    (∇evaluate(spline,t,i,i)[:, ones(Integer,d)... , repeat([:], d)... ])

#one dimensional
∂evaluate(nurbs::NURBS1D, t::Vector{T} where T<:Real) =
    ( ∇evaluate(nurbs, (t,) )[1,:,:] )
∂evaluate(nurbs::NURBS1D, t::Vector{T} where T<:Real, i_min::Integer, i_max::Integer) =
    ( ∇evaluate(nurbs, (t,), (i_min,), (i_max,) )[1,:,:] )
∂evaluate(nurbs::NURBS1D, t::Vector{T} where T<:Real, i::Integer) = ( ∂evaluate(nurbs, t,i,i )[1,:] )


#= Physical =#

function ∇evaluate(F::PhysicalMap{d}, t::NTuple{d})  where d
    b, C = F.basis, F.controls
    C = reshape(C, size(C,1), 1, size(C,2))

    #B = evaluate(b,t)
    ∇B = ∇evaluate(b, t)
    ∇B = reshape(∇B, d, length(b), size(∇B)[d+2:end]...)
    ∇B = reshape(∇B, 1, size(∇B)... )
    ∇B = sum(C .* ∇B, dims=3)
    ∇B = ∇B[:, :, 1, repeat([:], d)... ]
    return ∇B
end


#full vectorization
function ∇evaluate(spline::Union{BSpline{d},NURBS{d}},
    phys::PhysicalMap{d}, t::NTuple{d}) where d

    #println("WARNING! ∇evaluate(s,F,t) not tested" )

    #evaluate
    DF = ∇evaluate(phys, t)
    ∇B = ∇evaluate(spline, t)
    println("∇B ", size(∇B))

    #shape constants
    T = Tuple( length(t[k]) for k=1:d )
    τ = prod(T); n = size(DF, 1)

    #invert and flatten
    DF = reshape(DF, n, d, τ)
    for l=1:τ
        DF[:,:,l] = pinv(DF[:,:,l])'
    end
    DF = reshape(DF, n, d, ones(Integer, d)..., τ)
    ∇B = reshape(∇B, 1, d, size(spline)..., τ)

    #matrix product
    ∇B = DF .* ∇B
    ∇B = sum(∇B, dims=2)
    ∇B = ∇B[:, 1, repeat([:], d)..., :]
    ∇B = reshape(∇B, n, size(spline)..., T...)
    return ∇B
end


#ranged vectorization
function ∇evaluate(spline::Union{BSpline{d},NURBS{d}},
    phys::PhysicalMap{d}, t::NTuple{d},
    i_min::MultiIndex{d}, i_max::MultiIndex{d}) where d

    #println("WARNING! ∇evaluate(s,F,t,i,j) not tested" )

    #evaluate
    DF = ∇evaluate(phys, t)
    ∇B = ∇evaluate(spline, t, i_min, i_max)

    #shape constants
    T = Tuple( length(t[k]) for k=1:d )
    τ = prod(T); n = size(DF, 1)

    #invert and flatten
    DF = reshape(DF, n, d, τ)
    for l=1:τ
        DF[:,:,l] = pinv(DF[:,:,l])
    end
    DF = reshape(DF, size(DF)[1:end-1]..., ones(Integer, d)..., τ)
    ∇B = reshape(∇B, 1, d, size(∇B)[2:d+1]..., τ)

    #matrix product
    ∇B = DF .* ∇B
    ∇B = sum(∇B, dims=2)
    ∇B = ∇B[:, 1, repeat([:], d+1)...]
    ∇B = reshape(∇B, n, size(∇B)[2:d+1]..., T...)
    return ∇B
end

#single evaluation
∇evaluate(spline::Union{BSpline{d},NURBS{d}},
    phys::PhysicalMap{d}, t::NTuple{d}, i::MultiIndex{d}) where d =
        (∇evaluate(spline,phys,t,i,i)[:, ones(Integer,d)... , repeat([:], d)... ])

#jacobi pseudo-determinant
function detJ(phys::PhysicalMap{d}, t::NTuple{d}) where d

    #evaluate
    DF = ∇evaluate(phys, t)

    #shape constants
    T = Tuple( length(t[k]) for k=1:d )
    τ = prod(T); n = size(DF, 1)

    #calculate pseudo determinant
    detDF = zeros(τ)
    DF = reshape(DF, n, d, τ)
    for l=1:τ

        DF_l = DF[:,:,l]
        if n==d
            detDF[l] = abs( det( DF[:,:,l] ) )
        else
            #println(" > WARNING! using pseudo-determinant" )
            M = DF[:,:,l]
            #println("M ", size(M))
            M = transpose(M) * M
            detDF[l] = sqrt( det( M ) )
        end
    end
    detDF = reshape(detDF, T...)
    #detDF = 1 ./ detDF

    return detDF
end
