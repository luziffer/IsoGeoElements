export LstSqProjector
using LinearAlgebra


## Least squares projection
"""
    LstSqProjector( b, t, f )

evaluate function `f` at grid given by `t` and
calculate coefficients for the spline basis `b`
"""
function LstSqProjector end

#univariate
function LstSqProjector(spline::Union{BSpline1D, NURBS1D}, t, f::Function)
    Y = f.(t)
    B = evaluate(spline,t)

    B = Matrix(B')
    a = B \ Y
    return a
end

#multivariate
function LstSqProjector(spline::Union{BSpline{d}, NURBS{d}}, t::NTuple{d}, f::Function) where d

    #parametric space
    x = [(  shape=ones(Integer, d);
            shape[k]=length(t[k]);
            shape = Tuple(shape);
            reshape(t[k], shape) )
        for k=1:d
    ]

    #formulate least squares problem
    T = prod( length(t[k]) for k=1:d )
    Y = reshape( f.( x... ), T )
    B = evaluate(spline,t)
    B = reshape(B, (prod(size(spline)), T) )

    #solve least squares
    B = Matrix(B')
    a = B \ Y
    a = reshape(a, size(spline))
    return a
end

#physical
function LstSqProjector(spline::Union{BSpline{d}, NURBS{d}}, F::PhysicalMap{d}, t::NTuple{d}, f::Function) where d

    #physical space
    x = evaluate(F, t); n = size(x,1)
    indices = repeat([:], d)
    x = Tuple( x[l, indices...] for l=1:n )

    #formulate least squares problem
    T = prod( length(t[k]) for k=1:d )
    Y = f( x... )
    Y = reshape(Y, T)
    B = evaluate(spline, t)
    B = reshape(B, length(spline), T)

    #solve least squares
    B = Matrix(B')
    a = B \ Y
    a = real(a)
    a = reshape(a, size(spline))
    return a
end

## TODO: Taylor projection
