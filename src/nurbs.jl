import Base: length,size
#using Base.Cartesian

abstract type AbstractSpline end

## B-Spline

"""
    BSpline{d}( (Ξ¹ … Ξᵈ), (p¹ … pᵈ) )

constuct B(asic)-Spline with `d`-Tuple of knot vectors `Ξ` and degrees `p`
"""
struct BSpline{d} <: AbstractSpline
    Ξ::NTuple{d,Vector{T}} where T <: Real      #knot vectors
    p::NTuple{d,Integer}                        #degrees
end

const BSpline1D = BSpline{1}
const BSpline2D = BSpline{2}
const BSpline3D = BSpline{3}

#dimensionality of resulting spline space
size(spline::BSpline{d}) where d =
    Tuple( length(spline.Ξ[k])-spline.p[k]-1 for k=1:d )
length(spline::BSpline{d}) where d = prod(size(spline))

const MultiIndex = NTuple{d,Integer} where d
ensure_index_valid(spline::BSpline{d}, i_min::MultiIndex{d}, i_max::MultiIndex{d}) where d =
    (s = size(spline); all( 1 ≤ i_min[k] ≤ i_max[k] ≤ s[k] for k=1:d) )


"""
    meshsize(b)

returns max_j |ζⱼ₊₁ - ζⱼ|, useful for error analysis
"""
function meshsize(spline::BSpline{d}) where d
    h = zeros(d)
    for k=1:d
        Ξ = unique( spline.Ξ[k] )
        h[k] = maximum( Ξ[2:end] - Ξ[1:end-1] )
    end
    if d == 1 return h[1] else return Tuple(h) end
end

#1D constructor
"""
    BSpline1D(Ξ, p)

construct univariate B-Spline from `p`-open knot vector `Ξ`
"""
function BSpline1D(Ξ::Vector{T} where T <: Real, p::Integer)
    @assert Ξ == sort(Ξ) "knot vector Ξ must be ordered"
    @assert 0 ≤ p ≤ length(Ξ)-1 "degree p=$(p) is invalid"
    @assert length(Ξ)>=p+1 "expected knot length $(p+1) but recieved $(length(Ξ))"
    return BSpline1D((Ξ,),(p,))
end

#tensor constructor
"""
    BSpline( (s1, s2, ... ) )

tensor product of `BSpline1D`s `s1`,`s2`,...
"""
function BSpline(splines::NTuple{d, BSpline1D}) where d
    Ξ = Tuple( splines[k].Ξ[1] for k=1:d )
    p = Tuple( splines[k].p[1] for k=1:d )
    return BSpline{d}(Ξ,p)
end


## NURBS

"""
    NURBS{d}(b, w)

Non-Uniform Rational B-Splines with `d`-variate B-Spline basis `b` and weight vector `w`
"""
struct NURBS{d} <: AbstractSpline
    bspline::BSpline{d}                 #Basis
    weights::Vector{T} where T <: Real  #Weight

    function NURBS{d}(bspline::AbstractSpline, weights::Vector) where d
        @assert length(bspline)==length(weights) "weights dims $(length(weights)) need to match spline dims $(size(bspline))≡$(length(bspline))"
        new{d}(bspline, weights)
    end
end

#inherit specializations
const NURBS1D = NURBS{1}
const NURBS2D = NURBS{2}
const NURBS3D = NURBS{3}

#inherit attributes from multivariate B-BSpline as well
size(nurbs::NURBS{d}) where d = size(nurbs.bspline)
length(nurbs::NURBS{d}) where d = length(nurbs.bspline)
meshsize(nurbs::NURBS{d}) where d = meshsize(nurbs.bspline)



##Physical map

"""
    PhysicalMap{d}( b, C )

construct spline (B-Spline or NURBS) physical map `F` given by
    F: [0,1]ᵈ →    Ω ⊂ ℝⁿ
          t   ↦  ∑ᵢ cᵢ bᵢ(t)
from `d`-variate spline basis `b` and control points `C`
"""
struct PhysicalMap{d} <: AbstractSpline
    basis::Union{ NURBS{d}, BSpline{d} }
    controls::Matrix{T} where T <: Real

    function PhysicalMap(b::Union{ NURBS{d}, BSpline{d} }, C::Matrix) where d
        @assert size(C,2) == length(b) "$(size(C,2)) control points given, but $(length(b)) expected";
        new{d}(b, C)
    end
end

size(phys::PhysicalMap{d}) where d = ( size(controls,1), d ) # = (n, d)
