#include("nurbs.jl")
using FastGaussQuadrature: gausslegendre

function subdivisions(spline::BSpline{d}; n=3 ) where d

    #number of subdivisions
    if n isa Integer n = Tuple(n for k=1:d) end

    #loop over breakpoints
    t = []; for k=1:d
        t_k = unique(spline.Ξ[k])

        #subdivide meshsize
        h = t_k[2:end] .- t_k[1:end-1]
        h = h .* [0 : 1/n[k] : 1 ...]'

        t_k = t_k[1:end-1] .+ h
        t_k = [t_k' ... ]
        append!(t, [t_k])
    end

    t = Tuple( t[k] for k=1:d )
    if d==1 return t[1] else return t end
end

subdivisions(nurbs::NURBS{d}; n=5 ) where d =
    subdivisions(nurbs.bspline; n=n)


## Quadrature is weighted sum for functions with product arguments

struct Quadrature1D
    x::Vector{T} where T <: Real    # abscissas
    w::Vector{T} where T <: Real    # weights
end

struct Quadrature{d}
    x::NTuple{d, Vector{T}} where T<:Real
    w::NTuple{d, Vector{T}} where T<:Real
end

ensure_stable_quadrature(Q::Quadrature1D) =
    (length(Q.w)==length(Q.x) && all(-1 .≤ Q.x .≤ 1) && all(Q.w .> 0) )

const Mesh1D = Vector{T} where T <: Real
const Mesh = Tuple{Vararg{Mesh1D, d}} where d
ensure_minimal_mesh_size(ζ::Mesh1D; h=1e-12) = ( all(h .≤ ζ[2:end] .- ζ[1:end-1]) )

## Reference  quadratures

#Gauss quadrature
function gauss(p)::Quadrature1D
    #p = p ÷ 2 + p % 2 #exact for 2p + 1
    x,w = gausslegendre(p)
    return Quadrature1D(x,w)
end

#Trapezoidal rule
function trapz(n)::Quadrature1D
    x = 2 .* collect(0:n) ./ n .- 1
    w = 2 .* ones(n+1) ./ n
    w[1] = w[end] = 1 / n
    return Quadrature1D(x,w)
end


## Affine quadratures

function quadrature(ζ::Mesh1D, Q_ref::Quadrature1D)
@assert ensure_stable_quadrature(Q_ref)
#@assert ensure_minimal_mesh_size(ζ)

    #fetch data
    x,w = (Q_ref.x .+ 1.) ./ 2. , Q_ref.w ./ 2.
    a,h = ζ[1:end-1], ζ[2:end] - ζ[1:end-1]

    n,q = length(ζ)-1, length(x)
    a,h = reshape(a,1,n), reshape(h,1,n)
    x,w = reshape(x,q,1), reshape(w,q,1)

    #affine transform
    t = h .* x .+ a
    γ = h .* w
    t,γ = [t...], [γ...]

    return Quadrature1D(t,γ)

end

#multi-dimensional
function quadrature(ζ::Mesh{d}, Q_ref::NTuple{d,Quadrature1D}) where d
    Q = Tuple( quadrature(ζ[k],Q_ref[k]) for k=1:d )
    x = Tuple(Q[k].x for k=1:d)
    w = ( ( s = ones(Integer, d);
            s[k] = length(x[k]);
            reshape(Q[k].w, s...) ) for k=1:d )
    w = prod(w)
    return Quadrature(x,w)
end

#repeated reference quadrature
function quadrature(ζ::Mesh{d}, Q_ref::Quadrature1D) where d
    Q_ref = repeat([Q_ref],d)
    Q_ref = NTuple{d,Quadrature1D}(Q_ref)
    quadrature(ζ, Q_ref)
end
