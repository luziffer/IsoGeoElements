using Base.Cartesian: CartesianIndices

"""
    evaluate(b, t)
    evaluate(b, t, i)
    evaluate(b, t, i, j)

evaluate the spline basis `b` at points `t`,
efficient sparse vectorization using index ranges `i` and `j`
"""
function evaluate end

## BSpline1D

#fully vectorized
function evaluate(spline::BSpline1D, t)
    Ξ, p = spline.Ξ[1], spline.p[1]

    if p == 0 #intitialize with indicator function
        idx  = argmax(Ξ) - 1; max = Ξ[idx+1]
        t = [t...]; t = t[:,:]' #implicit broadcast
        Ξ = reshape(Ξ, length(Ξ),1)
        B = (Ξ[1:end-1, :] .≤ t) .* (t .< Ξ[2:end, :])
        B[idx:idx, :] .= (Ξ[idx:idx,:] .≤ t) .* (t .≤ max)

    elseif p > 0 #otherwise recursive
        B = evaluate(BSpline1D(Ξ,p-1), t)
        t = [t...]; t = t[:,:]'

        #blending functions
        α = Ξ[p+1:end] .- Ξ[1:end-p]
        nz = α .> 0
        α[nz] = 1. ./ α[nz]
        Ξ = reshape(Ξ, length(Ξ), 1)
        α = reshape(α, length(α), 1)
        α = α .* (t .- Ξ[1:end-p,:])

        #formula of Cox deBoor
        B = α[1:end-1,:] .* B[1:end-1,:] .+ (1 .- α[2:end,:]) .* B[2:end,:]

    end
    return B
end

#sparsely vectorized by integer intervals
function evaluate(spline::BSpline1D, t, i_min::Integer, i_max::Integer)
@assert ensure_index_valid(spline,(i_min,),(i_max,)) "Indices $i_min, $i_max out of range (1,$(length(spline)))"

    Ξ,p = spline.Ξ[1], spline.p[1]
    crop = BSpline1D( Ξ[i_min:i_max+p+1], p )
    return evaluate( crop, t )
end

#single index evaluation
evaluate(spline::BSpline1D, t, i::Integer) = evaluate(spline,t,i,i)[1,:]

## BSpline{d} where d > 1

#tensor product
function evaluate(spline::BSpline{d}, t::NTuple{d}) where d

    T = [ length(t[k]) for k=1:d ]
    B = ones(size(spline)..., T...)

    #loop over 1D-spline factors
    for k=1:d
        spline_k = BSpline1D(spline.Ξ[k], spline.p[k])
        B_k = evaluate(spline_k, t[k])

        #manual broadcast
        shape = ones(Integer, 2*d)
        shape[k], shape[k+d] = size(B_k)
        B_k = reshape(B_k, shape...)
        B = B .* B_k
    end
    return B
end

#tensor product
function evaluate(spline::BSpline{d}, t::NTuple{d},
    i_min::MultiIndex{d}, i_max::MultiIndex) where d
@assert ensure_index_valid(spline, i_min, i_max)

    T = [ length(t[k]) for k=1:d ]
    N = [i_max[k]-i_min[k]+1 for k=1:d]
    B = ones(N..., T...)

    #loop over 1D-spline factors
    for k=1:d
        spline_k = BSpline1D(spline.Ξ[k], spline.p[k])
        B_k = evaluate(spline_k, t[k], i_min[k], i_max[k])

        #manual broadcast
        shape = ones(Integer, 2*d)
        shape[k], shape[k+d] = size(B_k)
        B_k = reshape(B_k, shape...)
        B = B .* B_k
    end
    return B
end


## NURBS

#fully vectorized
function evaluate(nurbs::NURBS{d}, t::NTuple{d}) where d
    if d==1 t = t[1] end
    B = evaluate(nurbs.bspline, t)
    shape = size(B)

    #flatten indices
    N = length(nurbs)
    B = reshape(B, (N, shape[d+1:end]...) )

    #projective transformation
    B = nurbs.weights .* B
    W = sum(B, dims=1)
    nz = (W .!= 0)
    W[nz] = 1 ./ W[nz]
    B = B .* W

    B = reshape(B, shape)
    return B
end

#sparsely vectorized
function evaluate(nurbs::NURBS{d}, t::NTuple{d},
    i_min::MultiIndex{d}, i_max::MultiIndex{d}) where d

    #support ranges
    b,N = nurbs.bspline, size(nurbs)
    j_min = Tuple(max(1,   i_min[k]-b.p[k])    for k=1:d)
    j_max = Tuple(min(N[k],i_max[k]+b.p[k]+1)  for k=1:d)

    if d==1 t = t[1]; i=j_min[1]; j=j_max[1]
    else i=j_min; j=j_max end

    #crop support range
    w = reshape(nurbs.weights, N)
    indices = ( j_min[k]:j_max[k] for k=1:d )
    w = w[ indices... ]
    B = evaluate(b,t, i,j )
    shape = size(B)

    w = reshape(w, size(w)..., ones(Integer, d)... )

    #projective transformation
    B = w .* B
    W = sum(B, dims=[1:d...])
    nz = W .!= 0
    W[nz] = 1 ./ W[nz]
    B = B .* W

    #crop desired range
    rel_min = [ i_min[k]-j_min[k]+1 for k=1:d ]
    rel_max = [ i_max[k]-j_min[k]+1 for k=1:d ]
    indices = [ rel_min[k]:rel_max[k] for k=1:d ]
    B = B[ indices... , repeat([:], d)... ]
    return B
end

#one dimensional
evaluate(spline::NURBS1D, t::Vector{T} where T<:Real) = evaluate(spline,(t,) )
evaluate(spline::NURBS1D, t::Vector{T} where T<:Real, i_min::Integer, i_max::Integer) = evaluate(spline,(t,),(i_min,),(i_max,))


## syntax sugar

#flat index evaluations
function evaluate(spline::Union{BSpline{d},NURBS{d}}, t,
    i_min::Integer, i_max::Integer) where d

    cartesian = CartesianIndices(size(spline))
    i_min = cartesian[i_min]
    i_max = cartesian[i_min]
    i_min = Tuple( i_min[k] for k=1:d )
    i_max = Tuple( i_min[k] for k=1:d )
    return evaluate(spline, t, i_min, i_max)
end


#single index evaluation
evaluate(spline::Union{NURBS{d}, BSpline{d}}, t, i::Union{Integer, MultiIndex{d}}) where d =
    ( evaluate(spline,t,i,i)[ones(Integer, d)..., repeat([:],d)...] )


## Physical
function evaluate(phys::PhysicalMap{d}, t) where d
    b, C = phys.basis, phys.controls
    B = evaluate(b, t)
    B = reshape(B, 1,length(b), size(B)[d+1:end]...)
    F = B .* C
    F = sum(F, dims=2)[:, 1, repeat([:], d)...]
    return F
end
