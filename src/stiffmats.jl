export stiffness_matrix
using Base.Cartesian: CartesianIndices

#univariate
function stiffness_matrix(spline::BSpline1D)
    N = length(spline)
    ζ,p = unique(spline.Ξ[1]), spline.p[1]
    S = zeros(N,N)

    #get knot indices 'i' from breakpoint indices 'j'
    I = indexin(ζ,spline.Ξ[1])
    I = convert(Vector{Integer}, I)

    #quadrature
    Q_ref = gauss(2p)
    Q = quadrature(ζ, Q_ref)
    x,w = Q.x, Q.w
    m,l =  length(Q_ref.x), length(Q.x)

    #loop over mesh
    for j= 1 : l ÷ m
        x_j = x[ m*(j-1)+1 : m*j ]
        w_j = w[ m*(j-1)+1 : m*j ]
        w_j = reshape(w_j, 1, 1, length(w_j) )

        #evaluate
        i_min, i_max = max(1,I[j]-p), I[j+1]-1

        ∂B = ∂evaluate(spline, x_j, i_min, i_max)
        ∂B1 = reshape(∂B, 1, size(∂B,1), size(∂B,2) )
        ∂B2 = reshape(∂B, size(∂B,1), 1, size(∂B,2) )
        B = w_j .* ∂B1 .* ∂B2 #integrant

        #weighted sum
        B = sum(B, dims=3)[:,:,1]
        S[i_min:i_max, i_min:i_max] += B
    end

    return S
end


#multivariate
function stiffness_matrix(spline::BSpline{d}) where d
    N = size(spline)
    masses = []; stiffs = []

    stencil = ones(Integer, 2*d)
    for k=1:d
        s_k = BSpline1D(spline.Ξ[k],spline.p[k])

        #caclulate univariate mass matrix
        M_k = mass_matrix(s_k)
        shape = copy(stencil)
        shape[k],shape[k+d] = size(M_k)
        M_k = reshape(M_k, shape...)

        #caclulate univariate stiffness matrix
        S_k = stiffness_matrix(s_k)
        shape = copy(stencil)
        shape[k],shape[k+d] = size(S_k)
        S_k = reshape(S_k, shape...)

        append!(masses,[M_k])
        append!(stiffs,[S_k])
    end

    S = zeros(N..., N...)
    for k=1:d
        S_k = ones(N..., N...)
        for l=1:d
            S_k = S_k .* ( l==k ? stiffs[l] : masses[l] )
        end
        S = S .+ S_k
    end

    N = prod(N)
    S = reshape(S, N, N )
    return S
end



#nurbs with breakpoints
function stiffness_matrix(nurbs::NURBS{d}) where d
    Ns = size(nurbs)
    S = zeros( Ns..., Ns... )

    #breakpoints
    Ξ = nurbs.bspline.Ξ
    p = nurbs.bspline.p
    ζ = Tuple( unique(Ξ[k]) for k=1:d )
    L = Tuple( length(ζ[k])-1 for k=1:d )

    #conversion from breakpoint indices 'j' to knot indices 'i'
    ii = CartesianIndices(size(nurbs))
    jj = CartesianIndices(L)
    I = Tuple( convert(Vector{Integer}, indexin(ζ[k], Ξ[k])) for k=1:d )

    #quadrature
    Q_ref = Tuple( gauss(2p[k]) for k=1:d )
    Q = Tuple( quadrature(ζ[k], Q_ref[k]) for k=1:d )
    x = Tuple( Q[k].x for k=1:d )
    w = Tuple( Q[k].w for k=1:d )
    m = Tuple( length(Q_ref[k].x) for k=1:d )

    #loop over mesh segments (j₁ j₂ ... ) : (j₁+1 j₂+1 ...)
    for j=1:prod(L)
        j = jj[j]; j = Tuple(j[k] for k=1:d)
        #j_min = j; j_max = Tuple( j .+ ones(Integer,d) )

        #extract segment quadrature
        a_j = [ max(1, m[k]*(j[k]-1)+1) for k=1:d ]
        b_j = [ min(length(x[k]), m[k]*j[k]) for k=1:d ]
        x_j = Tuple( x[k][ a_j[k]:b_j[k] ] for k=1:d )
        γ = Tuple( w[k][ a_j[k]:b_j[k] ] for k=1:d )
        w_j = ( b_j[k]-a_j[k]+1 for k=1:d )
        w_j = ones(w_j...)
        for k=1:d
            shape = ones(Integer, d)
            shape[k] = length(γ[k])
            w_j = w_j .* reshape( γ[k], shape... )
        end

        #convert breakpoint indices to knot indices
        i_min = Tuple( I[k][j[k]] for k=1:d)
        i_max = Tuple( I[k][j[k]+1] for k=1:d)

        #extend to overlapping support
        i_min = Tuple( max(1, i_min[k]-p[k]) for k=1:d)
        i_max = Tuple( min(Ns[k], i_max[k]) for k=1:d)

        #evaluate
        ∇B = ∇evaluate(nurbs, x_j, i_min, i_max)
        shape = size(∇B)[2:end]
        ∇B1 = reshape( ∇B, d, ones(Integer, d)..., shape[1:d]..., shape[d+1:2d]...)
        ∇B2 = reshape( ∇B, d, shape[1:d]..., ones(Integer, d)..., shape[d+1:2d]...)

        #dot product
        B = ∇B1 .* ∇B2
        B = sum( B, dims=1 )
        B = B[1, repeat([:], 3d)... ]

        #weighted sum
        w_j = reshape(w_j, ones(Integer, 2d)..., size(w_j)...)
        B = w_j .* B #integrant
        B = sum(B, dims=[ 2d+1:3d...])
        B = B[ repeat([:], 2d)..., ones(Integer, d)... ]

        #insert into global massmatrix
        indices = Tuple(i_min[k]:i_max[k] for k=1:d)
        S[ indices..., indices... ] += B
    end

    N = prod(Ns)
    S = reshape(S, N, N)
    return S
end
