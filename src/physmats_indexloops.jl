
#physical
function mass_matrix(spline::Union{NURBS{d},BSpline{d}}, phys::PhysicalMap{d}) where d
    M = zeros( length(spline), length(spline) )

    #breakpoints
    if spline isa NURBS
        Ξ = spline.bspline.Ξ
        p = spline.bspline.p
    else
        Ξ = spline.Ξ
        p = spline.p
    end

    #quadrature
    Q_ref = Tuple( gauss(2p[k]+4) for k=1:d )

    #loop over multiindices (i₁ i₂ ... )
    multiindices = CartesianIndices(size(spline))
    flatindices = LinearIndices(size(spline))
    neighbours = prod(p)
    δ = CartesianIndices(p)
    for ii=1:length(spline)
        i = Tuple( multiindices[ii] )

        #calculate local quadrature rule
        ζ = Tuple( unique( Ξ[k][ i[k]:i[k]+p[k] ]) for k=1:d )

        Q = quadrature(ζ, Q_ref); t = Q.x
        dt = ones( [ length(x) for x in Q.x ]... )
        for k=1:d
            shape = ones(Integer, d)
            shape[k] = length(Q.w[k])
            dt = dt .* reshape(Q.w[k], shape... )
        end

        #evaluate Jacobi determinant
        detJ = jacobi(phys, t)
        @assert all(detJ .> 0) "Jacobi determinant det(DF)≯0 not positive!"

        #evaluate shape functions
        B_i = evaluate(spline, t, i)
        for jj=1:neighbours
            j = min.( size(spline), i .+ Tuple( δ[jj] ) .- 1 )
            jj = flatindices[j...]
            B_j = evaluate(spline, t, j)

            #update matrix
            M_ij = B_i .* B_j .* detJ .* dt
            M[ii,jj] += sum( M_ij )
        end

    end

    #enforce symmetry
    for ii=1:length(spline)
        for jj=2:neighbours #ii ≠ jj
            j = min.( size(spline), i .+ Tuple( δ[jj] ) .- 1 )
            jj = flatindices[j...]
            M[jj,ii] = M[ii,jj]
        end
    end
    return M
end


#physical
function stiffness_matrix(spline::Union{NURBS{d},BSpline{d}}, phys::PhysicalMap{d}) where d
    S = zeros( length(spline), length(spline) )

    #breakpoints
    if spline isa NURBS
        Ξ = spline.bspline.Ξ
        p = spline.bspline.p
    else
        Ξ = spline.Ξ
        p = spline.p
    end

    #quadrature
    Q_ref = Tuple( gauss(2p[k]+4) for k=1:d )

    #loop over multiindices (i₁ i₂ ... )
    multiindices = CartesianIndices(size(spline))
    flatindices = LinearIndices(size(spline))
    neighbours = prod( spline.p )
    δ = CartesianIndices(spline.p)
    for ii=1:length(spline)
        i = Tuple( multiindices[ii] )

        #calculate local quadrature rule
        ζ = Tuple( unique(Ξ[k][ i[k]:i[k]+p[k] ]) for k=1:d )
        Q = quadrature(ζ, Q_ref); t = Q.x
        dt = ones( [ length(x) for x in Q.x ]... )
        for k=1:d
            shape = ones(Integer, d)
            shape[k] = length(Q.w[k])
            dt = dt .* reshape(Q.w[k], shape... )
        end

        #evaluate Jacobi determinant
        detJ = jacobi(phys, t)
        @assert all(detJ .> 0) "Jacobi determinant det(DF)≯0 not positive!"


        #evaluate shape function derivatives
        ∇B_i = ∇evaluate(spline, phys, t, i)
        for jj=1:neighbours
            j = min.( size(spline), i .+ Tuple( δ[jj] ) .- 1 )
            jj = flatindices[j...]
            ∇B_j = ∇evaluate(spline, phys, t, j)

            #update matrix
            S_ij = ∇B_i .* ∇B_j
            S_ij = sum(S_ij, dims=1)[1, repeat([:], d)...]
            S_ij = S_ij .* detJ .* dt
            S[ii,jj] += sum( S_ij )
        end

    end

    #enforce symmetry
    for ii=1:length(spline)
        for jj=2:neighbours #ii ≠ jj
            j = min.( size(spline), i .+ Tuple( δ[jj] ) .- 1 )
            jj = flatindices[j...]
            S[jj,ii] = S[ii,jj]
        end
    end
    return S
end
