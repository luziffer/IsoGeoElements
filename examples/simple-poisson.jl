include("../src/IsoGeoElements.jl")
using .IsoGeoElements

using LinearAlgebra
using DataFrames
using ProgressMeter
using CSV

#=
Fourier solution to
   Δu = 1 in Ω
   u  = 0 on ∂Ω
on parametric domain Ω=(0,1)²
=#
function fourier_poisson_solver(n_fourier, x, y)

    #coefficients of indicator function
    b = zeros(n_fourier)
    n = [1:n_fourier ... ]
    b[1:2:end] = 4 ./ (π .* n[1:2:end] )

    #inverse laplace operator on sine basis
    b =(b' .* b) ./ (π^2 .* ((n.^2)' .+ n.^2) )
    b = reshape(b, (size(b)..., 1, 1) )

    #evaluate sine basis
    ψ1 = sin.(π .* n .* x')
    ψ2 = sin.(π .* n .* y')
    ψ1 = reshape(ψ1, n_fourier, 1, length(x), 1)
    ψ2 = reshape(ψ2, 1, n_fourier, 1, length(y))
    ψ = ψ1 .* ψ2

    #linearly combine basis vectors
    u =  ψ .* b
    u = sum(u, dims=[1,2])
    u = u[1,1,:,:]
    return u
end

#solve the same problem with splines
function spline_poisson_solver(spline::Union{BSpline2D, NURBS2D})

    #assemble system matrixces
    M = mass_matrix(spline)
    S = stiffness_matrix(spline)

    #zero Dirichlet boundary conditions
    int = boundary(spline)
    int = .! int
    int = [ int ... ]
    dof = sum(int)

    S = S[int,:]; S = S[:,int]
    M = M[int,:]; M = M[:,int]

    #source term is constant one
    L = M * ones(size(M,1))

    #solve
    nz = sum( S .!= 0 )
    u_interior = S \ L
    u = zeros(length(spline))
    u[int] = u_interior

    return u,nz, dof
end


function compare_solutions(spline::Union{BSpline2D, NURBS2D},
    t, u_spline, u_true, relative=true)

    #evaluate spline solution
    B = evaluate(spline, t); x,y = t
    B = reshape(B, length(spline), length(x), length(y))
    u = sum(B .* u_spline, dims=1)[1,:,:]

    #calculate l2 error
    l2e = u .- u_true
    l2e = sum( l2e.^2 )
    if relative
        l2e = l2e / sum( u_true.^2 )
    end
    return √l2e
end



function analysis(splinegenerator::Function, name::String; path = "examples/data/")
    println("Calculating true solution...")
    x,y = t = [0:.01:1 ... ],[0:.01:1 ... ]
    u_true = fourier_poisson_solver(100,x,y)


    @showprogress .5 "analysis '$name'" for p = [1,2,3,4,5]
        df = DataFrame()

        for n = [1:5..., 10, 15, 20]
            spline = splinegenerator(n,p)
            u, nz, dof = spline_poisson_solver(spline)
            l2e = compare_solutions(spline, t, u, u_true)

            data = Dict(
                "h" => meshsize(spline)[1],
                "dof" => dof,
                "nz" => nz,
                "l2e" => l2e
            )
            append!(df, data)
        end

        fname = path * name * "_p$p.csv"
        f = open(fname, "w"); close(f)
        CSV.write(fname, df)
    end
end


#function main ()
    regular(n,p) = (s= RegularSpline([0:(1/n):1...], p); BSpline((s,s)))
    analysis(regular, "simplepoisson_regular")

    piecew(n,p) = (s= PiecewisePoly([0:(1/n):1...], p); BSpline((s,s)))
    analysis(piecew, "simplepoisson_piecew")

#end; main()
