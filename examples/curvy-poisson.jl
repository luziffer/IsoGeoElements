include("../src/IsoGeoElements.jl")
using .IsoGeoElements

using LinearAlgebra
using DataFrames
using ProgressMeter
using CSV


F = PhysicalMap(QuaterAnnulus())

#= Example1
u_polar(r,ϕ)  = r^2*(r-1)*(2-r) * sin(2ϕ)
Δu_polar(r,ϕ) = (21r - 14r^2 - 4) * sin(2ϕ)
=## = Example2
u_polar(r,ϕ)  = exp(-r) * sin(2ϕ)
Δu_polar(r,ϕ) = (1-1/r-2/r^2) * exp(-r) * sin(2ϕ)
# =#

degrees = [1,2,3,4,5]
divs = [1:5...] #[1:5..., 10, 15, 20]


#polar coordinates
radius(x,y) = sqrt.(x.^2 .+ y.^2)
angle(x,y) = atan.(y,x)

#transform to cartesian
u_analytic(x,y) = u_polar.( radius(x,y), angle(x,y) )
Δu_analytic(x,y) = Δu_polar.( radius(x,y), angle(x,y) )


function bestapproximation(spline::Union{BSpline2D, NURBS2D})

    #projection onto spline space
    h = meshsize(spline)
    t = [0 : h[1]/2 : 1 ...],[ 0 : h[1]/2 : 1 ...]
    α_u = LstSqProjector(spline, F, t, u_analytic)
    #α_Δu = LstSqProjector(spline, F, t, Δu_analytic)
    α_u = [α_u... ]; #α_Δu = [α_Δu... ];
    return α_u

end

#solve the problem with splines
function spline_poisson_solver(spline::Union{BSpline2D, NURBS2D})

    #assemble system matrices
    M = mass_matrix(spline,F)
    S = stiffness_matrix(spline,F)

    #boundary conditions
    bnd = [boundary(spline)...]
    int = .! bnd
    dof = sum(int)

    #projection onto spline space
    h = meshsize(spline)
    t = [0 : h[1]/2 : 1 ...],[ 0 : h[1]/2 : 1 ...]
    α_u = LstSqProjector(spline, F, t, u_analytic)
    α_Δu = LstSqProjector(spline, F, t, Δu_analytic)
    α_u = [α_u... ]; α_Δu = [α_Δu... ];


    A = S[:,int]
    L = M * α_Δu - S[:,bnd] * α_u[bnd]

    #solve
    nz = sum( A .!= 0 )
    u_interior = A \ L
    u = zeros(length(spline))
    u[int] = u_interior
    u[bnd] = α_u[bnd]
    #u = α_u
    return u,nz, dof
end


function compare_solutions(spline::Union{BSpline2D, NURBS2D},
    t, u_spline, u_true, relative=true)

    #evaluate spline solution
    B = evaluate(spline, t); x,y = t
    B = reshape(B, length(spline), length(x), length(y))
    u = sum(B .* u_spline, dims=1)[1,:,:]

    #calculate l2 error
    l2e = u .- u_true
    l2e = sum( l2e.^2 )
    if relative
        l2e = l2e / sum( u_true.^2 )
    end
    return √l2e
end

function bestanalysis(splinegenerator::Function, name="curvypoisson"; path = "examples/data/")
    println("Calculating true solution...")
    t = [0:.01:1 ... ],[0:.01:1 ... ]
    xy = evaluate(F, t)
    x,y = xy[1,:,:], xy[2,:,:]
    u_true = u_analytic(x,y)

    @showprogress .5 "bestanalysis '$name'" for p = degrees
        df = DataFrame()

        for n = divs
            spline = splinegenerator(n,p)
            u = bestapproximation(spline)
            #nz
            dof = sum( .! boundary(spline) )
            l2e = compare_solutions(spline, t, u, u_true)

            data = Dict(
                "h" => meshsize(spline)[1],
                "dof" => dof,
                #"nz" => nz,
                "l2e" => l2e
            )
            append!(df, data)
        end

        fname = path * name * "_p$p.csv"
        f = open(fname, "w"); close(f)
        CSV.write(fname, df)
    end
end

function analysis(splinegenerator::Function, name="curvypoisson"; path = "examples/data/")
    println("Calculating true solution...")
    t = [0:.01:1 ... ],[0:.01:1 ... ]
    xy = evaluate(F, t)
    x,y = xy[1,:,:], xy[2,:,:]
    u_true = u_analytic(x,y)

    @showprogress .5 "analysis '$name'" for p = degrees
        df = DataFrame()

        for n = divs
            spline = splinegenerator(n,p)
            u, nz, dof = spline_poisson_solver(spline)
            l2e = compare_solutions(spline, t, u, u_true)

            data = Dict(
                "h" => meshsize(spline)[1],
                "dof" => dof,
                "nz" => nz,
                "l2e" => l2e
            )
            append!(df, data)
        end

        fname = path * name * "_p$p.csv"
        f = open(fname, "w"); close(f)
        CSV.write(fname, df)
    end
end

#function main ()
    regular(n,p) = (s= RegularSpline([0:(1/n):1...], p); BSpline((s,s)))
    analysis(regular, "curvypoisson_regular")
    bestanalysis(regular, "curvypoisson_bestapprox")

    #piecew(n,p) = (s= PiecewisePoly([0:(1/n):1...], p); BSpline((s,s)))
    #analysis(piecew, "curvypoisson_piecew")

#end; main()
