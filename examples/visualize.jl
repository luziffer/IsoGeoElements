using PyPlot
using CSV

#colors = ["red", "orange", "yellow", "green", "blue"]
colors = ["tab:red","tab:orange","tab:green","tab:blue","tab:purple"]
function errorcurve(name::String; quantity=:dof, path = "examples/data/", kwargs...)
    for f in readdir(path)
        if startswith(f, name * "_p") && endswith(f, ".csv")
            println("plotting ", quantity, " for ", f)
            p = parse(Int, f[length(name)+3:end-4])
            df = CSV.read("$(path)$(f)")
            q = df[!, quantity]
            l2e = df[!, :l2e]

            plot(q, l2e; color = colors[p], kwargs...)
        end
    end

    if quantity == :h
        #gca().invert_xaxis()
        xlabel("mesh size h")
    elseif quantity == :dof
        xlabel("degrees of freedom")
    elseif quantity == :nz
        xlabel("non-zeros of stiffness matrix")
    end

    ylabel("relative L2-error")
    yscale("log"); xscale("log")
end


function visualize(names, labels, styles; imgname="visualize", outdir="imgs/")
    for q in [:h, :nz, :dof]

        clf(); title("Error comparison")
        _handles = [
            [ plot([0,0],[0,0], linestyle=s, color="black") for s in styles]...
            [ plot([0,0],[0,0], linestyle=(0,(0,100)), marker="o", color=c) for c in colors ]...
        ]

        _labels = [labels... , ["p=$(p)" for p in 1:length(colors) ]... ]


        for (name,style) in zip(names, styles)
            errorcurve(name; quantity=q, linestyle=style)
        end

        legend(_labels)
        savefig(outdir*imgname*"-$(string(q)).png")
        display(gcf());
    end
end


#straight
visualize(
    ["simplepoisson_piecew",
     "simplepoisson_regular",
     "simplepoisson_poly"],
    ["\$C^{0}\$-splines",
     "\$C^{p-1}\$-splines (fully regular)",
     "IP-FEM (Lagrange)" ],
    ["--", "-", ":"]; imgname = "simplepoisson"
)

#=
#only polynomials
visualize(
    ["simplepoisson_poly"],
    ["Isoparametric (Lagrange)" ],
    [":"]; imgname = "simplepoisson_lagrange"
)
=#


#curvy
visualize(
    ["curvypoisson_iga", "curvypoisson_poly"],
    ["IGA", "IPA" ],
    [":", "--"]; imgname = "curvypoisson" )
