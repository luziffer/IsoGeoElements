include("routines.jl")
using DataFrames: DataFrame
using Tensors: Tensor
using ProgressMeter: @showprogress
using CSV
using PyPlot


#=
Solve
   Δu = 1 in Ω
   u  = 0 on ∂Ω
on parametric domain Ω=(0,1)²
=#
function fourier_solution(x, y; n_fourier=100, grad=false)

    #coefficients of indicator function
    b = zeros(n_fourier)
    n = [1:n_fourier ... ]
    b[1:2:end] = 4 ./ (π .* n[1:2:end] )

    #inverse laplace operator on sine basis
    b =(b' .* b) ./ (π^2 .* ((n.^2)' .+ n.^2) )
    b = reshape(b, (size(b)..., 1, 1) )

    #evaluate Fourier basis
    ψ1 = sin.(π .* n .* x')
    ψ2 = sin.(π .* n .* y')
    ψ1 = reshape(ψ1, n_fourier, 1, length(x), 1)
    ψ2 = reshape(ψ2, 1, n_fourier, 1, length(y))
    ψ = ψ1 .* ψ2
    if grad
        ρ1 = π .* n  .* cos.(π .* n .* x')
        ρ2 = π .* n  .* cos.(π .* n .* y')
        ρ1 = reshape(ρ1, n_fourier, 1, length(x), 1)
        ρ2 = reshape(ρ2, 1, n_fourier, 1, length(y))
        d1ψ = ρ1 .* ψ2
        d2ψ = ψ1 .* ρ2
    end

    #linearly combine basis vectors
    u =  ψ .* b
    u = sum(u, dims=[1,2])
    u = u[1,1,:,:]
    if ~grad return u end

    d1u =  d1ψ .* b
    d1u = sum(d1u, dims=[1,2])
    d1u = d1u[1,1,:,:]
    d2u =  d2ψ .* b
    d2u = sum(d2u, dims=[1,2])
    d2u = d2u[1,1,:,:]

    du = zeros(2, size(d1u)... )
    du[1,:,:] .= d1u
    du[2,:,:] .= d2u

    return u, du
end


function l2error(dh, ip_fe, ip_geo, u; h1error=false)
    n_fourier = 250; n_eval = 50
    t = [0:1/n_eval:1 ... ], [0:1/n_eval:1 ... ]

    #debug
    x = repeat( [ t[1] ], n_eval+1 )
    x = hcat( x... )
    y = repeat( [ t[2] ], n_eval+1 )
    y = hcat( y... )'

    if ~h1error
        u_true = fourier_solution(t[1],t[2]; n_fourier=n_fourier)
    else
        u_true, grad_u_true =  fourier_solution(t[1],t[2]; n_fourier=n_fourier, grad=true)
    end

    #evaluate discrete function
    u_poly = zeros(n_eval+1, n_eval+1)
    @showprogress .1 "evaluating solution N=$(length(u))" for t1=1:n_eval+1, t2=1:n_eval+1
        ξ = Vec(t[1][t1],t[2][t2])
        value = evaluate_function(dh, ip_fe, ip_geo, u, ξ)
        u_poly[t1,t2] = value
    end

    #Calculate relative L2-error
    l2e = u_poly .- u_true
    l2e = sqrt( sum( l2e .^2 ) )
    l2e = l2e / sqrt( sum(u_true.^2) )
    if ~h1error return l2e end

    #evaluate discrete derivative
    grad_u_poly = zeros(2, n_eval+1, n_eval+1)
    @showprogress .1 "evaluating gradient N=$(length(u)) " for t1=1:n_eval+1, t2=1:n_eval+1
        ξ = Vec(t[1][t1],t[2][t2])
        grad = evaluate_gradient(dh, ip_fe, ip_geo, u, ξ)
        grad_u_poly[:, t1,t2] .= grad
    end

    #Calculate relative H1-error
    h1e = grad_u_poly .- grad_u_true
    h1e = sqrt( sum( h1e .^2 ) )
    h1e = h1e / sqrt( sum(grad_u_true.^2) )
    return l2e, h1e
end


function poisson(subdivisions::Int, p::Int)

    #parametric space
    if p==1
        generator = Quadrilateral
    elseif p==2
        generator = QuadraticQuadrilateral
    else
        error("Isoparametric domain for p=$p not implemented yet.")
    end
    grid = generate_grid(generator, (subdivisions,subdivisions))
    normalize_to_unit_interval(x) = ((x[1]+1)/2, (x[2]+1)/2)
    transform!(grid, normalize_to_unit_interval)

    #construct basis
    ip_fe = Lagrange{2, RefCube, p}()
    ip_geo  = Lagrange{2, RefCube, p}()
    qr = QuadratureRule{2, RefCube}(p+1)
    cellvalues = CellScalarValues(qr, ip_fe, ip_geo)

    #declare discretized functions
    dh = DofHandler(grid)
    push!(dh, :u, 1, ip_fe) #scalar field
    close!(dh)

    #boundary conditions
    ∂Ω = union( getfaceset.((grid,), ["left", "right", "bottom", "top"])... )
    ch = ConstraintHandler(dh)
    dbc = Dirichlet(:u, ∂Ω, (x,t) -> 0 )
    add!(ch, dbc)
    close!(ch)
    update!(ch, 0.0)

    #initialize system
    K = create_sparsity_pattern(dh)
    fill!(K.nzval, 1.0);
    nz = sum(K)
    fill!(K.nzval, 0.0);
    L = zeros(ndofs(dh))

    #solve variational problem
    f(x) = 1.
    K,L = doassemble(cellvalues, dh, K, L, f)
    apply!(K,L,ch)
    u = K \ L;

    #=
    #export to vtk
    vtk_grid("heat_equation", dh) do vtk
        vtk_point_data(vtk, dh, u)
    end
    =#

    h = meshsize(grid)
    l2e,h1e = l2error(dh, ip_fe, ip_geo, u; h1error=true)
    return h, l2e, h1e, ndofs(dh), nz
end


function analysis(name="simplepoisson"; path="examples/data/")
    clf(); title("Parametric Analysis")
    for p=[1,2]

        df = DataFrame()
        subdivisions =  [1,2,3,4,5,7,10,15,20,25,30,50]
        if p==1 append!(subdivisions, [75, 100, 150, 200]) end
        @showprogress .01 "analysis '$name' p=$p " for s = subdivisions
            h, l2e, h1e, dof, nz = poisson(s,p)
            data = Dict(
                "h" => h,
                "dof" => dof,
                "nz" => nz,
                "l2e" => l2e,
                "h1e" => h1e
            )
            append!(df, data)
        end

        plot(df[!, :dof], df[!, :l2e], label="p=$p, \$L^2\$-error")
        plot(df[!, :dof], df[!, :h1e], label="p=$p, \$H^1\$-error")

        #Save to file
        fname = path * name * "_p$p.csv"
        f = open(fname, "w"); close(f)
        CSV.write(fname, df)
    end
    legend()
    loglog()
    display(gcf())
end


#function main()
    #poisson(10, 1)
    analysis("superharmonic_juafem"; path="data/")
#end; main()
