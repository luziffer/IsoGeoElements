using JuAFEM, SparseArrays
using PyPlot

#distance of any two neighboring points
function meshsize(grid::JuAFEM.AbstractGrid)
    ndims = JuAFEM.getdim(grid)
    h = zeros(ndims)
    for dim = 1:ndims
        x = [ n.x[dim] for n in grid.nodes ]
        sort!(x)
        hx = maximum( x[2:end] .- x[1:end-1])
        h[dim] = hx
    end
    return maximum(h)
end

function doassemble(cellvalues, dh::DofHandler, K::SparseMatrixCSC, L::Vector, f::Function) where dim
    #local system
    n_base = getnbasefunctions(cellvalues)
    Ke = zeros(n_base, n_base)
    Le = zeros(n_base)

    #loop over cells
    assembler = start_assemble(K,L)
    for cell in CellIterator(dh)
        fill!(Ke, 0); fill!(Le, 0)
        reinit!(cellvalues, cell)
        coords = getcoordinates(cell)

        #loop over quadrature points
        for q_point = 1:getnquadpoints(cellvalues)
            dΩ = getdetJdV(cellvalues, q_point)

            coord = coords[q_point]

            #update force vector
            for i = 1:n_base
                v  = shape_value(cellvalues, q_point, i)
                Le[i] += v * f((coord[1], coord[2])) * dΩ

                #update stiffness matrix
                ∇v = shape_gradient(cellvalues, q_point, i)
                for j in 1:n_base
                    ∇u = shape_gradient(cellvalues, q_point, j)
                    Ke[i,j] += (∇v ⋅ ∇u) * dΩ
                end
            end
        end
        assemble!(assembler, celldofs(cell), Le, Ke)
    end
    return K,L
end

#=
function l2error(cellvalues::CellScalarValues{dim}, dh::DofHandler, ip_fe, ip_geo, u, u_true::Function) where dim
    n_base = getnbasefunctions(cellvalues)
    l2e = 0
    @inbounds for cell in CellIterator(dh)
        reinit!( cellvalues, cell )
        u_cell = celldofs(dh, cell)

        #loop over quadrature points
        for q_point = 1:getnquadpoints(cellvalues)
            dΩ = getdetJdV(cellvalues, q_point)
            v = function_value(cellvalues, q_point, u_cell)
            coords = getcoordinates(q_point)
            u = u_true( coords[1], coords[2] )
            l2e += abs(v - u) *dΩ
        end
    end
    return l2e
end
=#

function find_cell_containing_point(grid, point)
    #=
    for cell in 1:length(grid.cells)
        cell_coords  = getcoordinates(grid, cell)
        coords_vertices = cell_coords

        if is_point_inside_triangle(point, cell_coords[[1,2,3]]) || is_point_inside_triangle(point, cell_coords[[1,3,4]])
        #if is_point_inside_quad(point, cell_coords)
            return cell
        end
    end
    =#
    for cell in 1:length(grid.cells)
        cell_coords  = getcoordinates(grid, cell)
        coords_vertices = cell_coords

        if is_point_inside_quad(point, cell_coords)
            return cell
        end
    end


    function dist(cell)
        cell_coords = getcoordinates(grid, cell)
        m = sum(cell_coords) / length(cell_coords)
        d = sqrt( sum( (m .- point).^2 ) )
        return d,m
    end


    nearest = 1; distance, midpoint = dist(nearest)
    cell_coords = getcoordinates(grid, nearest)
    for cell in 2:length(grid.cells)
        new_distance, new_midpoint = dist(cell)
        if new_distance <= distance
            nearest = cell
            distance = new_distance
            midpoint = new_midpoint
        end
    end
    #println("did not find cell containing point $point, using cell $nearest at $midpoint")
    return nearest
end

function is_point_inside_quad(point, quad)
    parametric(x) = [sqrt(x[1]^2 + x[2]^2), atan(x[2] / x[1])]
    point = parametric(point)
    quad = [ parametric(q) for q in quad ]

    x_min = min( quad[1][1],quad[2][1],quad[3][1],quad[4][1] )
    x_max = max( quad[1][1],quad[2][1],quad[3][1],quad[4][1] )
    y_min = min( quad[1][2],quad[2][2],quad[3][2],quad[4][2] )
    y_max = max( quad[1][2],quad[2][2],quad[3][2],quad[4][2] )
    return (x_min ≤ point[1] ≤ x_max) &&  (y_min ≤ point[2] ≤ y_max)
end

function is_point_inside_triangle(point, triangle)
    return _is_point_inside_triangle((x = point[1], y = point[2]),
                              (x = triangle[1][1], y = triangle[1][2]),
                              (x = triangle[2][1], y = triangle[2][2]),
                              (x = triangle[3][1], y = triangle[3][2]))
end

function _is_point_inside_triangle(p, p0, p1, p2)
    dX = p.x-p2.x
    dY = p.y-p2.y
    dX21 = p2.x-p1.x
    dY12 = p1.y-p2.y
    D = dY12*(p0.x-p2.x) + dX21*(p0.y-p2.y)
    s = dY12*dX + dX21*dY
    t = (p2.y-p0.y)*dX + (p0.x-p2.x)*dY
    (D<=0) && return s<=0 && t<=0 && s+t>=D
    return s>=0 && t>=0 && s+t<=D
end


function find_local_coordinate(interpolation, cell_coordinates, global_coordinate)
    dim = length(global_coordinate)
    local_guess = zero(Vec{dim})
    n_basefuncs = getnbasefunctions(interpolation)
    max_iters = 100
    tol_norm = 1e-10
    for iter in 1:10
        if iter == max_iters
            error("did not find a local coordinate")
        end
        N = JuAFEM.value(interpolation, local_guess)

        global_guess = zero(Vec{dim})
        for j in 1:n_basefuncs
            global_guess += N[j] * cell_coordinates[j]
        end
        residual = global_guess - global_coordinate
        if norm(residual) <= tol_norm
            break
        end
        dNdξ = JuAFEM.derivative(interpolation, local_guess)
        J = zero(Tensor{2, 2})
        for j in 1:n_basefuncs
            J += cell_coordinates[j] ⊗ dNdξ[j]
        end
        local_guess -= inv(J) ⋅ residual
    end
    return local_guess
end

function evaluate_function(dh, ip_fe, ip_geo, u, point)
    grid = dh.grid
    cell = find_cell_containing_point(grid, point)
    cell_coords = getcoordinates(grid, cell)
    local_coordinate = find_local_coordinate(ip_geo, cell_coords, point)
    qr = QuadratureRule{2, RefCube, Float64}([1], [local_coordinate])
    fe_values = CellScalarValues(qr, ip_fe, ip_geo)
    reinit!(fe_values, cell_coords)

    nodes = grid.cells[cell].nodes
    dofs = celldofs(dh, cell)
    u = [ u[n] for n=dofs ]

    value = function_value(fe_values, 1, u)
    return value
end

function evaluate_gradient(dh, ip_fe, ip_geo, u, point)
    grid = dh.grid

    cell = find_cell_containing_point(grid, point)
    cell_coords = getcoordinates(grid, cell)
    local_coordinate = find_local_coordinate(ip_geo, cell_coords, point)
    qr = QuadratureRule{2, RefCube, Float64}([1], [local_coordinate])
    fe_values = CellScalarValues(qr, ip_fe, ip_geo)
    reinit!(fe_values, cell_coords)

    nodes = grid.cells[cell].nodes
    dofs = celldofs(dh, cell)
    u = [ u[n] for n=dofs ]

    gradient = function_gradient(fe_values, 1, u)
    return gradient
end

#=
#parametric space
function unit_box()
    grid = generate_grid(Quadrilateral, (N,N))
    normalize_to_unit_box(x) = ((x[1]+1)/2, (x[2]+1)/2)
    transform!(grid, normalize_to_unit_interval)
end

#physical space
function annulus(; grid=unit_box())
    annulus(x) = (x[1]+1) .* (cos(π/2 * x[2]), sin(π/2 * (x[2]+1)))
    transform!(grid, annulus)
    return grid
end


#switch between isoparametric and linear geometry
function geometric_interpolator(p::Int; iso=true)
    ip_fe = Lagrange{2, RefCube, p}()
    if iso
        ip_geo = Lagrange{2, RefCube, min(2,p)}()
        qr = QuadratureRule{2, RefCube}(p+2+min(2,p))
    else
        ip_geo  = Lagrange{2, RefCube, 1}()
        qr = QuadratureRule{2, RefCube}(p+1)
    end
    return ip_fe, ip_geo, qr
end
=#
