include("routines.jl")
using DataFrames: DataFrame
using Tensors: Tensor
using ProgressMeter: @showprogress
using CSV



r(x,y) = sqrt.(x .^2 .+ y .^2 )
ϕ(x,y) = atan.( y ./ x )
u_analytic(x,y) = (r(x,y).^2 .- 3 .* r(x,y) .+ 2) .* sin.(2. * ϕ(x,y))
Δu_analytic(x,y) = ( 8 .- 9 .* r(x,y) ) ./ ( r(x,y) .^2 ) .* sin.(2. * ϕ(x,y))

function ∇u_analytic(x,y)
    u_r = (2 .* r(x,y) .- 3) .* sin.(2. * ϕ(x,y))
    u_ϕ = (r(x,y).^2 .- 3 .* r(x,y) .+ 2) .* 2 .* cos.(2. * ϕ(x,y))

    u_x =  cos.( ϕ(x,y) ) .* u_r .- sin.(  ϕ(x,y) ) ./ r(x,y) .* u_ϕ
    u_y =  sin.( ϕ(x,y) ) .* u_r .+ cos.(  ϕ(x,y) ) ./ r(x,y) .* u_ϕ

    ∇u = zeros(2, size(u_x)... )
    ∇u[1,:,:] = u_x
    ∇u[2,:,:] = u_y
    return ∇u
end

function l2error(dh, ip_fe, ip_geo, u; h1error=false)
    n_eval = 300
    t = [0:1/n_eval:1 ... ], [0:1/n_eval:1 ... ]

    #evaluate analytical solution
    x = repeat( [ t[1] .+ 1 ], n_eval+1 )
    x = hcat( x... )
    y = repeat( [ π/2 .* t[2] ], n_eval+1 )
    y = hcat( y... )'
    x,y = x .* cos.(y), x .* sin.(y)

    u_true = u_analytic(x,y)
    if h1error
        grad_u_true = ∇u_analytic(x,y)
    end

    #evaluate polynomials
    u_poly = zeros(n_eval+1, n_eval+1)
    @showprogress .1 "evaluating solution N=$(length(u)) " for t1=1:n_eval+1, t2=1:n_eval+1
        _x,_y = (1+t[1][t1]) * cos(π/2 * t[2][t2]), (1+t[1][t1]) * sin(π/2 * t[2][t2])

        radius = sqrt(_x^2 + _y^2)
        if 1 > radius
            _x = _x / radius
            _y = _y / radius
        elseif radius > 2
            _x = _x / radius * 2
            _y = _y / radius * 2
        end
        @assert (1 <= sqrt(_x^2 + _y^2) <= 2) "Invalid test point."
        ξ = Vec(_x,_y)
        value = evaluate_function(dh, ip_fe, ip_geo, u, ξ)
        u_poly[t1,t2] = value
    end

    #Calculate relative L2-error
    l2e = u_poly .- u_true
    l2e = sum( l2e .^2 ) / sum(u_true.^2)
    l2e = sqrt( l2e )
    if ~h1error return l2e end

    #evaluate discrete derivative
    grad_u_poly = zeros(2, n_eval+1, n_eval+1)
    @showprogress .1 "evaluating gradient N=$(length(u)) " for t1=1:n_eval+1, t2=1:n_eval+1
        _x,_y = (1+t[1][t1]) * cos(π/2 * t[2][t2]), (1+t[1][t1]) * sin(π/2 * t[2][t2])

        radius = sqrt(_x^2 + _y^2)
        if 1 > radius
            _x = _x / radius
            _y = _y / radius
        elseif radius > 2
            _x = _x / radius * 2
            _y = _y / radius * 2
        end
        @assert (1 <= sqrt(_x^2 + _y^2) <= 2) "Invalid test point."
        ξ = Vec(_x,_y)
        grad = evaluate_gradient(dh, ip_fe, ip_geo, u, ξ)
        grad_u_poly[:, t1,t2] .= grad
    end

    #Calculate relative H1-error
    h1e = grad_u_poly .- grad_u_true
    h1e = sqrt( sum( h1e .^2 ) )
    h1e = h1e / sqrt( sum(grad_u_true.^2) )
    return l2e, h1e
end




function poisson(subdivisions::Int, p::Int)

    #domain
    if p==1
        generator = Quadrilateral
    elseif p==2
        generator = QuadraticQuadrilateral
    else
        error("Isoparametric domain for p=$p not implemented yet.")
    end
    grid = generate_grid(generator, (subdivisions,subdivisions))
    normalize_to_unit_interval(x) = ((x[1]+1)/2, (x[2]+1)/2)
    transform!(grid, normalize_to_unit_interval)
    bend_to_annulus(x) = ( (x[1]+1)*(cos(π/2 * x[2])), (x[1]+1)*(sin(π/2 * x[2])) )
    transform!(grid, bend_to_annulus)


    #construct basis
    ip_fe = Lagrange{2, RefCube, p}()
    ip_geo  = Lagrange{2, RefCube, p}()
    qr = QuadratureRule{2, RefCube}(p+1)
    cellvalues = CellScalarValues(qr, ip_fe, ip_geo)

    #declare discretized functions
    dh = DofHandler(grid)
    push!(dh, :u, 1, ip_fe) #scalar field
    close!(dh)

    #boundary conditions
    ∂Ω = union( getfaceset.((grid,), ["left", "right", "bottom", "top"])... )
    ch = ConstraintHandler(dh)
    dbc = Dirichlet(:u, ∂Ω, (x,t) -> 0 )
    add!(ch, dbc)
    close!(ch)
    update!(ch, 0.0)

    #initialize system
    K = create_sparsity_pattern(dh)
    fill!(K.nzval, 1.0);
    nz = sum(K)
    fill!(K.nzval, 0.0);
    L = zeros(ndofs(dh))

    #solve variational problem
    f(x) = Δu_analytic(x[1], x[2])
    K,L = doassemble(cellvalues, dh, K, L, f)
    apply!(K,L,ch)
    u = K \ L;

    #=
    #export to vtk
    vtk_grid("heat_equation", dh) do vtk
        vtk_point_data(vtk, dh, u)
    end
    =#

    h = meshsize(grid)
    l2e, h1e = l2error(dh, ip_fe, ip_geo, u; h1error=true)
    return h, l2e, h1e, ndofs(dh), nz
end


function analysis(name="curvypoisson"; path="examples/data/")
    clf(); title("Physical Analysis")
    for p=[1,2]

        df = DataFrame()
        subdivisions =  [1,2,3,4,5,7,10,15,20,25,30,50]
        if p==1 append!(subdivisions, [75, 100, 150, 200]) end
        @showprogress .01 "analysis '$name' p=$p " for s = subdivisions
            h, l2e, h1e, dof, nz = poisson(s,p)
            data = Dict(
                "h" => h,
                "dof" => dof,
                "nz" => nz,
                "l2e" => l2e,
                "h1e" => h1e
            )
            append!(df, data)
        end

        plot(df[!, :dof], df[!, :l2e], label="p=$p, \$L^2\$-error")
        plot(df[!, :dof], df[!, :h1e], label="p=$p, \$H^1\$-error")

        #Save to file
        fname = path * name * "_p$p.csv"
        f = open(fname, "w"); close(f)
        CSV.write(fname, df)
    end
    legend()
    loglog()
    display(gcf())
end


#function main()
    #poisson(10, 1)
    analysis("homogeneous_quarterannulus_juafem"; path="data/")
#end; main()
