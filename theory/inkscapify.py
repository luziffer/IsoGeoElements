#!/usr/bin/python3

import os 

svg_directories = [
	'imgs/',
	'../experiments/output/'
]

for d in svg_directories:
	for f in os.listdir(d):
		name,ext = os.path.splitext(f)
		if ext=='.svg': 
			
			cmd = 'inkscape -z -D' 
			cmd = cmd + ' --file=' + d + f
			cmd = cmd + ' --export-pdf=' + 'imgs/' + name + '.pdf'
			print(cmd)
			os.system(cmd)