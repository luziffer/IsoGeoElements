
\documentclass[]{article}
\usepackage[a4paper, total={6in, 8in}]{geometry}
%references
\usepackage[backend=bibtex,style=numeric,maxbibnames=9,maxcitenames=10]{biblatex}
\bibliography{sources.bib}

%symbols
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{gensymb}
\usepackage{marvosym}
\usepackage{dsfont}
\input{defs/mathmacros.tex}

\usepackage[bookmarks]{hyperref}
\usepackage{listings}
\usepackage{subcaption}
\usepackage{float}
\usepackage[makeroom]{cancel}

%svg images
\usepackage{graphicx}
\usepackage{svg}
\usepackage{xcolor}
\usepackage{tikz}
\svgsetup{}
\svgpath{{imgs/}{../experiments/output/}}
\graphicspath{{imgs/}{../experiments/output/}}
\graphicspath{{imgs/}}


%debugging command
\newcommand{\?}[1]{\color{red}#1\color{black}}
%\newcommand{\highlight}[1]{\colorbox{yellow!30}{#1}}


\setlength{\parindent}{0pt}
\begin{document}
%\hfill
\title{
	{Solving Elliptic PDE Problems with Isogeometric Methods}\\
	{\large Interdisciplinary Center for Scientific Computing (IWR) }\\
	{University of Heidelberg}\\ 
	\ \\
	\author{
		Luca Lenz \\
		Supervisors: 
		Robert Scheichl, 
		Chupeng Ma
	}
}

\maketitle
\ \\ \ \\ 

\begin{abstract}
Isoparametric Finite element Methods (IP-FEM) have been proven to be very useful for solving Partial Differential Equations (PDE) problems. They model both the function space as well as the domain of the PDE problems with piece-wise polynomials (PP). Splines are a very special class of rational PP which can be made arbitrarily smooth with great flexibility and precision. Especially important is the ability of non-uniform rational basic splines (NURBS) to represent conic geometries exactly. We will solve the second order elliptic PDEs by the Isogeometric Finite Element Methods (IG-FEM) using NURBS for the generation of basis functions and the geometry and  compare the convergence behaviour with the standard IP-FEM. The influence of the spatial regularity on the quality of the solution is also investigated.	
	

	%Denoting the degree by $p$ and the degrees of freedom 
	%It will be demonstrated how to improve the assembly of the Galerkin system matrices %in  $O(p^2 dN)$ shape function evaluations
	%if the function space has a tensor-product structure. (Instead of $O(pN^{2d})$ for a naive implementation, disregarding sparsity in the Galerkin matrix.)
\end{abstract}

\newpage

\tableofcontents

\newpage



%\begin{abstract} %yet tot follow %\end{abstract}
\input{defs/environments.tex}

\section{Introduction.}

\subsection{History}
\quad Mechanical splines existed since ancient times in some form of curved rulers and were mainly used for building ships. The numerical spline was discovered around 1940 by I. Schoenberg. He was the first to use the term B(asic)-Spline.
Its potential was recognized during the 1960's when spline technology became popular in computer-aided design (CAD) to model curves and curved surfaces. Specifically, there was a high demand for engineering cars at the time. Among others P. B\'{e}zier popularized the use of numerical splines in CAD.\cite{NURBShistory}
Finite element methods (FEM) had been popular in simulations for some years but at the time FEM demanded the domain to be tessellated into triangles or quadrilaterals with straight lines as boundaries.
While converting CAD models to a FEM setting is possible, and was done for a long time, it soon became clear that a unified framework could be simpler and possibly increase performance.  

\quad One decade later Isoparametric methods (IP-FEM) were invented as an extension to FEM to handle domains with curved geometry. In IP-FEM one uses polynomial mappings (instead of affine ones as before) from the reference element to the local element. This mapping transforms the straight lines of the reference element boundaries into interfaces of curved local elements. Note that this approach can only be efficient if the degree of the mapping is at most as large as the degree of the function space. Again, for most applications this is good enough but at the same time it was clear that there was still room for improvement. 
 
\quad In 1995 L. Piegl and W. Tiller published a book on NURBS algorithms. \cite{the_NURBS_book} 
While many spline algorithms had been used in proprietary CAD software before, NURBS technology was finally available to public and became common knowledge.
Another decade later Isogeometric methods (IG-FEM) were developed in 2005 by T.Hughes et al. as alternative to IP-FEM. \cite{iga_hughes_bazilevs} While conventional FEM and IP-FEM only allow  $h$-refinement (mesh size) and $p$-refinement (degree) the new IG-FEM can additionally perform $k$-refinement which controls regularity across element interfaces. Also IG-FEM are numerically stable for high polynomial degrees while in standard FEM the shape functions change their sign for $p \geq 3$ which can lead to stability issues. 


\quad The error analysis for IG-FEM follows the same ideas as the analysis for Vanilla FEM. We replace C\'ea's Lemma with Strang's Lemma which is a direct generalization. \cite{strang_variational_crimes} These lemmata bound the error of the PDE solution by the approximation error of representing a function in the discrete space. 
In Vanilla FEM the function representation is done using an interpolator which is constructed using the Lagrange basis. For IG-FEM the choice of such a discrete representation is highly non-unique, so we will only get a quasi-interpolator. 
It is possible to use a Lagrange basis as well. This can be found in the work of L. Schumaker. \cite{schumaker}
A. Buffa et al. have given general approximation results for quasi-interpolators based on dual basis approaches. \cite{veiga_buffa_sangalli} 
Last year E. Sanden et al. have published sharp bounds with explicit constants for quasi-interpolation using an $L^2$-projection. \cite{l2projection}

\quad Several software packages for IG-FEM have been developed. One of the first and most commonly used was GeoPDEs \cite{geopdes} which was written by R. Vasqu\'{e}z et al. in 2010. A few years ago a new software package called PyIGA \cite{pyiga} written by C. Hofreither has been released. 


%However, the Lagrange is more efficient wen using IP-FEM. We have given a detailed construction and error analysis for the most commonly used quasi-interpolants. 

\newpage

\subsection{Outline}
\quad In the first three sections, so sections 2,3 and 4, we will present an introduction to NURBS and discuss the construction of (rational) splines, their mathematical properties and use in geometric design. We will also fix the most important notations. \\

\quad The following section 5 is devoted to quasi-interpolation (which can be seen as a generalization of interpolation to projectors). We will define different quasi-interpolants that project functions onto spline spaces. In particular we will show how to prolongate polynomial spaces in B-spline basis. \\

\quad The next sections 6 and 7 will provide variational tools for solving PDEs and give some hands-on examples. Here we will compare IP-FEM and IG-FEM approaches. \\

\quad Finally, in section 8 it will be demonstrated how to exploit sparsity of the Galerkin matrix and use recursion properties of splines and to get efficient implementation strategies. In particular we can improve the assembly of the Galerkin system matrices to $O(dp^2 N)$ shape function evaluations if the function space has a tensor-product structure, instead of $O(pN^{2d})$ for a naive implementation, disregarding sparsity in the Galerkin matrix. Here, $d$ denotes the dimension of the domain, $p$ denotes the local polynomial degree and $N$ denotes the degrees of freedom.

\newpage


\section{Constructing Numerical Splines}
	\input{chapters/construction/univariate.tex}
	\input{chapters/construction/examples.tex}
	\input{chapters/construction/guarantees.tex}
	\input{chapters/construction/multivariate.tex}
\newpage 

\section{Geometric Modelling with Spline }
	\input{chapters/construction/refinement.tex}
	\input{chapters/construction/geometry.tex}
	\input{chapters/construction/meshes.tex}

\section{Regularity of Splines}
	\input{chapters/regularity/bentsobolev.tex}

\section{Approximating Functions with Splines}
	\input{chapters/approximate/lstsq.tex}
	\input{chapters/approximate/taylor.tex}
	\input{chapters/approximate/l2projector.tex}
	\input{chapters/approximate/bramblehilbert.tex}

\section{Spline Quadrature}
	\input{chapters/integrate/quadrature.tex}
	\input{chapters/integrate/gaussquad.tex}
	\input{chapters/integrate/systemtensors.tex}
\newpage

\section{Elliptic Differential Equations}
	\input{chapters/solvepde/galerkin.tex}
	\input{chapters/solvepde/diffusion.tex}
	\input{chapters/solvepde/poisson.tex}
	\input{chapters/solvepde/laplace.tex}

\section{Implementation Details}
	\input{chapters/implementation/evalstrategies.tex}	

\section{Conclusion}
	\input{chapters/conclusion/conclusion.tex}

\section{Bibliography}
	\printbibliography
\end{document}
