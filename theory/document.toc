\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction.}{3}{section.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}History}{3}{subsection.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}Outline}{4}{subsection.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Constructing Numerical Splines}{5}{section.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Univariate B(asic)-Spline}{5}{subsection.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Univariate Examples}{6}{subsection.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Useful Properties}{8}{subsection.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4}Higher Dimensional Splines}{9}{subsection.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Geometric Modelling with Spline }{11}{section.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Refinement and Coarsening}{11}{subsection.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Physical Space}{12}{subsection.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}Boundary}{15}{subsection.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4}Meshes}{15}{subsection.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Regularity of Splines}{16}{section.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Bent Spaces}{16}{subsection.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Approximating Functions with Splines}{17}{section.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}Least-Squares Projector}{17}{subsection.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2}Dual Projectors}{18}{subsection.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3}$L^2$-projector}{20}{subsection.5.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Spline Quadrature}{22}{section.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Gau{\ss } Quadrature}{22}{subsection.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}System Matrices}{24}{subsection.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Elliptic Differential Equations}{26}{section.7}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1}Galerkin Method}{26}{subsection.7.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2}Variational Crimes}{26}{subsection.7.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.3}PDE Examples}{28}{subsection.7.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8}Implementation Details}{36}{section.8}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.1}Truncated Division}{36}{subsection.8.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.2}Vectorized Evaluation}{36}{subsection.8.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.3}Cached Evaluation}{37}{subsection.8.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.4}System Matrix Loops}{37}{subsection.8.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.5}Software}{38}{subsection.8.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {9}Conclusion}{38}{section.9}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {10}Bibliography}{40}{section.10}%
