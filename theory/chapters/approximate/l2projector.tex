\subsection{$L^2$-projector}

Another quasi-interpolator is the $L^2$-projector. Given $f \in L^2(\Omega)$ 
we want to find $(\alpha_{j})_{j \in \{1 \dots N\}} \subset \R$ such that
\[
	\int_\Omega f R_i \ dx 
	\approx
	\int_\Omega \sum_j \alpha_j R_j R_i \ dx
	= \sum_j \alpha_j \int_\Omega R_j R_i \ dx
	\qquad \forall i \in \{1 \dots N\}
\]

The matrix $M \in \R^{N \times N}$ with entries $M_{ij}= \int_\Omega R_i R_j \ dx$ is the mass matrix. In the next few chapters we will discuss how it can be computed. The $L^2$ inner product with a basis function $\ell_i \approx \int_\Omega f R_i \ dx$ can be approximated with quadrature rules. They will be discussed in the next chapter as well. Then the $L^2$-projector solves the linear equation

\[
	\begin{pmatrix}
		M_{11} & \dots & M_{1N} \\
		\vdots & & \vdots \\
		M_{N1} & \dots & M_{NN} 
	\end{pmatrix}
	\begin{pmatrix}
	\alpha_1 \\ \vdots \\ \alpha_N
	\end{pmatrix}
	=
	\begin{pmatrix}
	\ell_1 \\ \vdots \\ \ell_N
	\end{pmatrix}
	\qquad \Leftrightarrow \quad
	M\alpha = \ell.
\]

The resulting quasi-interpolant is given by
\[
	\I: \ \ L^2(\Omega) \rightarrow \NURBS_p(\Xi)
	\quad f \mapsto \sum_i \alpha_i R_i.
\]
For this projector it is possible to give explicit error bounds as demonstrated in \cite{l2projection}. For us it suffices to show $\|f - \I_p f \| = O(h^{p+1})$ so we will use a simplified version of the error bound.

\begin{Theorem}[$L^2$-projection]
Let $f \in H^{r}(\hat \Omega)$ for $\hat \Omega = (a,b)$ and $r \leq p+1$. Then the approximation error of the $L^2$-projection is bounded by
\[
	\| f - \I_p f \|_{L^2(\hat \Omega)} < C h^{r} |f|_{r, \hat \Omega}
\]
where $C > 0$ is some constant that depends on $r,p$ and $\hat \Omega$.
\end{Theorem}
\begin{Proof}
We will use induction on the degree $p$: \\

First consider $p=0$. 
For $r=0$ simply set $C=\|\id-\I_0\|$.
Otherwise note that any spline $s \in \Spline_p(\xi)$ is constant when resticted to an element $s|_{ (\xi_i, \xi_{i+1}) }$. Thus the $L^2$-projector calculates the mean value
\[
	\I_0 f|_{ (\xi_i, \xi_{i+1}) } = \frac{1}{h_i} \int_{\xi_i}^{\xi_{i+1}} f(t) \ dt.
\]
Thus we can use the Poicar\'{e} inequality
\[
	\|f - \I_0 f \|_{ L^2(\xi_i, \xi_{i+1}) } \leq C h_i |f|_{1, (\xi_i, \xi_{i+1})}.
\]
Using this local estimation we can derive a global one
\[
\begin{split}
	\|f - \I_0 f \|_{L^2(\hat \Omega)}^2 &= \sum_{i \in \{1 \dots N\}} \|f - \I_0 f \|_{ L^2(\xi_i, \xi_{i+1}) }^2 \\
	&\leq \sum_{i \in \{1 \dots N\}} C h_i^2 |f|_{1, (\xi_i, \xi_{i+1})}^2 \\
	&\leq C h^2 \sum_{i \in \{1 \dots N\}}  |f|_{1, (\xi_i, \xi_{i+1})}^2
	= C h^2 |f|_{1, \hat \Omega}^2.
\end{split}
\]
Now assume the error bound holds for all degrees up to $p$. We will show it holds for $p+1$ as well. \\
Let $f(x) = K g(x) := \int_{\xi_i}^x g(t) \ dt$ be the integral of some $g \in H^{r-1}(\hat \Omega)$ and $K$ be the integral operator.  \\
Note that both $K \I_p g$ and $\I_{p+1} K g$  approximate $Kg$ but since $\I_{p+1}$ is the best approximation with respect to the $L^2$ norm we have
\[
\begin{split}
\|f - \I_{p+1} f\|_{L^2(\xi_i, \xi_{i+1})}
&= \| K g - \I_{p+1} K g \|_{L^2(\xi_i, \xi_{i+1})} \\
&\leq \| K g -  K \I_{p} g \|_{L^2(\xi_i, \xi_{i+1})} \\
&= \| K (\id - \I_{p}) g \|_{L^2(\xi_i, \xi_{i+1})} \\
&= \|K\| C h^{r-1} |g|_{r-1, (\xi_i, \xi_{i+1})} \\
&\leq h_i C h^{r-1} |\partial f|_{r-1, (\xi_i, \xi_{i+1})} \\
&\leq  C h^{r} |f|_{r, (\xi_i, \xi_{i+1})}.
\end{split}
\]
And therefore $\|f - \I_{p+1} f\|_{L^2(\hat \Omega)} \leq C h^r |f|_{r, \Omega}$ as desired.
\end{Proof}
		
\begin{Remark}
This proof is only for the parametric univariate case. If we assume that the physical map is exact, and therefore does not have to be refined, and exploit the tensor-product structure we can expect a similar bound for the multivariate physical case. The constant $C$ will change of course depending on which physical map we use. 
\end{Remark}
\newpage