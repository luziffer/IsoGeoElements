\subsection{Dual Projectors}
Let us examine some of these approach which are based on duality. We will first construct the projectors for univariate B-Splines and then explain how to generalize to physical NURBS.

\begin{Def}[Dual projector]
	Let $(\lambda_{i})_{i} \subset V^\ast$ be a basis of the dual space that is orthonormal on the splines, ie. $\forall i, j: \lambda_{i}( \hat B_j ) = \delta_{ij}$. Then we define the
	quasi-interpolant, or projection operator, onto the spline space by
	\[
	\I: \ \ V \rightarrow V_h, \quad v \mapsto \I v = \sum_i \lambda_i(v) \hat B_i.
	\]
\end{Def}

\begin{Remark}
	%and chose the projector $\I$ (sometimes also called quasi-interpolant) in the dual basis representation $\I v = \sum_i \lambda_i(v) N_i$ splines must be left unchanged by the projector. 
	Note that the orthonormality property implies that the quasi-interpolant restricted to the spline space acts like the identity
	\[ 	
	\implies \quad
	\I \big( \hat B_j \big) = \sum_i \lambda_{i}( \hat B_j) \hat B_i = \sum_i \delta_{ij}  \hat B_i =  \hat B_j.
	\]
	
	%A very useful property is the exact representation of polynomials with same degree as the basis splines. This  will allow us to use a Bramble-Hilbert type lemma for splines.
	
	%We will construct a dual basis based on the work of \?{Morken[??]}.	
	The choice of such a basis is still highly non-unique! 
	We will focus on the  dual basis which is like a local Taylor basis. This dual basis was proposed by Carl de Boor in \cite{bsplinebasics}.
	It is possible to give a rather simple error analysis for the resulting dual projector. 
	While it is possible to give an error analysis for general dual projectors (see for example \cite{veiga_buffa_sangalli}), these general error bounds are not sharp in most cases.
	%We will focus on the work of M{\o}rken \cite{}.	
	%Other examples can be found in Schumaker's book \cite{schumaker} but other approximation bound are needed as well.
	%Then we will derive an approximation bound. 
\end{Remark}

\begin{Lemma}[Marsden's identity] The translated monomials have the spline representation
	\[
	(\cdot - s)^{q} := \sum_{i \in \{0 \dots N\}} \psi_{i,q}(s) \hat B_{i,p} 
	\]
	where $s \in \R$ is the translation and $\psi_{i,q}(s) = (\xi_{i+1} - s) \cdots (\xi_{i+q} - s)$ is the coefficient function.
\end{Lemma}

\begin{Proof}
	We will give an inductive proof. 
	
	For $q=1$ this follows from Lemma \ref{Lemma:bspline_steps} and indeed it holds
	\[
	(\cdot - s)^0 = 1 = \sum_i \underset{=1}{\underbrace{\psi_{i,0}(s)}} \hat B_i.
	\]
	
	Otherwise we will use an inductive argument on both $q$ and $p$. Denote $w_{i,q}(t) := \frac{\xi_i - t}{\xi_{i+q} - \xi_i}$ and consider the spline $f = \sum_i \psi_{i,q+1}(s) \hat B_{i,p+1}$. Then Cox-de Boor's recursion formula relation admits
	
	\[ \begin{split}
	f \quad &= \sum_{i \in \{0.. N+p-q-1\}} \psi_{i,q+1}(s) \hat B_{i,p+1}  
	\qquad \qquad \qquad \qquad \qquad \qquad  \qquad
	\text{(recursion)} \\
	&= \sum_{i \in \{0.. N+p-q\}} \psi_{i,q+1}(s) ( w_{i,p}  \hat B_{i,p}  + (1-w_{i+1,p} \hat B_{i+1,p})) 
	\qquad \qquad \text{(index shift)}\\
	&= \sum_{i \in \{0.. N+p-q\}} \Big( \psi_{i,q+1}(s) w_{i,p} + \psi_{i-1,q+1}(s)(1-w_{i,p}) \Big) \hat B_{i,p} \\
	&= \sum_{i \in \{0.. N+p-q\}} \psi_{i,q}(s) \Big( (\xi_{i+q+1}-s) w_{i,p} + (\xi_{i}-s)(1-w_{i,p}) \Big) \hat B_{i,p} .
	\end{split}	
	\]
	
	Note that any linear function $g$ can be represented by $g = g(\xi_i) w_{i,q} + g(\xi_{i+q+1})(1-w_{i,q})$, in particular if we let $g$ be the translated monomial we get
	\[
	(\cdot - s) = (\xi_{i+q+1}-s) w_{i,q} + (\xi_{i-1}-s)(1-w_{i,q}).
	\]
	Assuming the statement holds for $p,q \in \N$ then we can conclude
	\[
	f \ \ = (\cdot - s) \sum_{i} \psi_{i,q}(s) \hat B_{i,p}
	= (\cdot - s)(\cdot - s)^{q} = (\cdot - s)^{q+1}.
	\]
\end{Proof}
\begin{Theorem}[Taylor dual basis] 
	%The dual basis based on translated polynomials is given by
	Let $f \in C^{p}(\hat \Omega)$. Then the dual basis corresponding to Taylor expansion is given as 
	\[ 
	\lambda_{i,p} f 
	= \sum_{q \in\{0 \dots p\}} \frac{ f^{(q)}(s) }{q!} \psi_{i,q} (s) 
	\]
	where $s \in \R$ and $\psi$ is the coefficient function from Marsden's identity. 
	Note that we can choose $s \in \R$ arbitrarily.  In particular we can choose an $s$ for each $i \in \{1 \dots N\}$ independently. 
	Let $Q_i = (\xi_i, \xi_{i+1}) \neq \emptyset$ be a non-trivial interval. %and $\tilde Q_i = (\xi_i, \xi_{i+p+1})$ its support  extension. 
	If $s \in Q_i$ the approximation error can be bounded by
	\[
		\| f - \mathcal I f \|_{L^{2}(Q_i)} \leq \| f - \mathcal I f \|_{L^{\infty}(Q_i)} \  \leq O (h_i^p) .
	\]
\end{Theorem}
\begin{Proof} %We will derive this this 
	Assume $f$ is a polynomial of degree $p$. The Taylor expansion around $s$ is given by
	\[	
	f \ = \sum_{q \in \{0 \dots p\} } \frac{ f^{(q)}(s) }{q!} (\cdot - s)^q
	= \sum_{i} \sum_{q} \frac{ f^{(q)}(s) }{q!} \psi_{i,q} (s) \hat B_{i,p}.
	\]
	At the same time we demand the spline interpolation to be exact as well, so
	$f \overset != \mathcal{I}f = \sum_i \lambda_{i,p}(f) \ \hat B_{i,p}$. Comparing coefficients yields
	\[
	\lambda_{i,p}(f) = \sum_{0 \leq q \leq p} \frac{ \psi_{i,q} (s) }{q!}  f^{(q)}(s).
	\]
	Let $t \in Q_i$, then the approximation error can be written in integral form and bounded by
	
	\[ \begin{split}
		|f(t) - \mathcal I f(t)|^2 \quad
		&= \Big|\int_s^t \frac{f^{(p+1)}(z)}{(p+1)!} (z-s)^p \ dz \Big|^2 \\	
		&\leq \frac{1}{(p+1)!} \int_s^t \Big| f^{(p+1)}(z) (z-s)^p \Big|^2 \ dz  \\	
		&\leq \frac{1}{(p+1)!} |t-s|^{2p} \int_s^t \Big| f^{(p+1)}(z) \Big|^2 \ dz  
		\quad \leq \quad \frac{h_i^{2p}}{(p+1)!} | f |_{p+1, Q_i}^2  .			
	\end{split}
	\]
	Provided the function has enough smoothness we can even bound the strong norm by
	\[ \begin{split}
		|f(t) - \mathcal I f(t)| \quad
		&= \Big|\int_s^t \frac{f^{(p+1)}(z)}{(p+1)!} (z-s)^p \ dz \Big| \\
		&\leq \frac{1}{(p+1)!} \int_s^t |f^{(p+1)}(z)| (z-s)^p \ dz \\
		&\leq \frac{h_i^p}{(p+1)!} \ \underset{\leq h_i}{\underbrace{|t-s|}} \ \| f^{(p+1)} \|_{L^\infty(Q_i)} 
		\quad \leq \ \ \frac{h_i^{p+1}}{(p+1)!} \ \| f^{(p+1)} \|_{L^\infty(Q_i)} .
		\end{split}
	\]
	Therefore the quasi-interpolation error is bounded by $\| f - \mathcal I f \|_{L^{2}(Q_i)} \leq C h_i^p | f |_{p+1, Q_i}$ or even \\
	$\| f - \mathcal I f \|_{L^{\infty}(Q_i)} \leq  C h_i^{p+1}\| f^{(p+1)} \|_{L^\infty(Q_i)}$ if we have enough smoothness. 
\end{Proof}

\begin{Remark}[Multivariate Taylor Quasi-interpolant] 
We have only constructed the Taylor dual basis in one dimension. We could generalize it to multivariate spaces as well. For spaces without tensor structure a Taylor quasi-interpolant can still be constructed. However, higher order derivatives become tensors. Thus it is a lot harder to express the Taylor expansion and also the dual basis.  \\
For simplicity we will just consider functions with tensor product structure. So if the multivariate function $f(t_1, t_2 \dots t_d) = f_1(t_1) f_2(t_2) \dots f_d(t_d) $ is a tensor product then we can use the Taylor dual basis on each dimension, so $f_k(t_k)$ yields coefficients $(\alpha_{i_k})_{i_k \in \{1 \dots N_k\}}$. Then we simply multiply the resulting coefficients $\alpha_i = \alpha_{i_1} \alpha_{i_2} \dots \alpha_{i_d}$. 

%If however $f$ is not a tensor product we will have difficulties representing the function with multivariate B-Spline because they are tensor products themselves.
\end{Remark}




%\begin{Def}[Truncated monomials]
%	Denote the truncated shifted monomials by
%	\[
%	(\cdot - s)_+^q: \quad \R \rightarrow \R_+
%	\qquad
%	t \mapsto (t - s)_+^q = \begin{cases}
%	(t-s)^q & \text{ if } t \geq s \\
%	0 & \text{ otherwise } 
%	\end{cases}
%	\]
%	%Note that $(\cdot-s)^0$ simply indicates whether the argument is positive.
%	We will often choose $s \in \{\zeta_1 \dots \zeta_n \}$ because then we can use a modified version of Marsden's identity.
%\end{Def}
%
%\begin{Corollary}
%	Using a slight modification of Marden's identity we can derive a representation for truncated monomials in spline basis which states
%	\[
%	(\cdot - \xi_j)_+^q = \sum_{i > j} \psi_{i,q}(s) \hat B_{i,p}
%	\]
%\end{Corollary}
%\begin{Proof}
%	First consider $t \geq \xi_j$. In this case we just have to verify that Marden's identity is not violated. In particular we have to show that the following terms vanish
%	\[
%	(t - \xi_j)_+^q = (t-x_j)^q = \underset{=0 \ ?}{\underbrace{\sum_{i \leq j} \psi_{i,q}(s) \hat B_{i,p}(t)}}   + \sum_{i > j} \psi_{i,q}(s) \hat B_{i,p}(t)
%	\]
%	For $i < j-p$ the basis splines $\hat B_{i,p}(t) = 0$ vanishes. If $i \in \{j-p-1, \dots j\}$ then the term $(t_j - s)$, which is a factor of $\psi(s)$, vanishes. Therefore these terms don't contribute to the sum. \\
%	
%	Now consider $t \leq \xi_j$. Then the truncated monomial vanishes. We have to show that the terms for $i \geq j$ vanish as well. To do so simply remember the support interval given in Lemma \ref{Lemma:bspline_steps} which states that $t \not\in \supp \hat B_i$ for $i \geq j$.
%\end{Proof}
%
%\begin{Def}[Divided Differences]
%Let $f \in C(\hat \Omega)$ be a function then we define the divided differences recursively by
%\[
%	f[\zeta_j] = f(\zeta_j) 
%	\qquad \text{ and } \quad 
%	f[\zeta_j \dots \zeta_{j+p+1}] 
%	  = \frac{f[\zeta_j \dots \zeta_{j+p}] - f[\zeta_{j+1} \dots \zeta_{j+p+1}]}
%	  {\zeta_{j+p+1}-\zeta_j}
%\]
%If two points are the same, ie. when we replace breakpoints $\zeta$ with the knot vector $\xi$ the recursion step is technically not defined. Instead we will interpret it as taking the limit as these two points approach each other. This means that we will either get a derivative or a discontinuity jump.
%\end{Def}
%
%\begin{Lemma}[Divided Difference Representation]
%The B-Spline can be represented by divided differences of the truncated monomials
%\[
%	\hat B_{i,p}(t) = (-1)^{p-1} (\xi_{i+p+1} - \xi_{i}) \ (t - \xi_i)^{p-1}_+[\xi_i \dots \xi_{i+p+1}]
%\]
%\end{Lemma}
%\begin{Proof}
%We will use induction on the degree $p$.
%If $p=0$ then  indeed
%\[ \begin{split}
%	(\xi_{i+1} - \xi_{i}) \ (t - \xi_i)^0_+[\xi_i,\xi_{i+1}]
%	&= (\xi_{i+1} - \xi_{i}) \frac{ (t - \xi_{i+1})^0_+ - (t - \xi_i)^0_+ }{\xi_{i+1} - \xi_i} \\
%	&= (t - \xi_{i+1})^0_+ - (t - \xi_i)^0_+ \\
%	&= \begin{cases}
%		-1 & \text{ if } t \in (\xi_{i}, \xi_{i+1})  \\
%		0 & \text{ otherwise}
%	\end{cases}
%\end{split} 
%\]
%\end{Proof}




%\begin{Theorem}[Peano Kernel Theorem]
%Let $E: L^2(\hat \Omega) \rightarrow \R$ be a functional that annihilates polynomials of order $p$, ie.
%\[
%E[r] = 0 \qquad \forall r \in \Poly_{p} \]
%Then we can apply the functional to any $f \in C^{p+1}(\hat \Omega)$ function by
%\[
%	E[f] = \frac{1}{p!} \int_{\hat \Omega} \ f^{(p+1)}(t) \ E[ (\cdot-t)^p_+ ] \ \ dt
%\]
%\end{Theorem}
%\begin{Proof}
%The proof is inspired by \cite{peanokernel} but we do not need the most general version.
%\end{Proof}



\begin{Remark}[Physical dual space]
	Remember that we have constructed the physical NURBS basis from parametric B-Splines. The transformation can be inverted like this
	\[
	R_i(x) = \frac{w_i \hat B_i}{W} \circ F^{-1}(x)
	\qquad \Rightarrow \qquad
	\hat B_i(t) = W(t) \frac{N_i \circ F(t)}{w_i}.
	\]
	Similarly we can construct a physical dual basis $\{\mu_i\}_{1 \leq i \leq N}$ from the inverted transformation by
	\[ 
	\mu_i(\ x \mapsto f(x)\ ) = \lambda_i \Big(\ t \mapsto \ W(t) \frac{ f \circ F (t) }{w_i} \ \Big).
	\]
\end{Remark}
