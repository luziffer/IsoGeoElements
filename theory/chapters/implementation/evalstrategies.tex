
\subsection{Truncated Division}
In the recursive B-Spline algorithm of Cox-deBoor we divide by increments of the knot vectors. If these become zero the algorithm might not be well-defined any more. However these singularities are cancelled out by the B-splines being zero. Sometimes the notation $\frac{0}{0}=0$ is used. \cite{veiga_buffa_sangalli} A possible implemention is $\epsilon$-truncation. We choose $\epsilon <\!\!< 1 $ very small and consider the trucated division function
\[
	\texttt{tdiv}: [0,\infty) \rightarrow [0,\infty) \qquad h \mapsto \begin{cases}
		\frac{1}{h} & \text{if } h > \epsilon \\
		\frac{1}{\epsilon} & \text{else}.
	\end{cases}
\]
which models the correct behaviour for $h \in \{0\} \cup [\epsilon, \infty)$ while it is continuous and easily implementable. Optimally we let $\epsilon$ be as small as the machine precision. 

\subsection{Vectorized Evaluation}
We want to evaluate $\{\hat B_i(t) \}_{i \in A, t \in T}$. For convenience we will just discuss univariate splines but note that the following concepts apply to multivariate splines as well.
Depending on the index set $A \subset \{1,2, \dots N\}$ we can use different vectorization strategies

\begin{itemize}
\item \textbf{Naive:} We recursively calculate $\hat B_{i,q}(t)$ from $\hat B_{i,q-1}(t)$ and $\hat B_{i,q-1}(t)$. This yields a triangular recursion structure which is illustrated below with arrows. If we do not store the previously computed values in a cache they will be lost which will lead to a massive computation overhead. While this evaluation strategy is easy to implement it is very inefficient.
\item \textbf{Full Vectorization:} If we simply calculate and store all $\{\hat B_{i,q-1}\}_{1 \leq i \leq N+p-q}$ we can vectorize the function calls which lifts the performance significantly when $|A| \approx N$ is large enough. In particular we can use this strategy when evaluating the physical map. 
\item \textbf{Ranged Vectorization: } When calculating the system matrices we need to evaluate neighboring splines. In this case $|A| \approx p <\!\!< N$ and the fully vectorized strategy becomes inefficient. The transparent boxes indicate unused evaluation points. This strategy assume the sparsity pattern $A = \{i_{min}, \ i_{min}+1, \ \dots, i_{max}-1,\  i_{max}\}$ with $i_{max}-i_{min} \approx p$.
\end{itemize}

\begin{figure}[H]
	\centering
	\def\svgwidth{1.1\columnwidth}
	\resizebox{\textwidth}{!}{\input{imgs/spline_evaluation_methods.pdf_tex}}
	\caption{Visualization of evaluation strategies. The naive strategy computes one spline evaluation for every arrow while the full vectorization strategy computes each line simultaneously. The ranged sparsity pattern $i_{min}=i$ and $i_{max}=i+1$ is highlighted as an example. }
\end{figure}

\begin{Remark}
Note that for NURBS we have a different sparsity pattern. Recall the supports 
\[
	\supp \hat N_i 
	= \supp \frac{w_i \hat B_i(t)}{W(t)}
	= \supp \hat B_i = [\xi_i, \xi_{i+p+1}]
	\quad \text{ and } \quad
	\supp W = \supp (\sum_i w_i \hat B_i ) = [a,b] 
\]
Then $\hat N_j(t)$ only has to be evaluated for $t \in [\xi_i, \xi_{j+p+1}]$. On the other hand evaluating $W(t)$ at $t \in [\xi_j, \xi_{j+p+1}]$ reduces to evaluating
\[
	W(t) = \sum_{1 \leq i \leq N} w_i \hat B_i(t)
		 = \color{gray} 0 + \color{black} \sum_{j-p \leq i \leq j} w_i \hat B_i(t) \color{gray} +  {0} 
\]
So for $( j_{min}, \dots j_{max})$ ranged vectorized NURBS evaluation we have to compute a  
$(i_{min}, \dots i_{max})$ ranged vectorized B-Spline evaluation first, with
\[
	i_{min} = \max\{ 1, j_{min}-p \} 
	\qquad \text{and} \qquad
	i_{max} = \min\{ N, j_{max}+p \}
\]
\end{Remark}

\subsection{Cached Evaluation}
When evaluating the B-Splines we use recursion which is rather expensive.
Recall that the derivatives use previous recursion steps. In case we a have composition of multiple order partial derivatives it is sensible to cache the previous steps and reuse them. Let us look at the gradient computation for an example

\[
	\nabla \hat B_i(t) \ = \ 
		\begin{pmatrix}
			\partial_1 \hat B_i(t_1, t_2, t_3) \\
			\partial_2 \hat B_i(t_1, t_2, t_3) \\
			\partial_3 \hat B_i(t_1, t_2, t_3) 
		\end{pmatrix}
		\ = \ \begin{pmatrix}
			\hat B_{i_1}'(t_1) \ \hat B_{i_2}(t_2)  \ \hat B_{i_3}(t_3) \\
			\hat B_{i_1}(t_1)  \ \hat B_{i_2}'(t_2) \ \hat B_{i_3}(t_3) \\
			\hat B_{i_1}(t_1)  \ \hat B_{i_2}(t_2)  \ \hat B_{i_3}'(t_3) 
		\end{pmatrix}
\]
Let us also assume that the degree $p >\!\!> 1$ is rather large.
Naively we would need $d^2 =  9$ full evaluations. If we precompute $\{\hat B_{i_k}(t_k), \hat B_{i_k}'(t_k)\}_{1 \leq k \leq d}$ this would reduce to $2d = 6$ evaluations. However, if we store the 
second last recursion step $\hat B_{i_k, p-1}$ we can get down to $d=3$ full evaluations. 


\subsection{System Matrix Loops}
For illustration we will just discuss the univariate mass matrix in this section. Again note that all the concepts can be applied to other system matrices as well. When calculating the system matrix $M=(M_{i_1, i_2})_{1 \leq i_1, i_2 \leq N}$ we have to somehow loop over the indices. To maintain efficiency we will exploit symmetry $M_{i_1,i_2} = M_{i_2, i_1}$ and simply demand $i_1 \leq i_2$. Consider two types of loops
\begin{itemize}
\item \textbf{knot indices}: 
one free index and other follows sparsity pattern, so 
\[
	1 \leq i_1 \leq N 
	\qquad \text{and} \qquad
	i_1 \leq i_2 \leq i_1 + p + 1
\]
Then in each iteration we can simply calculate the mass matrix entry
\[
	M_{i_1,i_2} = \int_{\Omega} \hat B_{i_1} \hat B_{i_2} \ dt = \int_{\xi_{i_2}}^{\xi_{i_1+p+1}} \hat B_{i_1} \hat B_{i_2} \ dt.
\]
using the appropriate quadrature rule.
\item \textbf{mesh indices}: 
loop over intervals between breakpoints $(\zeta_j, \zeta_{j+1})$ with $1 \leq j \leq n-1$ \\
Note that for any breakpoint index $j$ there exists a knot index $1 \leq i \leq N$ such that $\xi_i = \zeta_j$. Let us denote this mapping by $I(j)=i$ and define the knot vector range $(i_{min}, i_{max})$ by
\[
	i_{min} := I(j)-p 
	\qquad \text{and} \qquad
	i_{max} := I(j+1).
\]
Then the following integral is efficiently computed with quadrature and ranged vectorization
\[
	m^j_{i_1, i_2} = \int_{\zeta_j}^{\zeta_{j+1}} \hat B_{i_1} \hat B_{i_2} \ dt, 
	\qquad  \text{for} \ \ 
	i_{min} \leq i_1 \leq i_2 \leq i_{max}.
\]
We will let the other entries $(m^j_{i_1, i_2})_{i_1, i_2}$ be zero for convenience. Then we can write out the mass matrix computation loop as summation over these local integrals 
\[
	M_{i_1,i_2} = \sum_{1 \leq j \leq n-1} m^j_{i_1, i_2}.
\]
%First initialize the mass matrix with zeros. Then loop over the . 
\end{itemize}

Note that if the mesh is maximally regular, so all multiplicities are one, these two loops coincide. However, if the spline space has many irregularities the mesh indices are more efficient.

\subsection{Software}
In early papers GeoPDEs \cite{geopdes} was very popular. It is written for Octave and compatible with Matlab. Unfortunately by the time of this work the programming language Octave is no longer officially supported, and Matlab is neither open-source nore free. We have found that even the simple programs crash the Octave kernel for $N > 50$ degrees of freedom or polynomial degree $p > 3$ which make it unusable in practice. GeoPDEs derives its performance by using calls to C++ and Fortran code. This makes the code very incomprehensible. \\
The software package PetIGA \cite{petiga} is also a popular choice. It builds upon PetSc and provides a Python interface for mesh generation but uses C++ for matrix building routines. Changing between programming languages can be cumbersome. Also we found it rather difficult to specify non-constant boundary conditions and the documentation seems to be incomplete. \\
Instead we have used PyIGA \cite{pyiga} which was just developed in recent years. It integrates well into the SciPy ecosystem and the performance is derived from Cython calls, so there is no difficulties regarding multiple programming languages. Also we found that when using PyIGA the resulting programs were a lot shorter and more comprehensible than using GeoPDEs. 