

\subsection{PDE Examples}

\begin{Example} \label{superharmonic}
Consider the super-harmonic Poisson Problem on the unit box domain $\Omega = (0,1)^2$ with zero Dirichlet boundary conditions  
\[ \begin{split}
\Delta u = 1 & \quad \text{in } \Omega = (0,1)^2\\
u = 0 & \quad \text{on } \partial \Omega 
\end{split}\]

The Dirichlet boundary condition is absorbed into the search space which is $V = H^1_0(\Omega)$. We will derive an analytic solution using the Fourier series expansion
\[ 
	u(s,t) = \sum_{n \geq 1} \sum_{m \geq 1} a_{n,m} \sin(\pi ns) \sin(\pi m t).
\]

Then we get a Fourier series for the Laplacian, too
\[ 
	\Delta u(s,t) = - \sum_{n \geq 1}\sum_{m \geq 1} a_{n,m} \pi^2 (n^2 + m^2) \sin(\pi n s) \sin(\pi m t). 
\]

Examine the Fourier basis further. It is an orthogonal basis:

Keeping in mind $\cos(2x)=1 - 2\sin(x)^2$ the following integral can be calculated
\[ 
	\int_0^1 \sin(\pi n t)^2 \ dt = \int_0^1 \frac{ 1- \cos(2 \pi n t)}{2} \ dt
	= \frac 12 .
\]
Let $n > m$ and use the identity $2\sin(x)\sin(y)=\cos(x+y)-\cos(x-y)$ to get
\[ 
	\int_0^1 \sin(\pi n t) \sin(\pi m t) \ dt 
	= \int_0^1 \frac 12 \Big( \cos(\pi (n+m) t ) - \cos(\pi\underset{> 0}{\underbrace{(n-m)}}t) \Big) \ dt 
	= 0 .
\] 

We have shown that the one dimensional inner product of the Fourier basis is 
\[ 
	\int_0^1 \sin(\pi n t) \sin(\pi m t) \ dt 
	= \begin{cases}
		\frac 12  & \text{if } n=m \\
		  		0 & \text{if } n \neq m 
	\end{cases}
\]

Then the Fourier series of the indicator function $\mathds{1}_{(0,1)}$ is determined by coefficients $(b_n)_{n \in \N_+}$ with
\[
b_n = \frac{\int_0^1 \mathds{1}_{(0,1)}(t) \sin(\pi n t) \ dt}{\int_0^1 \sin(\pi n t)^2 \ dt } = - \frac 2 {\pi n} \cos(\pi n t) \big|_{t=0}^{t=1} = \frac{(1 - (-1)^n)}{ 2 n \pi} = \begin{cases}
\frac 4 {n \pi} & \text{if } n \text{ is odd} \\
0 & \text{otherwise}
\end{cases}
\] 

Note that $\mathds{1}_{(0,1)^2}(s,t) = \mathds{1}_{(0,1)}(s) \mathds{1}_{(0,1)}(t)$ is just the tensor product, and therefore the two-dimensional coefficients are just products of the one-dimensional coefficients as well
$b_{n,m} = b_n b_m$.

Finally we can compare coefficients and retrieve a formula for the solution
\[
a_{n,m} = - \frac{b_n b_m}{\pi^2 (n^2 + m^2)}  
= \begin{cases}
\frac{-16}{\pi^4nm(n^2 + m^2)} & \text{ if } n,m \text{ odd } \\
0 & \text{ otherwise } 
\end{cases}
\]

\begin{figure}[H]
	\centering
	\hspace{-55pt}
	\begin{subfigure}{.55\textwidth}
		\includegraphics[width=250pt]{imgs/simple_poisson_fourier_one.pdf}
	\end{subfigure}~
	%\hspace{-25pt}
	\begin{subfigure}{.55\textwidth}
		\centering
		\includegraphics[width=250pt]{imgs/simple_poisson_fourier.pdf}
	\end{subfigure}
	\caption{The Fourier approximation which is depicted here is used as reference solution.x}
\end{figure}

Mathematically we would have to use an infinite number of Fourier modes $n,m \in \N$ but 
in practice we have to truncate the modes, so $n,m \in \{1 \dots M\}$ for some $M \in \N$. 
We will see that this causes some errors in the analysis curves. Luckily the Fourier expansion converges (absolutely) with order $O(M^{-4})$ and therefore it can be still used as a reference solution. \\

Now let us focus on the spline approximation. The variational forms are 
\[ 
a(u,v) = \int_{\Omega} \nabla u \cdot \nabla v \ dx,
\qquad 
\ell(v) = \int_\Omega f v \ dx.
\]

The discretization is given by the conforming spline space $V_h = V \cap \Spline(\Xi) = V \cap \Spline(\xi^1) \otimes \Spline(\xi^2)$,
%Now we will give a represention of the space using multivariate splines $\{\hat B_i\}_{1 \leq i \leq n}$
in particular the boundary conditions are represented by
\[ \begin{split}
V_h &= \text{span}_{\R} \Big\{ 
\hat B_i : 1 \leq i \leq n \text{ st. } \hat B_i |_{\partial \Omega} = 0 
\Big\}  \\
&= \text{span}_{\R} \Big\{ t \mapsto \hat B_{i_1,p_1}(t_1) \hat B_{i_2,p_1}(t_2) \  
: 1 < i_1 < n_1, 1 < i_2 < n_2  \Big\} .
%\text{span}_{\R} \Big\{ \hat B_{0,p} \dots \hat B_{n,p} \Big\}
\end{split}
\]

We will use two different types of knot vectors and therefore different spline spaces. Either we will use continuous $C^{0}$-spline or fully regular, so $C^{p-1}$-splines. (Remember the regularity of the spline space can be modified by changing the multiplicity of the interior break points.)
Note that the problem is posed on a convex domain and therefore we know that the solution is arbitrarily regular. 
Therefore we expect similar convergence behaviour with the only difference that the fully regular splines will be more efficient. \\

To measure the quality of a solution we will use relative errors in $L^2(\Omega)$ norm and $H^1(\Omega)$ semi-norm. So if $u_h$ is a discrete solution and $u$ is the true solution the relative $L^2$ norm and and $H^1$ semi-norm error are given by
\[
	e^{rel}_{L^2} 
	= \frac{\|u_h - u\|_{L^2(\Omega)}}{\|u\|_{L^2(\Omega)}}
	= \sqrt{ \frac{ \int_{\Omega} (u_h - u)^2 \ dx }{\int_\Omega u^2 \ dx} },
	\qquad 
	e^{rel}_{H^1} 
	= \frac{|u_h - u|_{H^1(\Omega)}}{|u|_{H^1(\Omega)}} 
	= \sqrt{ \frac{ \int_{\Omega} |\nabla u_h - \nabla u|^2 \ dx }{\int_\Omega |\nabla u|^2 \ dx} }.
\]
The integrals can be approximated with quadrature rules. The advantage of this relative error norm (compared to an absolute error norm) is that errors made by the quadrature rules can be canceled out by the division. 
%We expect the relative error norm to be $e^{rel}_{L^2(\Omega)}<1$.

\begin{figure}[H]
	\centering
	\begin{subfigure}{.6\textwidth}
		\includegraphics[width=\textwidth]{imgs/superharmonic.h_vs_l2e.png}
	\end{subfigure} \\
	\begin{subfigure}{.6\textwidth}
		\hspace{-45pt}
		\includegraphics[width=\textwidth]{imgs/superharmonic.dof_vs_l2e.png}
	\end{subfigure}~
	\begin{subfigure}{.6\textwidth}
		\hspace{-45pt}
		\includegraphics[width=\textwidth]{imgs/superharmonic.nz_vs_l2e.png}
	\end{subfigure}
	\caption*{These plots show relative $L^2$-error curves for IG-FEM and IP-FEM solutions solving super-harmonic problem from Example \ref{superharmonic}. We have compared the discrete solutions with a Fourier series expansion that was truncated after $M=500$ modes.
	The effects of the truncation become visible once the relative $L^2$-error falls below $10^{-7}$. 
	For both FEM solutions the convergence rate improves with higher polynomial degree. 
	} 
\end{figure}


\begin{figure}[H]
	\centering
	\begin{subfigure}{.6\textwidth}
		\includegraphics[width=\textwidth]{imgs/superharmonic.h_vs_h1e.png}
	\end{subfigure} \\
	\begin{subfigure}{.6\textwidth}
		\hspace{-45pt}
		\includegraphics[width=\textwidth]{imgs/superharmonic.dof_vs_h1e.png}
	\end{subfigure}~
	\begin{subfigure}{.6\textwidth}
		\hspace{-45pt}
		\includegraphics[width=\textwidth]{imgs/superharmonic.nz_vs_h1e.png}
	\end{subfigure}
	\caption*{These plots show relative $H^1$ semi-norm error curves. Again the IG-FEM and IP-FEM solutions solving super-harmonic problem from Example \ref{superharmonic} are compared to a Fourier series expansion that was truncated after $M=500$ modes.
		The effects of the truncation become visible once the relative $H^1$-error falls below $10^{-3}$. And again for both FEM solutions the convergence rate improves with higher polynomial degree. 
	} 
\end{figure}

\end{Example}
\newpage