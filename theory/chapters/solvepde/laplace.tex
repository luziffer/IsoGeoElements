\begin{Example} \label{curvy_homogeneous}
Now we solve the Poisson equation on the annulus. In polar coordinates the domain is given by
\[
	\Omega = \Big\{ 
		r \begin{pmatrix} \cos(\phi) \\ \sin(\phi) \end{pmatrix} 
		\ \Big| \ 
		\text{with } r \in (1,2) \text{ and } \phi \in \big(0, \frac \pi 2\big)
		\Big\}.
\]
For the analytical solution we have to find a polar representation of the Laplace operator. 
To do so consider the polar map
\[
	\vartheta: (1,2) \times (0, \frac{\pi}2) \rightarrow \Omega,
		\qquad
		(r,\phi)\mapsto
		\begin{pmatrix} x \\ y \end{pmatrix} 
	 	= r \begin{pmatrix} \cos(\phi) \\ \sin(\phi) \end{pmatrix}. 
\]
First we will calculate the Jacobi matrix
\[
J = \begin{pmatrix}
\partial_r x & \partial_\phi x \\ 
\partial_r y & \partial_\phi y 
\end{pmatrix}
= \begin{pmatrix}
\cos(\phi) & -r \sin(\phi) \\ 
\sin(\phi) & r \cos(\phi) \\ 
\end{pmatrix}.
\]
Then we can calculate the partial derivatives with respect to the polar coordinates by the chain rule
\[
	\begin{pmatrix} \partial_r \\ \partial_\phi \end{pmatrix} u = J \nabla u
	\qquad \implies \quad 
	\begin{cases}
	\partial_r u(r,\phi) &= \quad\ \  \cos(\phi) \partial_x u + \ \ \sin(\phi) \partial_y u 
	\\
	\partial_\phi u(r,\phi) &= \ - r \sin(\phi) \partial_x u + r \cos(\phi) \partial_y u
	\end{cases}
\]
Thus the gradient can be expressed in polar coordinates as
\[
	\nabla u 
	= J^{-1} \begin{pmatrix} \partial_r \\ \partial_\phi \end{pmatrix} u
	= \begin{pmatrix}
	\cos(\phi) &  \sin(\phi) \\ 
	- \frac{1}{r}\sin(\phi) & \frac{1}{r}\cos(\phi) 
	\end{pmatrix}
	\begin{pmatrix} \partial_r \\ \partial_\phi \end{pmatrix} u.
\]
Then the second derivative with respect to the radius is
\[\begin{split}
	\implies \partial_r^2 u 
	&= \partial_r (\cos(\phi) \partial_x u + \sin(\phi) \partial_y u ) \\
	&= \cos(\phi) \partial_x \partial_r u + \sin(\phi) \partial_y \partial_r u \\
	&= \cos(\phi)^2 \ \partial_x^2 u + 2 \cos(\phi) \sin(\phi) \ \partial_x \partial_y u 
	+ \sin(\phi)^2 \ \partial_y^2 u .
\end{split}\]
And the second derivative with respect to the angle 
\[\begin{split}
\implies \partial_\phi^2 u 
&= \partial_\phi (-r\sin(\phi) \partial_x u + r\cos(\phi) \partial_y u ) \\
&= -r \cos(\phi) \partial_x u 
	- r \sin(\phi) \partial_x \partial_\phi u
	- r \sin(\phi) \partial_y u 
	+ r \cos(\phi) \partial_y \partial_\phi u \\
&= -r \Big( \cos(\phi) \partial_x u +  \sin(\phi) \partial_y u \Big) 
+ r^2 \Big( \sin(\phi)^2 \partial_x^2 u - 2\sin(\phi)\cos(\phi) \partial_x \partial_y u +\cos(\phi)^2 \partial_y^2 u \Big) \\
& = - r \partial_r u + r^2 \Big( \sin(\phi)^2 \partial_x^2 u - 2\sin(\phi)\cos(\phi) \partial_x \partial_y u +\cos(\phi)^2 \partial_y^2 u \Big) .
\end{split}\]
Putting these both together and keeping in mind the trigonometric identity $\cos(\phi)^2 + \sin(\phi)^2= 1$ yields a formula for the Laplacian
\[ 
	\partial_r^2 u + \frac 1 {r^2} \partial_\phi^2 u = - \frac 1 r \partial_r u + \partial_x^2 u + \partial_y^2 
	\qquad \implies \quad
	\Delta u = \partial_r^2 u + \frac 1 r \partial_r u + \frac 1 {r^2} \partial_\phi^2 u .
\]

Consider the ansatz functions given by 
\[	
	\forall k,n \in \N: \qquad \psi_{k,n}(r,\phi) := r^k \sin(n \phi) .
\]
Note that the Laplacian becomes a linear mapping on the ansatz space due to
\[ 
	\partial_\phi^2 \psi_{k,n} = - n^2 \ \psi_{k,n},
	\qquad 
	\partial_r \psi_{k,n} = k \ \psi_{k-1, n},
	\qquad
	\partial_r^2 \psi_{k,n} = k (k-1) \ \psi_{k-2, n}.
\]

Suppose the function $u$ is represented by these functions
\[
	u(r,\phi) = \sum_{k} \sum_{n} a_{k,n} r^k \sin(n \phi).
\]

%Let us consider an explicit example in polar coordinates
%\[ 
%	u(r,\phi) = r^2 (r-1)(2-r) \sin(2\phi)
%\]
%Indeed we have Dirichlet boundary conditions $u|_{\partial \Omega} = 0$ and the derivatives are
%\[ \begin{split}
%	\Rightarrow \partial_r u &= \Big(2r(r-1)(2-r) + r^2(2-r) - r^2(r-1)\Big) \sin(2\phi) \\
%				 &= r \Big( 9r-4r^2-4 \Big) \sin(2\phi) \\
%	\Rightarrow \partial_r^2 u &= \Big( (9r-4-4r^2) + r(9-8r) \Big) \sin(2\phi) \\
%				&= \Big( 18r-12r^2-4 \Big) \sin(2\phi) \\	
%\end{split} \]
%Then the Laplace operator can be written out as well
%\[ \begin{split}
%	\Delta u &= \partial_r^2 u + \frac{1}r \partial_r u + \frac1 {r^2} \partial_\phi^2 u \\
%	&= \Big( (18r -12r^2 -4) + (9r - 4r^2 - 4) - 2 (r-1)(2-r) \Big) \sin(2\phi) \\
%	&= \Big( 27r -16r^2 -8 + (2r^2 -6r + 4) \Big) \sin(2\phi) \\ 
%	&= \Big( 21r -14r^2 -4 \Big) \sin(2\phi) \\ 
%\end{split}\]

Let us consider an explicit example in polar coordinates
\[ 
	u(r,\phi) = (r-1)(r-2) \sin(2\phi).
\]
Indeed we have Dirichlet boundary conditions $u|_{\partial \Omega} = 0$ and the derivatives are
\[ \begin{split}
	\partial_r u &= ( 2r - 3 ) \sin(2\phi), \\
	\partial_r^2 u &= 2 \sin(2 \phi), \\
	\partial_\phi^2 u &= - 4 (r^2 - 3r + 2) \sin(2\phi).
\end{split}\]

Then the Laplace operator applied to $u$ is
\[
	\begin{split}
	\implies \Delta u &= \partial_r^2 u + \frac{1}r \partial_r u + \frac{1}{r^2} \partial_\phi^2 u \\
	&= \Big( 2 + \frac 1r (2r- 3) - \frac{4}{r^2}(r^2 - 3 r + 2) \Big) \sin(2\phi) \\
	&= \Big( 2 + 2 - \frac{3}{r} - 4 +  \frac{12}{r} + \frac{8}{r^2} \Big) \sin(2\phi) \\ 
	&= \frac{9 r - 8}{r^2} \sin(2\phi).
	\end{split}
\]

Again we will compare $C^0$ and $C^{p-1}$ splines and measure the errors in the $L^2$ norm and $H^1$ semi-norm. Also we will compare to IP-FEM solutions.\\

Note that the polar map $\vartheta$ is smooth and the analytical solution $u$ is smooth with respect to polar coordinates. Therefore the composition is smooth as well and the problem has high regularity.


\begin{figure}[H]
	\centering
	\begin{subfigure}{.6\textwidth}
		\includegraphics[width=\textwidth]{imgs/homogeneous_quarter_annulus.h_vs_l2e.png}
	\end{subfigure} \\
	\begin{subfigure}{.6\textwidth}
		\hspace{-45pt}
		\includegraphics[width=\textwidth]{imgs/homogeneous_quarter_annulus.dof_vs_l2e.png}
	\end{subfigure}~
	\begin{subfigure}{.6\textwidth}
		\hspace{-45pt}
		\includegraphics[width=\textwidth]{imgs/homogeneous_quarter_annulus.nz_vs_l2e.png}
	\end{subfigure}
	\caption*{These plots show relative $L^2$ error curves for IG-FEM and IP-FEM solutions solving the homogeneous PDE problem on the quarter annulus from Example \ref{curvy_homogeneous} For $p=1$ both the IP-FEM and IG-FEM don't represent the domain exactly and they both converge with the same rate. For $p>1$ however the IG-FEM gives an exact representation and therefore the convergence rates improve.}
\end{figure}


\begin{figure}[H]
	\centering
	\begin{subfigure}{.6\textwidth}
		\includegraphics[width=\textwidth]{imgs/homogeneous_quarter_annulus.h_vs_h1e.png}
	\end{subfigure} \\
	\begin{subfigure}{.6\textwidth}
		\hspace{-45pt}
		\includegraphics[width=\textwidth]{imgs/homogeneous_quarter_annulus.dof_vs_h1e.png}
	\end{subfigure}~
	\begin{subfigure}{.6\textwidth}
		\hspace{-45pt}
		\includegraphics[width=\textwidth]{imgs/homogeneous_quarter_annulus.nz_vs_h1e.png}
	\end{subfigure}
	\caption*{These plots show relative $H^1$ semi-norm error curves for IG-FEM and IP-FEM solutions solving the homogeneous PDE problem on the quarter annulus from Example \ref{curvy_homogeneous}. Again for higher degrees the convergence rates improve.}
\end{figure}


%\begin{figure}[H]
%	\centering
%	\includegraphics{imgs/geopdes_vs_dune.png}
%\end{figure}

%\[
%	u(r, \phi) = \sin\Big( \frac{1}{r-1+\epsilon} \Big) \sin(2\phi) \qquad \epsilon > 0
%\]
%
%\[ \begin{split}
%	\partial_r u &= -\frac{1}{r-1+\epsilon} \cos\Big( \frac{1}{r-1+\epsilon} \Big)  \sin(2\phi)  \\
%	\partial_r^2 u &= \Big(\frac{1}{r-1+\epsilon}\Big)^2  \Bigg(
%		\sin\Big( \frac{1}{r-1+\epsilon} \Big) + 2 \cos\Big( \frac{1}{r-1+\epsilon} \Big) 
%		\Bigg)  \sin(2\phi)  \\
%	\partial_\phi^2 u &= -2 \sin\Big( \frac{1}{r-1+\epsilon} \Big) \sin(2\phi)
%\end{split}\]
%\[ \begin{split}
%	\Delta u(r, \phi) &= \partial_r^2 u + \frac 1r \partial_r u + \frac{1}{r^2} \partial_r u \\
%	&= \Bigg(
%	\Big(\frac{1}{r-1+\epsilon}\Big)^2  \bigg(
%	\sin\Big( \frac{1}{r-1+\epsilon} \Big) + 2 \cos\Big( \frac{1}{r-1+\epsilon} \Big) 
%	\Bigg)
%	- \frac{1}{r(r-1+\epsilon)} \cos\Big( \frac{1}{r-1+\epsilon} \Big)  - 2 
%	\Bigg)\sin(2\phi) 
%\end{split}\]

\end{Example}
