For simplicity we will only consider stationary second order elliptic PDE even though Isogeometric Analysis could of course be used for other PDEs as well. 

\begin{Def}[Diffusion Equation] 
Consider the following partial differential equation for the function $u$. It is known as Diffusion Equation. 
\[
	\begin{split}
		- \nabla \cdot ( K \nabla u ) &= f \quad \text{in } \Omega \\
		u &=  g \quad \text{on } \partial \Omega_D \\ 
		\nu \cdot ( K \nabla u ) &= \rho \quad \text{on } \partial \Omega_N \\
	\end{split}
\]
We will assume the domain $\Omega \subset \R^d$ to be bounded and decompose the boundary $\partial \Omega  \ =\  \partial \Omega_D \ \cup \ \partial \Omega_N$ into a Dirichlet and a Neumann boundary respectively.
The matrix $K(x) = (K_{i,j}(x))_{i,j \leq d}$ is assumed to be elliptic, so $\xi^T K(x) \xi \geq \alpha |\xi|^2$ for any $x,\xi \in \R^d$ and some ellipticity constant $\alpha >0$. Furthermore we assume $K_{i,j} \in L^\infty(\Omega)$ are essentially bounded.
\end{Def}


\subsection{Galerkin Method}

\begin{Def}[Variational Problem]\label{def:vari}
Consider the variational setting of the problem
\[
	\text{Find } \ u \in V \ \text{s.t.} \ \forall v \in V:  a(u,v) = \ell(v).
\]
where the energy product $a$ is and the linear form $\ell$ are given by
\[ 
	a(u,v) = \int_\Omega K \nabla u \cdot \nabla v \ dx
	\qquad \text{and} \qquad
	\ell(v) = \int_{ \partial \Omega} f v \ dS.
\]
\end{Def}

\begin{Def}[Discretized Variational Problem] 
We will use the NURBS ansatz space 
\[
	V_h \ = \ 
	\vecspan_{\R} \{\phi_i \}_{1 \leq i \leq N} 
	\quad \text{ with } \quad
	\phi_i = \hat \phi_i \circ F \ 
	\text{ and }
	\ \hat \phi_i \in \NURBS_p(\Xi, w) 
\]
The variational problem \ref{def:vari} has the discrete approximation
\[
\text{Find } \ u_h \in V_h \ \text{s.t.} \ \forall v_h \in V_h:  a(u_h,v_h) = \ell(v_h).
\]
Calculate the system matrix $A_{ij} = a(\phi_i, \phi_j)$ and vector $L_j = \ell(\phi_j)$. Then the discrete solution $u_h$ can be found by solving a linear equation
\[
	A x = L \qquad \implies \qquad u_h = \sum_{0 \leq i \leq N} x_i \phi_i \in V_h .
\]
We call such a method an Isogeometric Elements Method when we are discretizing both $V_h$ and $\Omega$ with the same spline space $\NURBS(\Xi)$. 
\end{Def}

\subsection{Variational Crimes} 
In reality $a$ and $l$ cannot be calculated exactly. However we insist on using quadrature rules and get new forms $a_h$ and $\ell_h$. Using these new forms $a_h,\ell_h$ instead of $a,\ell$ is considered a variational crime in literature. \cite{strang_variational_crimes}
The NURBS crimes mainly consist of
\begin{itemize}
\item \textbf{inexact quadrature:}
if $\{\phi_i\}_i$ are not polynomial we often don't know the exact integration, but we insist on using Gaussian quadrature nonetheless (for efficiency reasons)

\item \textbf{quasi-interpolation:}
projecting the right hand side's function representation on spline space
\[
	\ell(v) = \int_\Omega f v \ dx
	\qquad \Rightarrow \qquad
	\ell_h(v_h) = \int_\Omega (\I_h f) v_h \ dx  
\]
\end{itemize}

Keep in mind that quadrature rules might not be defined for $L^2(\Omega)$-functions! 
Thus the new forms $a_h$ and $\ell_h$ are only defined on the discrete space $V_h$ but not necessarily on $V$. 

\begin{Def}
We will interpret $\{a_h\}_{h > 0}$ as families of bilinear forms on finite dimensional approximation spaces $V_h \subset V$.
We say these bilinar forms are uniformly coercive if 
\[
	\min_{h > 0} \min_{v \in V_h} a_h(v,v) \geq \alpha \|v\|^2 
\]
and uniformly bounded if
\[
	\max_{h > 0} \max_{v,w \in V_h} \frac{a_h(v, w)}{\|v\|\|w\|} \leq c \|v\| \|w\|
\]
independent of the mesh size $h$. 
Similarly we can define what it means for a family of linear forms $\{\ell_h\}_{h > 0}$ to be uniformly bounded.
For such uniform families we can find approximation bounds. These will used to find a replacement of C\'{e}a's lemma.
\end{Def}


\begin{Theorem}[Strang's Lemma]
Let $\{\ell_h\}_{h > 0}$ be uniformly bounded and let $\{a_h\}_{h > 0}$ be both uniformly coercive and uniformly bounded. 
Then the approximation error is bounded by
\[
	\|u - u_h \| \leq C \Bigg( 
		\inf_{v_h \in V_h} \Big( 
		\|u-v_h\| 
			+ \sup_{w_h \in V_h} \frac{|a_h(u, w_h) - a(u, w_h ) |}{ \|w_h\| }
		\Big)
		+ \sup_{w_h \in V_h} \frac{| \langle \ell_h, w_h \rangle - \langle \ell , w_h \rangle |}{ \|w_h\| }
		\Bigg).
\]
\end{Theorem}
\begin{Proof}
Let $v_h \in V_h$ and denote the error to the discrete solution by $w_h = u_h - v_h$.
\[
\begin{split}
	\alpha \| w_h \|^2 
	&\leq a_h(w_h, w_h) = a_h(u_h - v_h, w_h) \\
	&= a(u - v_h, w_h) + \Big( a_h(u_h,w_h) - a(u, w_h) \Big) + \Big( a(v_h, w_h) - a_h(v_h,w_h) \Big) \\
	&= a(u - v_h, w_h) + \Big( \langle \ell_h,w_h \rangle - \langle \ell, w_h \rangle \Big) + \Big( a(v_h, w_h) - a_h(v_h,w_h). \Big) \\
\end{split}
\]
Assume $v_h \neq u_h$ and divide by $\|w_h\| > 0$. Then the error is bounded by 
\[ \begin{split}
\implies \|  u_h - v_h \| 
&\leq 
\frac 1 \alpha \Bigg( \frac{ a(u - v_h,w_h)|}{\| w_h\|}
+ \frac{|a_h(u, w_h) - a(u, w_h ) |}{ \|w_h\| }
+ \frac{| \langle \ell_h, w_h \rangle - \langle \ell , w_h \rangle |}{ \|w_h\| }  \Bigg) \\
&\leq \frac 1 \alpha \Bigg(  c\| u - v_h \|
+ \frac{|a_h(u, w_h) - a(u, w_h ) |}{ \|w_h\| }
+ \frac{| \langle \ell_h, w_h \rangle - \langle \ell , w_h \rangle |}{ \|w_h\| }  \Bigg).
\end{split}
\] 
Using the triangle inequality we get
\[
 \begin{split}
	\|u-u_h\| &\leq \| u - v_h \| + \|v_h - u_h \| \\
	&\leq (1 + \frac c \alpha) \| u - v_h \|
	+ \frac{1}\alpha \frac{|a_h(u, w_h) - a(u, w_h ) |}{ \|w_h\| }
	+ \frac{1}\alpha \frac{| \langle \ell_h, w_h \rangle - \langle \ell , w_h \rangle |}{ \|w_h\| } \\
	&\leq C \Bigg( 
	\inf_{v_h \in V_h} \Big( 
	\|u-v_h\| 
	+ \sup_{w_h \in V_h} \frac{|a_h(u, w_h) - a(u, w_h ) |}{ \|w_h\| }
	\Big)
	+ \sup_{w_h \in V_h} \frac{| \langle \ell_h, w_h \rangle - \langle \ell , w_h \rangle |}{ \|w_h\| }
	\Bigg).
\end{split}
\]
Note if $a_h = a, \ell_h = \ell$ we get $C=1+\frac{c}\alpha$ which is exactly the constant from C\'{e}a's Lemma.
\end{Proof}

%The Gau{\ss} quadrature formula is exact for polynomials, therefore it is also exact for splines if the grid is fully contained in the support.
%Assuming we have a representation for $\ell$ in $V_h$, which is exists due to Riesz theorem, the Galerkin matrix $A_{ij} = a(\phi_i, \phi_j)$ and vector %$L_j = \ell(\phi_j)$ have terms of the form


%We have to scale the grid to $\{\hat t_j\}_{0\leq j \leq {2p+q}} \subset Q_i\cap Q_j$ which might vanish. In particular if we have the product of splines the supports have to be intersected.


%\paragraph{Source Term}
%If we assume $f \in \Poly_{k}$ for some $k \geq 0$ we can proceed analogously %to the matrix assembly and construct the 

