In this section we will recapitulate the important properties of standard one-dimensional basis splines on the unit interval to fix some notation. 
Then we show how to extend the one-dimensional spline to multiple dimensions by introducing the tensor product space of multivariate splines. 

\subsection{Univariate B(asic)-Spline}
First we will define the parametric space $\hat \Omega$ which generalizes the notion of a reference element. We start in one dimension with an interval $\hat \Omega=(a,b)$. For some cases an infinite basis mesh $\hat \Omega =(-\infty,\infty) = \R$ might be useful as well but we will assume $\hat \Omega = (a,b)=(0,1)$ by default.  
To define the reference splines $\hat \{B_{i,p}\}_{i \in \{1 \dots N\}}$ we have to introduce the knot vector $\xi$. 

\begin{Def}[Knot vectors]
Let $p \in \N$ denote the degree. 
Let the breakpoints 
\[
a = \zeta_1 < \zeta_2  < ... < \zeta_n = b
\]
form a partition of the mesh $\hat \Omega$. Then a knot vector $\xi$ is constructed by
\[
\xi \ \ =\ \  (\xi_1 \dots \xi_{N+p+1}) \ =\ (
\underset{m_1 \text{ times}}{\underbrace{ \zeta_1 \dots \zeta_1}}, \ \ 
\underset{m_2 \text{ times}}{\underbrace{ \zeta_2 \dots \zeta_2}}, 
\  \dots \ 
%\underset{m_{n-1} \text{ times}}{\underbrace{ \zeta_{n-1}, .., \zeta_{n-1}}}, \ \ 
\underset{m_n \text{ times}}{\underbrace{ \zeta_n \dots \zeta_n }}
).
\]
We will denote the mesh size as 
\[ 	
\forall j \in \{1 \dots n-1\}: \  \ 
h_j = \zeta_{j+1} - \zeta_{j} > 0 
\qquad \text{and} \qquad
h := \!\! \max_{j \in \{1, \dots n-1\} } \!\! h_j.
%\forall{p-1 \leq i \leq n}: \qquad h_i = \xi_{i+p+1} - \xi_{i-p+1} > 0
%\forall { i \in \{0 \dots N\} }: \ \ h_i = \xi_{i+p+1} - \xi_{i} > 0
%\qquad \text{and} \qquad h = \!\! \max_{i \in \{0, \dots N\} } \!\! h_i 
\]
If the multiplicities $(m_j)_{j \in \{1 \dots n\} } \subset \N$ obey $m_1 = m_{n} = p+1$ and $ m_j \leq p+1$
then the knot vector $\xi$ is said to be $p$-open. If $\xi$ is $p$-open we can define the support size 
\[
	\tilde h_i = \xi_{i+p+1} - \xi_{i} > 0.
\]
\end{Def}

\begin{Remark}
Here $n \in \N$ denotes the number of breakpoints and $N \in \N$ denotes the degrees of freedom. 
These are related by 
\[
	2(p+1) + n 
	\quad \leq \quad 
	N+p+1 = \!\!\!\! \sum_{j \in \{1 \dots n\}} \!\!\!\! m_j 
	\quad \leq \quad 
	2(p+1)n 
\]
If the breakpoints are equidistantly distributed then $h \sim \frac{1}{n}$.
Later on we will see how in practice either $h$ or $N$ can be used for convergence analysis.
\end{Remark}

\begin{Remark}[Alternative knot vectors]
As long as $\xi_1 \leq \xi_2 \leq \dots$ is ordered we are guaranteed numerical stability as we will see in Lemma \ref{Lemma:bspline_steps}. Thus any knot vector $\xi = \{\xi_1, \xi_2 \dots \} \subset \R$ could be used.  
However, the condition $\forall i \in \{1 \dots N\}: \ \xi_{i+p+1} > \xi_i \Leftrightarrow \forall j \in \{1 \dots n\}: \ m_j \leq p+1$ is crucial for constructing linear independent basis functions as we will see in Lemma \ref{Lemma:bspline_basis}.


Periodic boundary conditions $1=\zeta_{n+1}\equiv\zeta_0=0$ can be used as well. These are very popular in applications. The same geometry can be achieved with $p$-open knot vectors by simply splitting the domain into a multi-patch domain. For the sake of simplicity we will only consider parametric domains $\hat \Omega = [0,1]$ and $p$-open knot vectors $\xi$.
\end{Remark}

% and would describe different spaces. Also other regularity conditions (eg. $C^k$-regularity by $m_j \leq p+1-k$ for $k \in \N$) can be imposed which we will discuss later on in more detail. However for the sake of simplicity in this work only $p$-open knot vectors will be considered.  
	
%In order for the mesh to be non-degenerate and we demand for the degree $p \in \N$ and the multiplicities $(m_j)_{1 \leq j \leq n}$ to obey $m_0 = m_{n} = p+1$ and $ m_j \in\{0,1, .., p+1\}$. 


\begin{Algo}[Cox-de-Boor] 
The B-splines can be evaluated using the Cox-de-Boor algorithm which also constructs the B-Splines recursively at $t \in \hat \Omega = [0,1]$
\[ \begin{split}
	q=0\quad: \quad & 
			\forall i \in \{1, \dots, N+p\}: \ 
			\hat B_{i,0} = \mathds{1}_{[\xi_i,  \xi_{i+1})}, 
		\qquad \text{and } \ \ 
			\hat B_{n+p,0} = \mathds{1}_{[\xi_{N+p+1},1]} \\
	\forall 1 \!\! \leq \! q \! \leq \! p : \quad &
			\hat B_{i,q}(t) = \frac{t-\xi_{i}}{ \xi_{i+q} - \xi_{i} }\hat B_{i,q-1}(t) + 
			\frac{\xi_{i+p+1}-t}{\xi_{i+p+1}-\xi_{i+1}} 
			\hat B_{i+1, q-1}(t) 
		\qquad \qquad \\  
\end{split} \]
where $1 \leq i \leq N+p-q$ is the index of the B-spline at recursion step $q$ and indicator function $\mathds 1$  
 \[
 	\mathds 1_A(t) = \begin{cases} 1 \text{ if } t\in A \\ 0 \text{ if } t \notin A \end{cases}.
\]

For simplicity we will denote the resulting basis splines by
$\hat B_i \equiv \hat B_{i,p}$ which is the result of the last iteration.
The spline space $\Spline$ is defined as the space spanned by the basis functions which are constructed from the knot vector $\xi$. To emphasize this dependency we describe the spline space by the following notation 
\[
	\Spline(\xi) := \text{span}_{\R}\{\hat B_{1} \cdots \hat B_{N} \} .
\]
We will sometimes call $i \in \{1, \dots, N\}$ the degrees of freedom. Indeed we will see $\dim_\R \Spline(\xi) = N$.
\end{Algo}

