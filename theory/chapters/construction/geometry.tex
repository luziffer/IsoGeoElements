\subsection{Physical Space}
While the parametric domain	$\hat \Omega = (0,1)^d$ was useful for constructing the spline space $\NURBS(\Xi)$, we are still limited to the unit box but are interested in domains with curvature. 

\begin{Def}[Physical mapping]
Let $F: [0,1]^d \rightarrow \R^n$ be a continuous mapping such that embedding dimension $n \geq d$ is at least as big as the dimension of our parametric space. We will say $F$ is a physical mapping if it is a diffeomorphism, which means $F$ is invertible and both $F, F^{-1}$ are continuously differentiable.
The physical splines are the pullback of the parametric splines onto the physical domain
\[
	R_i = \hat R_i \circ F^{-1}\ \ : \ \  \Omega \rightarrow \R \qquad \quad \forall i \in \{ 1 \dots  N \}
 	%V_h = \vecspan \{ R_i = \hat v \circ F^{-1} | \hat v \in \NURBS(\xi) \}
\]
If $F$ is a spline itself, we speak of an isogeometric spline space meaning both the shape functions and the geomtry of the space are modeled with splines. A spline physical map $F$ is represented by 
\[
	F: \ \  \hat \Omega \rightarrow \R^D, \quad  t \ \rightarrow \ x = \sum_i c_i \hat R_i(t).
\]
The control points $c = \{c_i\}_{1 \leq i\leq N} = \{c_{i_1 \dots i_d} \}_{1 \leq k \leq d, 1 \leq i_k \leq N_k} \subset \R^n$ act like a sequence of coefficients for each dimension. 

%are all distinct and have to be placed in a certain order for $F$ to be a homeomorphism.
%While functions on the parametric domain are described by a spline space $\NURBS(\Xi)$ we will use the pullpack onto the 

\end{Def}
\begin{Remark}
While we know the inverse physical map $F^{-1}$ exists we cannot compute it in most cases. 
In particular if $F$ is a spline physical map we would have to invert spline functions which cannot be done easily. 
\end{Remark}

%on the parametric domain $\hat \Omega = (0,1)^d$ and control points $c = \{c_i\}_{0 \leq i\leq N} = \{c_{i_1 \dots i_d} \}_{0 \leq i_k \leq N_k} \subset \R^D$ with $D \geq d$ we can define the physical map $F$ in a spline basis as well
%The physical domain $\Omega$ is defined as the image of the parametric space, so $\Omega := F(\hat \Omega)$. We assume that $F: \hat \Omega \overset{\sim}\rightarrow \Omega$ is a homeomorphism. %and denote the inverse as $G = F^{-1}$.
%Given parametric basis functions $\hat \phi_i \in \NURBS(\Xi)$ we can define the physical basis functions by $\phi_i = \hat \phi_i \circ G$. 


\begin{Example} 
	\label{Ex:quarterarc}
	The $90^\circ$ arc can be represented as curve which is given by the parameters
	\[
	\xi = \{0,0,0,1,1,1\}, \quad
	c = \Bigg\{ 
	\begin{pmatrix} 1 \\ 0 \end{pmatrix},
	\begin{pmatrix} 1 \\ 1 \end{pmatrix},
	\begin{pmatrix} 0 \\ 1 \end{pmatrix}
	\Bigg\},
	\quad w = \Big\{ 1, \ \frac1{\sqrt 2 }, \ 1 \Big\}.
	\]
\end{Example}

\begin{Theorem}
	The physical map $F$ from Example \ref{Ex:quarterarc} parameterizes the quarter annulus exactly.
	However, this parameterization is non-uniform.
\end{Theorem}
\begin{figure}[H]
	\centering
	\includegraphics[width=300pt]{imgs/90degarc.png}
	\caption{Comparison of the polar map with trigonometric functions to the physical NURBS map. Both represent the quarter annulus exactly but the NURBS map is non-uniform, whereas the polar map is uniform. (We could use this as reference element for the patches of a full circle.)}
\end{figure}

\begin{Proof}

	
	Note that this knot vector implies that $\Spline(\xi)$ are the Bernstein polynomials 
	\[
	\implies \qquad 
	\hat B_{3}(t) = t^2, \quad
	\hat B_{2}(t) = 2 t(1-t), \quad
	\hat B_{1}(t) = (1-t)^2.
	\]
	Then the weight function can be written out in polynomial form as well
	\[ \begin{split}
	W(t) = \sum_i w_i \hat B_i(t) 
	&= t^2 + \frac{2}{\sqrt{2}} t(1-t) + (1-t)^2 \\
	&= \big( t+(1-t) \big)^2 + (2-\sqrt{2})t(1-t) = 1 + (2-\sqrt 2) t(1-t).
	\end{split}\]
	
	To prove that this represents the arc exactly let $t \in [0,1]$ be arbitrary and calculate
	\[ \begin{split}
	\Big(\sum_i c_i w_i \hat B_i(t) \Big)^2
	&= \Big|\begin{pmatrix}
	t^2 + \sqrt{2}t(1-t) \\
	(1-t)^2 + \sqrt{2}t(1-t) \\
	\end{pmatrix}\Big|^2 \\
	&= \ \Big( t^2 + \sqrt{2}t(1-t) \Big)^2  \ \ + \Big( (1-t)^2 + \sqrt{2}t(1-t) \Big)^2 \\
	&= \Big( t^4 \qquad + \qquad 2 t^2 (1-t)^2 \qquad + \quad 2 \sqrt{2} t^3 (1-t) \Big) \\
	& \quad\ \  + \Big( (1-t)^4 + 2 t^2 (1-t)^2 \qquad + \quad 2 \sqrt{2} t (1-t)^3 \Big) \\
	&= t^4 + (1-t)^4 + 4 t^2 (1-t)^2 + 2\sqrt{2}t(1-t) (t^2+(1-t)^2) \\
	&= t^4 + (1-t)^4 + 2 t^2 (1-t)^2 + 2 \Big( t^2(1-t)^2 + (t^2+(1-t)^2)\sqrt{2}t(1-t)\Big) \\
	&= \Big( t^2 + (1-t)^2 + \sqrt{2} t(1-t) \Big)^2 = W(t)^2.
	\end{split}
	\]
	Thus the curve $|F(t)|^2 = \big(\sum_{i} c_i \hat R_i(t) \big)^2 = 1$ lies completely on the circle. Since also all control points are in the non-negative quadrant and the basis functions $\{\hat R_i\}$ are non-negative as well we indeed get the $90^\circ$ arc. \\
	
	Note that for this specific example it is possible to calculate the inverse $F^{-1}$ exactly! 
	\[ \begin{split}
		\begin{pmatrix} x \\ y \end{pmatrix} &= 
		F(t) = \sum_i c_i \hat R_i(t) = \frac 1 {W(t)} \begin{pmatrix}
			\hat B_1(t) + \frac 1 {\sqrt{2}} \hat B_2(t) \qquad \quad \ \\
 			\quad \ \qquad \frac 1 {\sqrt{2}} \hat B_2(t) + \hat B_3(t)
		\end{pmatrix} \\
		&= \frac 1 {W(t)} \begin{pmatrix}
			(1-t)^2 + \sqrt 2 t(1-t) \ \   \\
			\ \ t^2  \ \ \ + \sqrt 2 t(1-t) 
		\end{pmatrix} 
	\end{split}
	\]
Since $x,y \in [0,1]$ are in the positive quadrant we know $x = \sqrt{1-y^2}$, so we can focus on finding a formula for $y$ and get a formula for $x$ for free. (When this specific example is used for debugging it is better to use the relation $x(t)=y(1-t)$ because then the formula is numerically more stable.)
	\[
		y = \frac{ t(t + \sqrt 2 (1-t) ) }{W(t)} = \frac{ t(t + \sqrt 2 (1-t) ) }{(2-\sqrt{2})t(t-1) + 1}
	\]
	\[
	\Rightarrow \ 
		\Big( (2-\sqrt{2})t(t-1) + 1 \Big) y = t(t + \sqrt 2 (1-t) ) 
	\]
	\[
		\Rightarrow \ 
		\Big( (2-\sqrt{2})y + \sqrt{2} - 1 \Big) t^2
		+ \Big( -(2 -\sqrt{2})y - \sqrt{2} \Big) t + y  = 0            
	\]
Notice that this is a quadratic equation which can be solved using the formula
	\[
	\Leftrightarrow \ 
	\alpha t^2
	+ \beta t + \gamma  = 0
	\qquad 
	\Rightarrow 
	t = \frac{ -\beta \pm \sqrt{ \beta^2 - 4 \alpha \gamma }}{2 \alpha}.
	%t = -\frac{\beta}{2\alpha} \pm \sqrt{ \big(\frac{\beta^2}{4\alpha^2}\big) - \frac{x}{\alpha} }            
	\]
Only one of these solutions is the right one. To determine which sign to use (so replacing the symbol $\pm$ by either $+$ or $-$) we can insert $y=0$. Then it suffices to check $F(0) \overset != (1,0)^T$ by calculating
\[
	y = 0 
	\qquad \Rightarrow 
		\alpha = \sqrt{2} - 1, \quad \beta = - \sqrt{2}, \quad \gamma = 0 
\]
\[
	\qquad \Rightarrow
		0 \overset != t|_{y=0} = \frac{\sqrt{2} \pm \sqrt{ 2 - 0} }{\sqrt{2} - 1} 
		= \begin{cases}
			0 & \text{ if } - \\
			\frac{2\sqrt{2}}{\sqrt 2 - 1} \neq 0 & \text{ if } +
		\end{cases}
\]
Finally the inverse is given as
\[
	t = F^{-1}(x,y) = \frac{ -\beta - \sqrt{ \beta^2 - 4 \alpha \gamma }}{2 \alpha}.
\]
Keep in mind that the coefficients can either be dependent on $x$ or $y$ depending on what is most convenient, ie. $\alpha = \alpha(y) = \alpha(\sqrt{1 - x^2})$ and the same holds for $\beta$ and $\gamma$.
\end{Proof}



\subsection{Boundary}
For now consider the univariate case. We have assumed that the boundary knots are repeated $m=p+1$ times. The continuity formula yields $C^{p-m} = C^{-1}$, which implies that the spline is interpolatory at the endpoints 
\[
	\hat B_i(0) = \delta_{i,1} 
	\qquad \text{and} \qquad 
	\hat B_i(1) = \delta_{i,N} 
\] 

\begin{Def}[Boundary Operator] 
The one dimensional boundary operator is simply given by the projection onto the outer basis splines represented by indices $\{1,N\}$. It has the form
\[ \begin{split}
\gamma: \qquad \Spline(\xi)\qquad   
&\quad \rightarrow \quad \Spline(\{\xi_1 \dots \xi_{p+1} \}) + \Spline(\{\xi_N \dots \xi_{N+p+1} \}) \\
\qquad s = \sum_i \alpha_i \hat B_i &\quad \mapsto \qquad \gamma s = \alpha_1 \hat B_1 \ + \ \ \alpha_N \hat B_N.
\end{split}
\]
For multivariate B-Splines, NURBS and even physical spline spaces we use the same projection but now the outer basis splines are represented by multi-indices 
\[
	i=(i_1 \dots i_d)  \quad \text{ such that } \quad \exists 1 \leq k\leq d : \quad  i_k \in \{1, N_k\}.
\]
\end{Def}

%Now consider 

%The multi-indices of the boundary elements can be represented by $i_k \in \{0,N_k\}$ for some dimensional index is $k \in \{1 ... d \}$. All the B-splines with other indices vanish on the boundary. The projection onto the boundary gives yields another spline space 


\begin{Remark}[Patches]
	So far we have just modeled parametric spaces $\Omega \overset{\sim}= [0,1]^d$. Using multiple patches we can stitch together a domain with more complicated geometry. We have to ensure regularity along the patch interfaces as well. This can be done with standard discontinuous Galerkin methods. For simplicity we will only discuss single patch domains.
\end{Remark}


