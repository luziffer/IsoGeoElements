\subsection{Meshes}
While a lot of different meshes can describe the same geometry the numerical properties can differ. We will give an overview of which meshes are useful for us.

\begin{Def}[Parametric Mesh] The parametric mesh $\hat{\mathcal M}$ associated with a knot vector $\Xi$ divides the parametric domain $\hat \Omega = [0,1]^d$ into smaller boxes which lie between breakpoints
\[
	Q_j = (\zeta^1_{j_1}, \zeta^1_{j_1 + 1}) \times \dots \times (\zeta^d_{j_d}, \zeta^d_{j_d + 1}) .
\]
The parametric mesh is simply defined as the collection $\hat {\mathcal M} = \{Q_j | 1 \leq j_1 < n_1, \dots, 1 \leq j_d < n_d\}$. 
\end{Def}

\begin{Def}[Physical Mesh]
Transform the parametric elements $Q \in \hat{\mathcal M}$ using the physical map $F$. Then we get physical elements $K = F(Q)$. Note that the adjacency properties of the parametric elements are conserved. The physical mesh is defined as collection of such elements 
\[
	\mathcal M = \{ F(Q) \ | \ Q \in \hat{\mathcal M} \}.
\]
\end{Def}

\begin{Def}[Greville Mesh]
The Greville sites $\gamma = \{\gamma_i\}_{1 \leq i \leq N}$ of $\Spline_p(\Xi)$ are defined as
\[
	\gamma^k_{i_k} = \frac{\xi^k_{i_k} + \dots \xi^k_{i_k+p_k}}{p_k}
	\qquad \forall 1 \leq k \leq d, 1 \leq i_k \leq N_k.
\]
The Greville sites have the convenient property of recovering the input parameter \cite{veiga_buffa_sangalli}
\[
	t_k = \sum_{1 \leq i_k \leq N_k } \gamma^k_{i_k} \hat B^{k}_{i_k}(t_k) \qquad \forall 1 \leq k \leq d.
\]
While the knot vectors $\Xi$ might be repeated the Greville sites are not. They form a partition of the domain $\hat \Omega$. This partition can itself be interpreted as a mesh and therefore will be refered to as the Greville mesh. 
\end{Def}

\begin{Remark}
The physical domain is determined by the control points. Since splines do not interpolate in general the dependency of the physical domain on the control points can be rather complex. In practice it is very important to adjust the control points to the desired domain. Therefore we introduce the control polygon, which is a linear approximation of the physical domain which interpolates between control points. 
\end{Remark}

\begin{Def}[Control Polygon]
Let $\{\phi_i\}_{1 \leq i \leq N}$ be the linear Lagrange polynomials on the Greville sites, ie. $\phi_i(\gamma_j) = \delta_{i,j}$. Then the control polygon is defined as 
\[
	F_c(t) = \sum_{1 \leq i \leq N} w_i c_i \phi_i(t).
\]
The control polygon is useful for modeling because $\|F_c - F\| \rightarrow 0$ as the mesh is refined, so $h \rightarrow 0$. 
\end{Def}