Splines are particularly useful because of their numeric stability and geometric properties. In this section we will focus on the later. First we will explain how to refine and coarsen spline space. Then we will demonstrate how to model non-trivial domains.



\subsection{Refinement and Coarsening}

Refinement and coarsening strategies fall into three categories
\begin{itemize}
	\item $h$-refinement/coarsening: insert or remove breakpoints $\{\zeta_i\}$ 
	\item $p$-refinement/coarsening: elevate or lower the polynomial order $p$ 
	\item $k$-refinement/coarsening: change the regularity $\{k_i\}$ by changing the multiplicity $m_i = p - k_i$
\end{itemize}
All these operations rely on the basic operation of changing the knot vector. We will give algorithm for this which are called knot insertion/removal.
We start in one dimension. Assume we are given some coarse mesh $\xi$ and refine it by inserting some new knot $z$ with $\xi_j \leq z \leq \xi_{j+1}$ for some index $0 \leq j \leq N$. The refined knot vector will be denoted as $\tilde \xi$ and is given by
%Assume we have some mesh fine $\tilde \xi$ which is just the coarse mesh $\xi$ given by
\[ 	
	\tilde \xi := (\xi_1 \dots \xi_j,  \eta,  \xi_{j+1} \dots \xi_{N+p+1} ).
\]
Let $\{B_i\}_{i \leq N} \subset \Spline(\xi)$ be the basis on the coarse and $\{\tilde B_i\}_{i \leq N+1} \in \Spline(\tilde \xi)$ the basis on the fine mesh. 

\begin{Lemma}[Knot removal]
Knot removal is a linear mapping which is expressed as 
\[
	B_i = \alpha_i \tilde B_i + (1-\alpha_{i+1}) \tilde B_{i+1}.
\] 

where the restriction coefficients are given by 
\[
	\alpha_1 = \dots = \alpha_{j-p} = 1
	\qquad \text{and} \qquad
	\alpha_{j+1} = \dots = \alpha_{N} = 0,
\]
\[
	\forall i \in \{j-p +1 \dots j\}: \quad \alpha_i = \frac{\eta - \xi_i}{\xi_{i+p+1}-\xi_i}.
\]

%Then it can be embedded into the finer spline space $f \in \Spline(\xi)$ given the following relation
%Then the restriction 

%Let $\{\hat B_{i,q}\}_{i \in \{0,\dots,N+p-q\}} \subset \Spline(\xi)$ be the basis functions on the coarse mesh and $\{\hat A_{i,q}\}_{i \in \{0, \dots, N+p-q+1\}} \subset \Spline(\nu)$ be the basis functions on the refined mesh. Then these two are related by
%\[ 
%	\forall i \leq j-q: \ \hat B_{i,q} = \hat  A_{i,q} 
%	\qquad \text{ and } \qquad
%	\forall i \geq j: \ \hat B_{i,q} = \hat A_{j+1,q} 
%\]
%\[
%	\forall \text{ other } i : \ \hat B_{i,q} = \alpha_i \hat A_{i,q} + (1- \alpha_{i+1}) \hat A_{i+1,q} 
%\]
%where $\alpha_i = \frac{z - \xi'_i}{\xi_{i+q+1}-\xi_i}$ for $i \in \{j -q, \dots, j\}$ are the refinement coefficients.
\end{Lemma}

\begin{Proof}
In fact $\alpha_i$ is dependent on the degree $p$. We will write $\alpha_{i,p} \equiv \alpha_i$. 
Use induction on $p$.  \\
Let $p=0$. Since $\supp \hat B_{i,0} = [\xi_i, \xi_{i+1}]$ indeed 
$B_{i,0} = \tilde B_{i,0}$ for $i < j$ and $B_{i,0} = \bar B_{i+1,0}$ for $i > j$ and 
\[
	B_{j,0} = \mathds 1_{[\xi_j, \xi_{j+1})} 
			= \mathds 1_{[\xi_j, \eta)} + \mathds 1_{[\eta,\xi_{j+1})} 
			= \mathds 1_{[\tilde \xi_j, \tilde \xi_{j+1})} + \mathds 1_{[\tilde \xi_{j+1},\tilde \xi_{j+2})} 
			= \tilde B_{j,0} + \tilde B_{j+1, 0}.
\]
Suppose $p \geq 0$. Looking at the supports gives $B_i=\tilde B_i$ for $i \leq j-p$ and $B_{i,0} = \bar B_{i+1,0}$ for $i > j$. Now let $j-p+1 \leq i \leq j$ and
denote the recursion coefficients by
\[ 
	\omega_{i,p} := \frac{t - \xi_i}{\xi_{i+p} - \xi_i}
	\quad \text{ and } \quad
	\tilde \omega_{i,p} := \frac{t - \tilde \xi_i}{\tilde \xi_{i+p} - \tilde \xi_i} 
\]
\[ \implies \begin{split}
	\omega_{i,p} \alpha_{i,p}
		&= \tilde \omega_{i,p-1} \alpha_{i,p}
		= \tilde \omega_{i,p} \alpha_{i, p+1} 
		\\ \text{ and } \qquad
		(1-\omega_{i+1,p}) \alpha_{i+1,p}
		&= (1-\tilde \omega_{i+1,p-1}) \alpha_{i+1,p}
		= (1-\tilde \omega_{i+1,p}) \alpha_{i,p}
\end{split}\]

Then we can formulate the inductive argument 
\[ \begin{split}
	B_{i,p+1} & = \omega_{i,p} B_{i,p} + (1-\omega_{i+1,p}) B_{i+1,p} \\ 
			&= \omega_{i,p} \Big( \alpha_{i,p} \tilde B_{i,p} + (1-\alpha_{i+1,p}) \tilde B_{i+1,p} \Big) 
				+ (1-\omega_{i+1,p}) 
				\Big( \alpha_{i+1,p} \tilde B_{i+1,p} + (1-\alpha_{i+2,p}) \tilde B_{i+2,p} \Big)  \\ 
			&= \alpha_{i,p+1} \Big( \tilde\omega_{i,p} \tilde B_{i,p} + (1-\tilde \omega_{i+1,p}) \tilde B_{i+1,p} \Big)
			+ (1- \alpha_{i+1,p})  \Big( \tilde\omega_{i+1,p} \tilde B_{i+1,p} + (1-\tilde \omega_{i+2,p}) \tilde B_{i+2,p} \Big) \\
			&= \alpha_{i,p+1} \tilde B_{i,p+1} + (1- \alpha_{i+1,p}) \tilde B_{i+1,p+1}. 
\end{split}\]
\end{Proof}


\begin{Remark}[Prolongation]
The embedding $\Spline(\xi) \subset \Spline(\tilde \xi)$ can be expressed as matrix writing out the coordinates in the B-spline basis
\[
	f = \sum_{i \leq N} b_i B_i[\xi] \in \Spline(\xi)
	\qquad \text{and} \qquad
	\tilde f = \sum_{i \leq N+1} \tilde b_i \tilde B_i[\tilde \xi] \in \Spline(\tilde \xi).
\]
Demanding $f \overset!= \tilde f$ and using the lemma yields the matrix form 
\[
	\tilde b = \begin{pmatrix}
	\tilde b_1 \\ \tilde b_2 \\ \tilde b_3 \\ \\ \vdots \\ \\ \tilde b_{N+1} 
	\end{pmatrix}
	= 
	\begin{pmatrix}
	\alpha_1  \\
	  1-\alpha_2 & \alpha_2 \\
	& 1-\alpha_3 & \alpha_3 \\ \\
	&            & \ddots & \ddots \\ \\
	&            &       & 1-\alpha_N  & \alpha_N 
	\end{pmatrix}
	\begin{pmatrix}
	b_1 \\ b_2 \\ b_3 \\ \vdots \vspace{6pt} \\ b_{N} 
	\end{pmatrix}
	= P b .
\]
Note that the prolongation matrix $P$ is surjective and therefore has a left inverse $R = P^{-1}$ which is called the restriction matrix. Note that $R \neq P^T$ is not just the transposition in general! This can be seen by asserting that the constant unity function is mapped to itself. \\


For the NURBS space embedding $\NURBS(\xi, w) \subset \NURBS(\tilde \xi, \tilde w)$ the weights have to be adjusted accordingly. The refinement mapping is given by
$ 
\tilde w = P w 
$ .
\end{Remark}

