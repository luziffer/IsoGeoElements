\subsection{Higher Dimensional Splines}
Consider the multi-dimensional parametric mesh $\hat \Omega = \hat \Omega^{1} \times .. \times \hat \Omega^{d} = (0,1)^d$ that results from product of one-dimensional splines. The knot vectors will be indicated by $\Xi = (\xi^1 \dots \xi ^d)$ which is just a collection of one-dimensional knot vectors. 

\begin{Def}[Multivariate B-Splines] The space of multivariate B-Splines is the tensor product of univariate B-Splines, so $\Spline(\Xi) := \Spline(\xi^1) \otimes \dots \otimes \Spline(\xi^d)$. The multivariate basis  splines consist of the products of the univariate basis splines
\[
	\hat{B}_{i_1 \dots i_d} (t_1 \dots t_d)
	= \hat{B}_{i_1, p_1} (t_1) \hat{B}_{i_2, p_2} (t_2) \dots \hat{B}_{i_d, p_d} (t_d)
	\quad \text{for } i_k \in \{1\dots, N_k \}
\]
\end{Def}

Note that we will use the multi-index notation 
$i=(i_1, \dots i_d) \in \N^d$ with $i_k \in \{1\dots, N_k \} $
or the flattened index notation $i \in \{1 \dots, N \}$ with $N=\prod_{k \in \{1 \dots d\}} N_k$ equivalently and we might switch between them where convenient.

%Note that the derivatives are multi-dimensional now as well. 

\begin{Lemma}[Multivariate derivative] The partial derivatives are given by the product 
\[\forall k \in \{1 \dots d \}: \quad
	\partial_{k} \hat B_{i_1, \dots, i_d }(t) 
	= \hat B'_{i_k}(t_k) \prod_{
		%\substack{
		%	j \in \{1 \dots d\} \\
			j \neq k
		}%}
	\hat B_{i_j}(t_j).
\]
\end{Lemma}
 
\begin{Def}[NURBS]
Given knots $\Xi$ and positive weights $\{w_i\}_{1 \leq i \leq N}, w_i > 0$ we define the Non-Uniform Rational B-Splines, aka. NURBS as
\[
\hat R_{i,p}(t) := \frac{w_i \hat B_{i,p}(t)}{W(t)} 
= \frac{w_i \hat B_{i,p}(t)}{\sum_j w_j \hat B_{j,p}(t) .}
\]
The space of NURBS will be written as \[
\NURBS_p(\Xi) = \text{span}_{\R} \{\hat R_1, \dots \hat R_N \} .
\] 
(Again, if the degree is clear from context or does not matter we may omit it in the notation.)

Notice that for trivial weights $\forall i: w_i = 1$ NURBS reduce to B-splines, so $\Spline_p(\Xi) \subset \NURBS_p(\Xi)$. 
\end{Def}
\begin{Remark} Caveat! In general we have non-uniform weights and $\NURBS(\Xi) \neq \NURBS(\xi^1) \otimes \dots \otimes \NURBS(\xi^d)$. However, in some cases (for instance the annulus) the multi-dimensional space is indeed a tensor product. We will see later on that the tensor structure is a very useful property.
\end{Remark}




\begin{Lemma} The gradient of any non-uniform rational B-spline is given by
\[ 
		\nabla \hat R_{i}(t) 
		= w_i \frac{\nabla \hat B_{i}(t)  W(t)  - \hat B_i(t)  \nabla W(t) }{W(t)^2 }.
\]
\end{Lemma}


\begin{Remark}[Projective Geometry]
Note that scaling the weights $\tilde w = c w$ by a positive constant $c > 0$ will not change the NURBS basis
\[
 	\hat R_i = \frac{w_i \hat B_i}{W} = \frac{( c \  w_i) \hat B_i}{c\   W} 
 	= \tilde R_i .
\]
Therefore we use an equivalence relation on the weights $\tilde w \equiv w$ if $\tilde w = c w$ and it turns out that the weights are elements of projective space. 
The control points which will be introduced later on, can in fact be seen as elements of a projective space as well. 
More details on how to interpret NURBS in the context of projective geometry can be found in Farin's book. \cite{farin1999nurbs}
\end{Remark}