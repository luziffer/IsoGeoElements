%% Creator: Inkscape inkscape 0.92.3, www.inkscape.org
%% PDF/EPS/PS + LaTeX output extension by Johan Engelen, 2010
%% Accompanies image file 'spline_evaluation_methods.pdf' (pdf, eps, ps)
%%
%% To include the image in your LaTeX document, write
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics{<filename>.pdf}
%% To scale the image, write
%%   \def\svgwidth{<desired width>}
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics[width=<desired width>]{<filename>.pdf}
%%
%% Images with a different path to the parent latex file can
%% be accessed with the `import' package (which may need to be
%% installed) using
%%   \usepackage{import}
%% in the preamble, and then including the image with
%%   \import{<path to file>}{<filename>.pdf_tex}
%% Alternatively, one can specify
%%   \graphicspath{{<path to file>/}}
%% 
%% For more information, please see info/svg-inkscape on CTAN:
%%   http://tug.ctan.org/tex-archive/info/svg-inkscape
%%
\begingroup%
  \makeatletter%
  \providecommand\color[2][]{%
    \errmessage{(Inkscape) Color is used for the text in Inkscape, but the package 'color.sty' is not loaded}%
    \renewcommand\color[2][]{}%
  }%
  \providecommand\transparent[1]{%
    \errmessage{(Inkscape) Transparency is used (non-zero) for the text in Inkscape, but the package 'transparent.sty' is not loaded}%
    \renewcommand\transparent[1]{}%
  }%
  \providecommand\rotatebox[2]{#2}%
  \newcommand*\fsize{\dimexpr\f@size pt\relax}%
  \newcommand*\lineheight[1]{\fontsize{\fsize}{#1\fsize}\selectfont}%
  \ifx\svgwidth\undefined%
    \setlength{\unitlength}{440.15698531bp}%
    \ifx\svgscale\undefined%
      \relax%
    \else%
      \setlength{\unitlength}{\unitlength * \real{\svgscale}}%
    \fi%
  \else%
    \setlength{\unitlength}{\svgwidth}%
  \fi%
  \global\let\svgwidth\undefined%
  \global\let\svgscale\undefined%
  \makeatother%
  \begin{picture}(1,0.29486363)%
    \lineheight{1}%
    \setlength\tabcolsep{0pt}%
    \put(0,0){\includegraphics[width=\unitlength,page=1]{spline_evaluation_methods.pdf}}%
    \put(0.30410556,0.06893161){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\vdots$\end{tabular}}}}%
    \put(0.65972011,0.07095676){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\ddots$\end{tabular}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=2]{spline_evaluation_methods.pdf}}%
    \put(0.24470428,0.01775582){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\hat B_{i,0}(t)$\end{tabular}}}}%
    \put(0.12006957,0.01775582){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\hat B_{i-1,0}(t)$\end{tabular}}}}%
    \put(0.36933899,0.01775582){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\hat B_{i+1,0}(t)$\end{tabular}}}}%
    \put(0.49397374,0.01775582){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\hat B_{i+2,0}(t)$\end{tabular}}}}%
    \put(0.75701301,0.0211983){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\dots$\end{tabular}}}}%
    \put(0.61874691,0.01779044){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\hat B_{i+3,0}(t)$\end{tabular}}}}%
    \put(0.86801641,0.01779044){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\hat B_{N+p+1,0}(t)$\end{tabular}}}}%
    \put(0.24178604,0.16233973){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\hat B_{i,p-1}(t)$\end{tabular}}}}%
    \put(0.11715141,0.16233972){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\hat B_{i-1,p-1}(t)$\end{tabular}}}}%
    \put(0.36642069,0.16233972){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\hat B_{i+1,p-1}(t)$\end{tabular}}}}%
    \put(0.49105544,0.16233972){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\hat B_{i+2,p-1}(t)$\end{tabular}}}}%
    \put(0.61569019,0.16233972){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\hat B_{i+3,p-1}(t)$\end{tabular}}}}%
    \put(0.24257747,0.26288942){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\hat B_{i,p}(t)$\end{tabular}}}}%
    \put(0.11794276,0.26288942){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\hat B_{i-1,p}(t)$\end{tabular}}}}%
    \put(0.36721214,0.26288942){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\hat B_{i+1,p}(t)$\end{tabular}}}}%
    \put(0.49184689,0.26288942){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\hat B_{i+2,p}(t)$\end{tabular}}}}%
    \put(0.00489793,0.26539277){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$q=p: $\end{tabular}}}}%
    \put(0.00145545,0.16839668){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$q=p: $\end{tabular}}}}%
    \put(0.0014377,0.01682353){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$q=0: $\end{tabular}}}}%
    \put(0.03257003,0.09291605){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\smash{\begin{tabular}[t]{l}$\vdots$\end{tabular}}}}%
  \end{picture}%
\endgroup%
