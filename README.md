# IsoGeoElements

Isogeometric Element Methods can be seen as extension to Finite Element Methods which are applied for solving PDE problems.
In particular they allow for representation of curved geometry using spline technology.
We provide an implementation of Galerkin routines for diffusion problems on single patch NURBS domains.

Here are some links where to find what
- for the theoretical background take a look at the [slides](../theory/presentation.pdf) or read the [full version](../theory/document.pdf)
- take a look at some hands on [examples](../examples/) for solving the Poisson equation 
- view the [source code](../src/)


## Examples

Univariate construction
```julia
Ξ = [0, .1, .2, .3, .4, .5, .6, .7, .8, .9,  1 ]
p = 2
s = BSpline1D(Ξ,p)

t = [0:1/100:1 ... ] #equidistant evaluation
for i=1:length(b)
    B_i = evaluate(s, t, i)
    #...
end
```
![img](../imgs/equidist.gif)

Multivariate tensor product construction
```julia
Ξ = [0, .25, .5, .75, 1]
p = 2
s = RegularSpline(Ξ, p)

s = BSpline((s,s)) #tensor product

t = [0:1/100:1 ... ],[0:1/100:1 ... ]
for i=1:size(s)[1], j=1:size(s)[2]
    B_ij = evaluate(s,t,(i,j))
    ∇B_ij = ∇evaluate(s,t,(i,j))
    #...
end
```
![img](../imgs/parametric.gif)

Add physical mapping
```julia
F = PhysicalMap(QuaterAnnulus())
for i=1:size(s)[1], j=1:size(s)[2]
    B_ij = evaluate(s,F,t,(i,j))
    ∇B_ij = ∇evaluate(s,F,t,(i,j))
    
    #physical coordinates 
    xy = evaluate(F,t)
    x = xy[1,:,:]; y = xy[2,:,:]
  
    #...
end
```
![img](../imgs/physical.gif)

---

> Some years ago a few researchers joked about NURBS,
> saying that the acronym stands for Nobody Understands Rational B-Splines.
> We admit that our collegues were right. [...]
> We hope to change the acronym from NURBS to EURBS, that is, Everybody Understands Rational B-Splines.
>
> L. Piegl, W. Tiller (1995)

---

## Installation
1. Make sure [Julia](https://julialang.org/) is installed.
2. Install this repository
```julia
using Pkg
Pkg.add("https://gitlab.com/luziffer/IsoGeoElements")
```
3. Add the following packages
```julia
using Pkg
Pkg.add([

  #dependencies
  "LinearAlgebra",
  "FastGaussQuadrature",
  "PyPlot",

  #dependencies of examples
  "CSV",
  "DataFrames"
  
  #only needed for test
  "Calculus",  
  "ProgressMeter",

  
  #only neededed for comparison
  "JuAFEM"
])
```

## Similar packages
- [NURBS.jl](https://github.com/TheBB/NURBS.jl) by TheBB.
  Provides an alternative implementation for evaluation and derivative of univariate NURBS.
- [Interpolation.jl](https://github.com/JuliaMath/Interpolations.jl) by tlycken.
  Fast least-squares quasi-interpolant on uniform meshes. Only supports splines up to cubic degree.
