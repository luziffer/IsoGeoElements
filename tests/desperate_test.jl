try
    using .IsoGeoElements
catch e
    if e isa UndefVarError
        include("../src/IsoGeoElements.jl")
        using .IsoGeoElements
    else raise(e)  end
end

ξ = [0,0,0,1,1,1.]
b = BSpline1D(ξ,2)
b = BSpline((b,b))
w = repeat([1, 1/√2, 1], 3)
b = NURBS2D(b, w)
c = [ 1 0 ; 1 1; 0 1 ]
c = [ c ; 1.5 .* c; 2 .* c ]
c = Array(c')
F = PhysicalMap(b, c)
M = mass_matrix(b, F)
