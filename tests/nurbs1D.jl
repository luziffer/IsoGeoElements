#copy bspline1D.jl and insert these lines of code appropriately
# #=NURBify=# spline = NURBS(spline, ones(length(spline)))
# #=NURBify=# Q = quadrature( unique(spline.bspline.Ξ[1]), Q )

include("../src/IsoGeoElements.jl")
using .IsoGeoElements

using Test
using PyPlot
using Calculus
using ProgressMeter



#sanity checks
spline = BSpline1D([0., 0,0,1,1,1], 2)
#=NURBify=# spline = NURBS(spline, ones(length(spline)))
@test length(spline)==3


clf(); title("1D spline")
plot(spline, i=1, label=L"\hat B_1")
plot(spline, i=2, label=L"\hat B_2")
plot(spline, i=3, label=L"\hat B_3")
display(gcf())

clf(); title("1D spline derivatives")
plot(spline, i=1, order=1, label=L"\hat B_1")
plot(spline, i=2, order=1, label=L"\hat B_2")
plot(spline, i=3, order=1, label=L"\hat B_3")
display(gcf())

#mass matrix
spline = BSpline1D([0,0, 0:.1:1... ,1,1], 2)
#=NURBify=# spline = NURBS(spline, ones(length(spline)))
M = mass_matrix(spline)
@test abs(sum(M) - 1) < 1e-3
@test sum( abs.(M.- M') ) < 1e-3

M_ref = zeros(length(spline),length(spline))
Q = trapz(1000)
#=NURBify=# Q = quadrature( unique(spline.bspline.Ξ[1]), Q )
@showprogress .5 "Manual 1D mass computation :" for i=1:length(spline)
    B_i = evaluate(spline, Q.x, i)
    for j=i:length(spline)
        B_j = evaluate(spline, Q.x, j)
        M_ij = Q.w' * (B_i .* B_j)
        M_ref[i,j] = M_ref[j,i] = M_ij
    end
end
@test sum( abs.( M_ref .- M ) ) < 1e-3



#stiffness matrix
spline = BSpline1D([0,0, 0:.05:1... ,1,1], 2)
#=NURBify=# spline = NURBS(spline, ones(length(spline)))
S = stiffness_matrix(spline)
@test abs(sum(S)) < 1e-3
@test sum( abs.(S.- S') ) < 1e-3

S_ref = zeros(length(spline),length(spline))
Q = trapz(1000)
#=NURBify=# Q = quadrature( unique(spline.bspline.Ξ[1]), Q )
@showprogress .5 "Manual 1D stiffness computation :" for i=1:length(spline)
    ∂B_i = ∂evaluate(spline, Q.x, i)
    for j=i:length(spline)
        ∂B_j = ∂evaluate(spline, Q.x, j)
        S_ij = ∂B_i .* ∂B_j
        S_ij = Q.w' * S_ij
        S_ref[i,j] = S_ref[j,i] = S_ij
    end
end
@test sum( abs.( S_ref .- S ) ) < 2e-3


#example problem
u(t) = sin(4*π*t^2)
u1(t) = 8*π*t*cos(4*π*t^2)
u2(t) = 8*π*( cos(4*π*t^2) - 8*π*t^2*sin(4*π*t^2))
spline = BSpline1D([0,0,0:.1:1...,1,1], 2)
#=NURBify=# spline = NURBS(spline, ones(length(spline)))
α = LstSqProjector(spline, [0:.1:1...], u2)
β = LstSqProjector(spline, [0:.1:1...], u)

#solve
i = [ interior(spline) ... ]
M = mass_matrix(spline)
S = stiffness_matrix(spline)
M = M[i,:]; M = M[:, i]
S = S[i,:]; S = S[:, i]
L = - M * [α...][i]
z_i = S\L
z = zeros(length(spline))
z[i] = z_i

clf(); title("Example problem")
t = [0:.01:1 ... ]
#plot( t, u2.(t), label="true source", linestyle="--")
#plot( spline, coef=α, t=t, label="spline source")
plot( t, u.(t), label="true solution", linestyle="--")
plot( spline, coef=z, t=t, label="spline solution")
plot( spline, coef=[β...], label="LstSq projection", linestyle=":", alpha=.5)
scatter( spline.bspline.Ξ[1], zeros(length(spline.bspline.Ξ[1])), color="black",label="knots Ξ" )
legend()
display(gcf())
