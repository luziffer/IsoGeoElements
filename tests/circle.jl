include("../src/alldeps.jl")
using Test
using LinearAlgebra
using Calculus
using ProgressMeter


F = QuaterCircle()
b = RegularSpline([0:.25:1...], 2)

#display basis function
#for i in [1] #single basis function
for i=1:length(b)
    clf()
    plot(b, F, i=i, label=L"B_i")
    #plot(b, F, i=i, order=1, label=L"∂B_i")
    xlabel("X")
    ylabel("Y")
    xlim((0,1))
    ylim((0,1))
    zlim((0,1))
    legend(); title("Quater Circle \$i=$i\$")
    display(gcf())
end

#true inverse
α(y) =  (2-√2)y + √2-1
β(y) = -(2-√2)y - √2
Gy(y) = ( -β(y) - √( max(0, β(y)^2 - 4*α(y)*y)) ) / (2α(y))
Gx(x) = 1 - Gy(x)


t = [0:.01:1 ... ]
xy = evaluate(F, t)
x = xy[1,:]; y = xy[2,:]
t_inv = Gy.(y)
@test sum( abs.(t_inv .- t ) ) < 1e-3
t_inv = Gx.(x)
@test sum( abs.(t_inv .- t ) ) < 1e-3

uv = ∇evaluate(b,F,(t,),(1,))
u = uv[1,:]; v = uv[2,:]
∂x_B1 = derivative(x -> evaluate(b,Gx(x),1)[1])
∂y_B1 = derivative(y -> evaluate(b,Gy(y),1)[1])
#@test sum( abs.(u .- ∂x_B1.(x)) ) < 1e-2
@test maximum( abs.(v[2:end] .- ∂y_B1.(y[2:end])) ) < 1e-2


#system matrices
M = mass_matrix(b, F)
@test abs(sum(M) - π/2) < 1e-3
@test sum( abs.(M.- M') ) < 1e-3

S = stiffness_matrix(b, F)
@test abs(sum(S)) < 1e-3
@test sum( abs.(S.- S') ) < 1e-3

i = [interior(b) ... ]
M = M[i,:]; M = M[:,i]
S = S[i,:]; S = S[:,i]
