using Test #Unit Test
try using .IsoGeoElements catch e
    if e isa UndefVarError
        include("../src/IsoGeoElements.jl")
        using .IsoGeoElements
    end
end

s1 = BSpline([0.,0,0, 1,1,1], 2)
@test length(s1) == 3
s2 = s1 ⊗ s1
@test size(s2) == (3,3)

t = [0:.1:1 ... ]
B = evaluate(s1,t)
@test size(B) == (3,11)
@test B[1,:] == (1 .- t).^2
@test B[2,:] == 2 .* t .* (1 .- t)
@test B[3,:] == t.^2

B1 = evaluate(s1, t, 1)
@test B1 == B[1,:]
B23 = evaluate(s1, t, 2, 3)
@test B23 == B[2:3, :]

B = evaluate(s2, (t,t))
@test size(B) == (3,3,11,11)


∂B = ∂evaluate(s1, t)
@test size(∂B) == (3,11)
@test ∂B[1,:] == 2 .* (t .- 1)
@test ∂B[2,:] == 2 .- 4 .* t
@test ∂B[3,:] == 2 .* t
