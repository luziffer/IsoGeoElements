include("../src/alldeps.jl")
using PyPlot

r(x,y) = sqrt.( x .^2  .+ (y') .^2 )
ϕ(x,y)  = atan.( y' ./ x )

u(x,y) = exp.(-r) .* sin(2. * ϕ)
∇u(x,y) = - exp(-r) .* 
