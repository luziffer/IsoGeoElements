using Test

@testset "parametric" begin
    include("bspline1D.jl")
    include("nurbs1D.jl")
    include("bspline2D.jl")
    include("nurbs2D.jl")
end


@testset "physical" begin
    include("annulus.jl")
    #include("circle.jl") # not needed
end
