include("../src/IsoGeoElements.jl")
using .IsoGeoElements

using Test
using PyPlot
using LinearAlgebra
using ProgressMeter


#sanity checks
spline = BSpline1D([0., 0,0,1,1,1], 2)
spline = BSpline((spline,spline))
@test size(spline)==(3,3)

clf(); title("2D B-Spline")
t = [0:.01:1 ... ], [0:.01:1 ... ]
plot(spline, i=(2,2), t=t)
plot(spline, i=(2,2), order=:∇)
display(gcf())



#mass matrix
spline = BSpline1D([0,0, 0:.25:1... ,1,1], 2)
spline = BSpline((spline,spline))
M = mass_matrix(spline)
@test abs(sum(M) - 1) < 1e-3
@test sum( abs.(M.- M') ) < 1e-3


M_ref = zeros(length(spline),length(spline))
Q = trapz(50)
ζ = ( unique(spline.Ξ[1]), unique(spline.Ξ[2]))
Q = quadrature(ζ, Q )

@showprogress .5 "Manual 2D mass computation :" for i=1:length(spline)
    B_i = evaluate(spline, Q.x, i)
    for j=i:length(spline)
        B_j = evaluate(spline, Q.x, j)
        M_ij = B_i .* B_j
        M_ij = sum(Q.w .* M_ij)
        M_ref[i,j] = M_ref[j,i] = M_ij
    end
end
@test sum( abs.( M_ref .- M ) ) < 1e-3



#stifness matrix
spline = BSpline1D([0,0, 0:.25:1... ,1,1], 2)
spline = BSpline((spline,spline))
S = stiffness_matrix(spline)
@test abs(sum(S)) < 1e-3
@test sum( abs.(S.- S') ) < 1e-3


S_ref = zeros(length(spline),length(spline))
Q = trapz(150)
ζ = ( unique(spline.Ξ[1]), unique(spline.Ξ[2]))
Q = quadrature(ζ, Q )

@showprogress .5 "Manual 2D stiffness computation :" for i=1:length(spline)
    ∇B_i = ∇evaluate(spline, Q.x, i)
    for j=i:length(spline)
        ∇B_j = ∇evaluate(spline, Q.x, j)
        S_ij = ∇B_i .* ∇B_j
        S_ij = sum(S_ij, dims=1)[1,:,:]
        S_ij = sum(Q.w .* S_ij)
        S_ref[i,j] = S_ref[j,i] = S_ij
    end
end
@test sum( abs.( S_ref .- S ) ) < 1e-2 #TODO: Why is this so bad?




#example problem
u(x,y)    = sin.(4π .* x.^2) .* sin.(2π .* y)
u_x(x,y)  = 8π .* x .* cos.(4π .* x.^2) .* sin.(2π .* y)
u_xx(x,y) = 8π .* (cos.(4π .* x.^2) - 8π .* x.^2 .* sin.(4π .* x.^2)) .* sin.(2π .* y)
u_y(x,y)  = sin.(4π .* x.^2) .* 2π .* cos.(2π .* y)
u_yy(x,y) = sin.(4π .* x.^2) .* (-4π^2) .* sin.(2π .* y)
Δu(x,y) = ( u_xx(x,y) .+ u_yy(x,y) )
spline = BSpline1D([0,0,0:.1:1...,1,1], 2)
spline = BSpline((spline,spline))
τ = [0:.1:1...],[0:.1:1...]
α = LstSqProjector(spline, τ, Δu)
β = LstSqProjector(spline, τ, u)

#solve
i = [ interior(spline) ... ]
M = mass_matrix(spline)
S = stiffness_matrix(spline)
M = M[i,:]; M = M[:, i]
S = S[i,:]; S = S[:, i]
L = - M * [α...][i]
z_i = S\L
z = zeros(length(spline))
z[i] = z_i

#draw knot vector as dashed lines
function drawknots(spline::BSpline2D)
    for k=1:2, i=1:size(spline)[k]
            Ξ = (spline isa NURBS ? spline.b.Ξ : spline.Ξ)
            x = Ξ[k][i]
            x,y = [x,x], [0,1]
            x,y = (k==2 ? (y,x) : (x,y))
            plot(x,y, color="black", linestyle=":")
    end
end

#draw solutions
clf(); title("BSpline 2D Example: true solution")
x = repeat( [t[1]], length(t[2]) )
y = repeat( [t[2]], length(t[1]) )
x = Matrix(hcat(x...)); y = Matrix(hcat(y...)')
contourf( x,y, u(x,y) )
colorbar(); drawknots(spline); display(gcf())
clf(); title("BSpline 2D Example: LstSq spline projection of true solution")
plot( spline, coef=z, t=t)
colorbar(); drawknots(spline); display(gcf())
clf(); title("BSpline 2D Example: Galerkin spline solution")
plot( spline, coef=z, t=t)
colorbar(); drawknots(spline); display(gcf())
