include("../src/IsoGeoElements.jl")
using .IsoGeoElements

F = PhysicalMap(QuaterAnnulus())
s1 = BSpline1D([0,0,0:.25 ... ,1,1], 2)
s2 = BSpline1D([0,0,0:.5 ... ,1,1], 2)
s = BSpline((s1,s2))
M = mass_matrix(s,F)
