include("../src/alldeps.jl")
using Test
using PyPlot
using LinearAlgebra
using ProgressMeter


#compare against analytic functionalities
phys = PhysicalMap(QuaterAnnulus())

#construct basis
ξ = [0:.1:1 ... ]
b = RegularSpline(ξ, 2)
b = BSpline((b,b))

#display basis functions
for (i,j) in [(4,4)] #single basis function
#for i=1:size(b)[1],j=1:size(b)[2]
    clf(); figure(figsize=(6,6))
    title("Quater Annulus \$i=$((i,j))\$")
    plot(b, phys, i=(i,j))
    plot(b, phys, i=(i,j), order=:∇, scale=75., linewidth=.1)
    display(gcf())
end


#example problem
radius(x,y) = sqrt.(x.^2 .+ y.^2)
angle(x,y) = atan.(y,x)

#= Example1
u_polar(r,ϕ)  = r^2*(r-1)*(2-r) * sin(2ϕ)
Δu_polar(r,ϕ) = (21r - 14r^2 - 4) * sin(2ϕ)
=## = Example2
u_polar(r,ϕ)  = exp(-r) * sin(2ϕ)
Δu_polar(r,ϕ) = (1-1/r-2/r^2) * exp(-r) * sin(2ϕ)
# =#

u(x,y) = u_polar.( radius(x,y), angle(x,y) )
Δu(x,y) = Δu_polar.( radius(x,y), angle(x,y) )

t = [0:.05:1 ... ], [0:.05:1 ...]
α_u  = LstSqProjector(b,phys, t, u)
α_Δu = LstSqProjector(b,phys, t, Δu)
α_u = [α_u ... ]; α_Δu = [α_Δu ... ]
xy = evaluate(phys, t)
x = xy[1,:,:]; y = xy[2,:,:]
B = evaluate(b, t)
B = reshape(B, length(b), size(B)[3:4]...)
@test sum(abs.(sum(B.*α_u,dims=1)[1,:,:] - u(x,y))) < 1e-2
@test sum(abs.(sum(B.*α_Δu,dims=1)[1,:,:] - Δu(x,y))) < 1e-2



println("Calculating system matrices...")
@time M = mass_matrix(b, phys)
@time S = stiffness_matrix(b, phys)

#variational sanity check
@test sum( abs.(S * α_u - M * α_Δu) ) < 1e-2
#@test sum( abs.(S_ref * α_u - M_ref * α_Δu) ) < 1e-3

#println("Calculating reference system matrices...")
#@time M_ref = mass_matrix_quater_annulus(b; n_quadrature=250)
#@time S_ref = stiffness_matrix_quater_annulus(b; n_quadrature=250)
#@test sum( abs.(M-M_ref)) < 1e-3
#@test sum( abs.(S-S_ref)) < 1e-3



#inverse physical map
#t = [0:.1:1 ... ]; t= (t,t)
#xy = evaluate(phys,t)
#x,y = xy[1,:,:], xy[2,:,:]
#t1,t2 = IGA.inverse_quater_annulus(x,y)
#@test sum(abs.(t[1] .- t1)) < 1e-6
#@test sum(abs.(t[2] .- t2)) < 1e-6

#===
#physcail basis splines
F = QuaterAnnulus()
#ξ = [0, .01, .1:.1:.9 ... , .99, 1]
ξ = [0:.1:1 ... ]
b = RegularSpline(ξ, 2)
b = BSpline((b,b))

#=
#display basis functions
for (i,j) in [(2,2)] #single basis function
#for i=1:size(b)[1],j=1:size(b)[2]
    clf(); title("Quater Annulus \$i=$((i,j))\$")
    plot(b, F, i=(i,j))
    plot(b, F, i=(i,j), order=:∇)
    display(gcf())
end
=#

#inverse physical map
function F_inv(x,y)
    r = sqrt.( x.^2 + y.^2 )
    y = y./r
    β = -(2-√2) .* y .- (√2)
    α =  (2-√2) .* y .+ (√2-1)
    t2 = (-β .- sqrt.(β.^2 .- 4 .* α .* y )) ./ (2 .* α)
    t1 = r[:,1] .- 1; t2 = t2[1,:]
    return t1,t2
end
t = [0:.05:1 ... ], [0:.025:1 ... ]
xy = evaluate(F,t)
x,y = xy[1,:,:], xy[2,:,:]
t1,t2 = F_inv(x,y)
@test sum(abs.(t[1] .- t1)) < 1e-6
@test sum(abs.(t[2] .- t2)) < 1e-6


#derivative sanity check
t1,t2 = t1[2:end-1],t2[2:end-1]
function b22(x)
    t = F_inv([x[1]],[x[2]])
    B = evaluate(b, t, (2,2))
    B = B[1,1]
    return B
end
∇b22 = gradient(b22)
for i=1:length(t1), j=1:length(t2)
    t = ([t1[i]], [t2[j]])
    xy = evaluate(F, t)[:,1,1]
    try
        @test sum(abs.(∇b22(xy) .- ∇evaluate(b,F,t,(2,2))[:,1,1])) < 1e-3
    catch
        println("Error at t = ", t)
        println("numerically ", ∇b22(xy))
        println("programmed ", ∇evaluate(b,F,t,(2,2))[:,1,1])

    end
end



#mass matrix
M = mass_matrix(b, F)
@test abs(sum(M) - 3π/4) < 1e-3
@test sum( abs.(M.- M') ) < 1e-3

r = 2 .* [0:.005:1 ... ] .+ 1.
ϕ = π/2 .* [0:.005:1 ... ]
r = reshape(r, length(r), 1)
ϕ = reshape(r, 1, length(ϕ))
x,y = r .* cos.(ϕ), r .* sin.(ϕ)
t = F_inv(x,y)
M_ref = zeros(length(b),length(b))
B = evaluate(b,t)
B = reshape(B, length(b), size(B)[3:4]... )
for i=1:length(b)
    B_i = B[i, :, :]
    for j=i:length(b)
        B_j = B[j,:,:]
        M_ij = B_i .* B_j .* r
        M_ij = sum(M_ij) / prod(size(M_ij))
        M_ref[i,j] = M_ref[j,i] = M_ij
    end
end
#@test sum( abs.( M_ref .- M ) ) < 1e-3

#stiffness matrix
S = stiffness_matrix(b, F)
@test abs(sum(S)) < 1e-3
@test sum( abs.(S.- S') ) < 1e-3

r = 2 .* [0:.005:1 ... ] .+ 1.
ϕ = π/2 .* [0:.005:1 ... ]
r = reshape(r, length(r), 1)
ϕ = reshape(r, 1, length(ϕ))
x,y = r .* cos.(ϕ), r .* sin.(ϕ)
t = F_inv(x,y)
S_ref = zeros(length(b),length(b))
∇B = ∇evaluate(b,t)
∇B = reshape(∇B, 2, length(b), size(∇B)[4:5]... )
for i=1:length(b)
    ∇B_i = ∇B[:,i, :, :]
    for j=i:length(b)
        ∇B_j = ∇B[:,j,:,:]
        S_ij = sum(∇B_i .* ∇B_j, dims=1)[1,:,:] .* r
        S_ij = sum(S_ij) / prod(size(S_ij))
        S_ref[i,j] = S_ref[j,i] = S_ij
    end
end
#@test sum( abs.( S_ref .- S ) ) < 1e-3


#variational sanity check
t = [0:.05:1 ... ], [0:.05:1 ...]
α_u  = LstSqProjector(b,F, t, u)
α_Δu = LstSqProjector(b,F, t, Δu)
α_u = [α_u ... ]; α_Δu = [α_Δu ... ]
#@test sum( abs.(S * α_u - M * α_Δu) ) < 1e-3
@test sum( abs.(S_ref * α_u - M_ref * α_Δu) ) < 1e-3


#solve discrete problem
int = [ interior(b) ... ]
bnd = .! int

A = S[:,int]
L = M * α_Δu - S[:,bnd] * α_u[bnd]
u0 = A / L
u_spline = zeros( length(b) )
u_spline[int] = u0
u_spline[bnd] = α_u[bnd]

xy = evaluate(F, t)
x,y = xy[1,:,:], xy[2,:,:]
B = evaluate(b, t)
B = reshape(B, length(b), size(B,3), size(B,4) )
u_spline = sum( B .* u_spline, dims=1)[1,:,:]

clf(); title("True solution")
contourf(x,y,u(x,y))
colorbar()
display(gcf())
clf(); title("Spline solution")
contourf(x,y,u_spline)
colorbar()
display(gcf())

@test maximum( abs.( u_spline - u(x,y) ) ) < 1e-2
===#
